#!/bin/sh

numthreads=4
js="0-50"

# Four-T
spins="2, 2, 2, 2"
delta12="0"
delta43="0"
s=1
for q4 in "2,2,2,2" "1,1,1,1" "1,2,1,2" "1,1,2,2" "2,1,1,2" "0,0,0,0" "0,1,0,1" "0,2,0,2" "0,1,1,2" "1,0,1,2" "0,0,1,1" "1,0,0,1" "0,0,-1,1" "-1,0,0,1" "0,0,2,2" "2,0,0,2" "0,1,-1,2" "-1,1,0,2" "0,-1,1,2" "1,-1,0,2" "1,-1,-1,1" "-1,-1,1,1"
do
    for j12 in 0 1 2 3 4
    do
        for j43 in 0 1 2 3 4
        do
            echo "Four T" $q4 $s $j12 $j43
            /usr/bin/time ./build/blocks_3d --j-external "$spins" --j-internal "$js" --j-12 $j12 --j-43 $j43 --delta-12 "$delta12" --delta-43 "$delta43" --four-pt-struct "$q4" --four-pt-sign "${s}" --order 80 --lambda 25 --kept-pole-order 30 --num-threads $numthreads --precision 665 -o test/benchmark/zzb_${q4}_${s}_${j12}_${j43}_{}.json
        done
    done
done
