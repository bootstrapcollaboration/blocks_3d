#!/usr/bin/env python3

import decimal
import json
import os
import shutil
import subprocess
import unittest


class Blocks3dTestCase(unittest.TestCase):
    blocks_3d_path = 'build/blocks_3d'
    test_data_dir = 'test/data'
    test_out_dir = 'test/out'
    threshold_to_print_small_number_warning = 0
    json_file_name = None

    def compare_bigfloat_str(self, a: str, b: str) -> None:
        if a == b:
            return

        # We run blocks_3d with precision=665,
        # which is ~200 decimal digits.
        # Last digits may differ due to rounding errors.
        # Thus, we should compare bigfloats up to a lower precision.
        # We take (rather conservatively) first 100 decimal digits
        def get_bigfloat(x: str):
            return decimal.Context(prec=100).create_decimal(x)

        a_decimal = get_bigfloat(a)
        b_decimal = get_bigfloat(b)
        # Ignore small nonzero numbers, subject to numerical noise
        # I've observed difference up to ~1e-193 for "scalar-fermion/0" case
        if a_decimal != 0 and b_decimal != 0 and a_decimal != b_decimal:
            abs_sum = abs(a_decimal) + abs(b_decimal)
            if abs_sum < 1e-150:
                # When running tests e.g. in Docker, we have more than 1000 small-number diffs,
                # which would lead to >4000 lines of output.
                # Essentially, user needs to see the warning only
                # for the largest small_a and small_b for each blocks_3d output file.
                # Thus, we print warning only if small_a and small_b
                # are larger than all small numbers we encountered before.
                # With this approach, we get only ~200 lines of output.
                if abs_sum > self.threshold_to_print_small_number_warning:
                    self.threshold_to_print_small_number_warning = abs_sum
                    print(f'{self.json_file_name}: '
                          f'Ignoring difference between small numbers (less than 1e-150):\n  {a}\n  {b}')
                return
        self.assertEqual(a_decimal, b_decimal, f'JSON bigfloats are not equal:\n  {a}\n  {b}')

    def compare_json_value(self, a, b, str_is_bigfloat=False):
        self.assertEqual(type(a), type(b), f'JSON value types are not equal: a={a} b={b}')

        if isinstance(a, dict):
            self.assertEqual(a.keys(), b.keys(), 'JSON object keys are not equal')
            for key in sorted(a.keys()):
                # "derivs" contains arrays of bigfloats.
                # Bigfloats can differ by last digits due to rounding errors,
                # so we cannot compare them as strings.
                # we convert to decimals, truncating last digits
                str_is_bigfloat = key == 'derivs'
                self.compare_json_value(a[key], b[key], str_is_bigfloat)
        elif isinstance(a, list):
            self.assertEqual(len(a), len(b), 'JSON array sizes are not equal')
            for aa, bb in zip(a, b):
                self.compare_json_value(aa, bb, str_is_bigfloat)
        elif isinstance(a, str) and str_is_bigfloat:
            self.compare_bigfloat_str(a, b)
        else:
            self.assertEqual(a, b, 'JSON values are not equal')

    def compare_output_dirs(self, a, b):
        a_names = {os.path.basename(x) for x in os.listdir(a) if not x.endswith(".profile")}
        b_names = {os.path.basename(x) for x in os.listdir(b) if not x.endswith(".profile")}
        self.assertEqual(a_names, b_names, f'Directories "{a}" and "{b}" are not equal.')

        for name in sorted(a_names):
            a_json_path = os.path.join(a, name)
            b_json_path = os.path.join(b, name)

            with open(a_json_path) as f:
                a_json = json.load(f)
            with open(b_json_path) as f:
                b_json = json.load(f)

            try:
                self.json_file_name = name
                self.threshold_to_print_small_number_warning = 0
                self.compare_json_value(a_json, b_json)
            except AssertionError as e:
                self.fail(f'diff {a_json_path} {b_json_path}: {e}')
            finally:
                self.json_file_name = None
                self.threshold_to_print_small_number_warning = 0

    def do_test_blocks_3d(self, test_name: str, command_line_args: str):
        out_dir = os.path.join(self.test_out_dir, test_name)
        if os.path.exists(out_dir):
            shutil.rmtree(out_dir)
        output_template = os.path.join(out_dir, 'derivs_{}.json')

        cmd = f'{self.blocks_3d_path} -o {output_template} {command_line_args}'
        print(cmd)
        res = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if res.stdout:
            print(res.stdout.decode())
        self.assertEqual(res.returncode, 0, f'Process failed: {res}')

        data_dir = os.path.join(self.test_data_dir, test_name)
        self.compare_output_dirs(out_dir, data_dir)

    def test_blocks_3d(self):
        print('Scalar')
        self.do_test_blocks_3d('scalar/0',
                               '--j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('scalar/1',
                               '--j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial"')

        print('Scalar-fermion')
        self.do_test_blocks_3d('scalar-fermion/0',
                               '--j-external "0.5, 0, 0, 0.5" --j-internal "0.5-4.5" --j-12 0.5 --j-43 0.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0, 0, -0.5" --four-pt-sign "-1" --order 15 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('scalar-fermion/1',
                               '--j-external "0.5, 0, 0, 0.5" --j-internal "0.5-4.5" --j-12 0.5 --j-43 0.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0, 0, -0.5" --four-pt-sign "-1" --order 15 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')

        print('Four-fermion')
        self.do_test_blocks_3d('four-fermion/0',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('four-fermion/1',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')

        self.do_test_blocks_3d('four-fermion/2',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('four-fermion/3',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')

        self.do_test_blocks_3d('four-fermion/4',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('four-fermion/5',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')

        self.do_test_blocks_3d('four-fermion/6',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('four-fermion/7',
                               '--j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')

        print('Vector-fermion')
        self.do_test_blocks_3d('vector-fermion/0',
                               '--j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, -0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('vector-fermion/1',
                               '--j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, -0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial"')

        self.do_test_blocks_3d('vector-fermion/2',
                               '--j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, 0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('vector-fermion/3',
                               '--j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, 0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial"')

        print('Vector-Vector')
        self.do_test_blocks_3d('vector-vector/0',
                               '--j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 1" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('vector-vector/1',
                               '--j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 1" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')

        self.do_test_blocks_3d('vector-vector/2',
                               '--j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 0" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"')
        self.do_test_blocks_3d('vector-vector/3',
                               '--j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 0" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 --coordinates="zzb,yyb,xt,ws"')


if __name__ == '__main__':
    # Set working directory
    cwd = os.getcwd()
    # blocks_3d_repo/test/run_test.py
    script_path = os.path.abspath(__file__)
    # blocks_3d_repo/
    root = os.path.dirname(os.path.dirname(script_path))
    os.chdir(root)
    if not os.path.exists('build/blocks_3d'):
        raise FileNotFoundError(f'build/blocks_3d not found! CWD={cwd}')

    unittest.main()
