#!/usr/bin/env wolframscript

tableDir="test/tables_test"
<<"test/mathematica/radial_recursion.m"
<<"test/mathematica/derivative_conversions.m"


(* Scalar blocks *)
twojsIntermediate=2*({0,1,2,3,4});
partialTables=writePartialTables[tableDir, 2*{0,0,0,0},
                                 0.120000000`200, 0.5430000000`200,
                                 2*0, 2*0, {2*{0,0,0,0},1},
                                 twojsIntermediate, 5, 10, 20];

Do[
        write\[Lambda]lr2zzb[partialTables, tableDir, 2*{0,0,0,0},
                             0.120000000`200, 0.5430000000`200,
                             2*0, 2*0, {2*{0,0,0,0},1},
                             twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20];
        write\[Lambda]lr2xt[partialTables, tableDir, {0, 0, 0, 0},
                            0.12`200, 0.543`200,
                            0, 0, {{0, 0, 0, 0}, 1},
                            twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20];
        write\[Lambda]lr2yyb[partialTables, tableDir, {0, 0, 0, 0},
                             0.12`200, 0.543`200,
                             0, 0, {{0, 0, 0, 0}, 1},
                             twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20];
        write\[Lambda]lr2ws[partialTables, tableDir, {0, 0, 0, 0},
                            0.12`200, 0.543`200,
                            0, 0, {{0, 0, 0, 0}, 1},
                            twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20];

        writeRadialOnlyBlocks["xt"][partialTables, tableDir, {0, 0, 0, 0},
                                    0.12`200, 0.543`200,
                                    0, 0, {{0, 0, 0, 0}, 1},
                                    twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20, 2.3`200];
        writeRadialOnlyBlocks["ws"][partialTables, tableDir, {0, 0, 0, 0},
                                    0.12`200, 0.543`200,
                                    0, 0, {{0, 0, 0, 0}, 1},
                                    twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20, 2.3`200];

        writeConvertedBlocks["zzb"][partialTables, tableDir, {0, 0, 0, 0},
                                    0.12`200, 0.543`200,
                                    0, 0, {{0, 0, 0, 0}, 1},
                                    twojsIntermediate[[twojIndex]], twojIndex,
                                    5, 10, 20, 2.3`200]
        
        writeConvertedBlocks["xt"][partialTables, tableDir, {0, 0, 0, 0},
                                   0.12`200, 0.543`200,
                                   0, 0, {{0, 0, 0, 0}, 1},
                                   twojsIntermediate[[twojIndex]], twojIndex,
                                   5, 10, 20, 2.3`200]
        
        writeConvertedBlocks["yyb"][partialTables, tableDir, {0, 0, 0, 0},
                                    0.12`200, 0.543`200,
                                    0, 0, {{0, 0, 0, 0}, 1},
                                    twojsIntermediate[[twojIndex]], twojIndex,
                                    5, 10, 20, 2.3`200]
        
        writeConvertedBlocks["ws"][partialTables, tableDir, {0, 0, 0, 0},
                                   0.12`200, 0.543`200,
                                   0, 0, {{0, 0, 0, 0}, 1},
                                   twojsIntermediate[[twojIndex]], twojIndex,
                                   5, 10, 20, 2.3`200]
        
       ,{twojIndex, Length[twojsIntermediate]}
];

(* (\* Scalar-fermion blocks *\) *)
(* twojsIntermediate=2*({0,1,2,3,4}+1/2); *)
(* partialTables=writePartialTables[tableDir, 2*{1/2,0,0,1/2}, *)
(*                                  0.12`200, 0.543`200, *)
(*                                  2*1/2, 2*1/2, {2*{1/2,0,0,-1/2},-1}, *)
(*                                  twojsIntermediate, 5, 10, 15]; *)
(* Do[ *)
(*         write\[Lambda]lr2zzb[partialTables, tableDir, 2*{1/2,0,0,1/2}, *)
(*                              0.12`200, 0.543`200, *)
(*                              2*1/2, 2*1/2, {2*{1/2,0,0,-1/2},-1}, *)
(*                              twojsIntermediate[[twojIndex]], twojIndex, 5,10, 15]; *)
(*        ,{twojIndex, Length[twojsIntermediate]} *)
(* ] *)


(* (\* Four-fermion blocks *\) *)
(* twojsIntermediate=2*({0,1,2,3,4}); *)
(* Do[ *)
(*         partialTables=writePartialTables[tableDir, 2*{1/2,1/2,1/2,1/2}, *)
(*                                          0.12`200, 0.543`200, *)
(*                                          2*j12, 2*j43, {2*{1/2,1/2,1/2,-1/2},1}, *)
(*                                          twojsIntermediate, 5, 5, 10]; *)
(*         Print["two_j: ",j12*2," ",j43*2]; *)
(*         Do[ *)
(*                 write\[Lambda]lr2zzb[partialTables, tableDir, 2*{1/2,1/2,1/2,1/2}, *)
(*                                      0.12`200, 0.543`200, *)
(*                                      2*j12, 2*j43, {2*{1/2,1/2,1/2,-1/2},1}, *)
(*                                      twojsIntermediate[[twojIndex]], twojIndex, *)
(*                                      5, 5, 10]; *)
(*                ,{twojIndex, Length[twojsIntermediate]} *)
(*         ] *)
(*        ,{j12,0,1} *)
(*        ,{j43,0,1} *)
(* ] *)


(* Vector-fermion blocks *)
twojsIntermediate=2*({0,1,2,3,4}+1/2);

partialTables=writePartialTables[tableDir, 2*{1,1/2,1,1/2},
                                 0.12`200, 0.543`200,
                                 2*3/2, 2*3/2, {2*{1,1/2,1,-1/2},1},
                                 twojsIntermediate, 3, 5, 7];
Do[
        (* write\[Lambda]lr2zzb[partialTables, tableDir, 2*{1,1/2,1,1/2}, *)
        (*                      0.12`200, 0.543`200, *)
        (*                      2*3/2, 2*3/2, {2*{1,1/2,1,-1/2},1}, *)
        (*                      twojsIntermediate[[twojIndex]], twojIndex, *)
        (*                      3, 5, 7]; *)
        (* writeRadialOnlyBlocks["xt"][partialTables, tableDir, {0, 0, 0, 0}, *)
        (*                             0.12`200, 0.543`200, *)
        (*                             0, 0, {{0, 0, 0, 0}, 1}, *)
        (*                             twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20, 2.3`200]; *)
        writeRadialOnlyBlocks["xt"][partialTables, tableDir, {0, 0, 0, 0},
                                    0.12`200, 0.543`200,
                                    0, 0, {{0, 0, 0, 0}, 1},
                                    twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20, 2.3`200];
        writeRadialOnlyBlocks["ws"][partialTables, tableDir, {0, 0, 0, 0},
                                    0.12`200, 0.543`200,
                                    0, 0, {{0, 0, 0, 0}, 1},
                                    twojsIntermediate[[twojIndex]], twojIndex, 5, 10, 20, 2.3`200];
       ,{twojIndex, Length[twojsIntermediate]}
]


(* partialTables=writePartialTables[tableDir, 2*{1,1/2,1,1/2}, *)
(*                                  0.12`200, 0.543`200, *)
(*                                  2*3/2, 2*3/2, {2*{1,1/2,1,1/2},1}, *)
(*                                  twojsIntermediate, 3, 5, 7]; *)
(* Do[ *)
(*         write\[Lambda]lr2zzb[partialTables, tableDir, 2*{1,1/2,1,1/2}, *)
(*                              0.12`200, 0.543`200, *)
(*                              2*3/2, 2*3/2, {2*{1,1/2,1,1/2},1}, *)
(*                              twojsIntermediate[[twojIndex]], twojIndex, *)
(*                              3, 5, 7]; *)
(*        ,{twojIndex, Length[twojsIntermediate]} *)
(* ] *)


(* (\* Vector-vector blocks *\) *)
(* twojsIntermediate=2*({0,1,2,3,4}); *)

(* partialTables=writePartialTables[tableDir, 2*{1,1,1,1}, *)
(*                                  0.12`200, 0.543`200, *)
(*                                  2*2, 2*2, {2*{1,1,1,1},1}, *)
(*                                  twojsIntermediate, 3, 5, 7]; *)

(* Do[ *)
(*         write\[Lambda]lr2zzb[partialTables, tableDir, 2*{1,1,1,1}, *)
(*                              0.12`200, 0.543`200, *)
(*                              2*2, 2*2, {2*{1,1,1,1},1}, *)
(*                              twojsIntermediate[[twojIndex]], twojIndex, *)
(*                              3, 5, 7]; *)
(*        ,{twojIndex, Length[twojsIntermediate]} *)
(* ] *)


(* partialTables=writePartialTables[tableDir, 2*{1,1,1,1}, *)
(*                                  0.12`200, 0.543`200, *)
(*                                  2*2, 2*2, {2*{1,1,1,0},1}, *)
(*                                  twojsIntermediate, 3, 5, 7]; *)
(* Do[ *)
(*         write\[Lambda]lr2zzb[partialTables, tableDir, 2*{1,1,1,1}, *)
(*                              0.12`200, 0.543`200, *)
(*                              2*2, 2*2, {2*{1,1,1,0},1}, *)
(*                              twojsIntermediate[[twojIndex]], twojIndex, *)
(*                              3, 5, 7]; *)
(*        ,{twojIndex, Length[twojsIntermediate]} *)
(* ] *)

