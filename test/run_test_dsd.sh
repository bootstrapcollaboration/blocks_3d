#!/bin/sh

# # Set working directory to sdpb root
# script_path="$0" # blocks_3d/test/run_test.sh
# cd "$(dirname "$script_path")" || exit 1 # blocks_3d/test/
# cd .. # blocks_3d/
# echo "root directory: $PWD"

# python3 test/run_test.py || { exit $?; }

# echo "$0: ALL TESTS PASSED"

#build/blocks_3d --debug 1 -o test/out/scalar/0/derivs_{}.json --j-external "0, 0, 0, 0" --j-internal "0-70" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 80 --lambda 5 --kept-pole-order 10 --num-threads 1 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"

# for ord in 40 50 60; do
#     /usr/bin/time -v build/blocks_3d --debug 1 -o test/out/tttt/0/derivs_{}.json --j-external "2, 2, 2, 2" --j-internal "0-70" --j-12 4 --j-43 4 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order $ord --lambda 4 --kept-pole-order 10 --num-threads 16 --precision 1024 --coordinates="xt" --delta-1-plus-2="6"
# done

#valgrind --tool=massif


# for order in 20 30 40; do
#     for keptPoleOrder in 10 20; do
#         for lambda in 4 8 12 16; do
#             for jMax in 20 40; do
#                 echo "--------"
#                 echo "order =" $order
#                 echo "kept_pole_order =" $keptPoleOrder
#                 echo "lambda =" $lambda
#                 echo "jMax =" $jMax
#                 /usr/bin/time -v build/blocks_3d --debug 1 -o test/out/tttt/0/derivs_{:.1f}.json --j-external "2, 2, 2, 2" --j-internal "0-$jMax" --j-12 4 --j-43 4 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order $order --lambda $lambda --kept-pole-order $keptPoleOrder --num-threads 16 --precision 1024 --coordinates="xt" --delta-1-plus-2="6" | grep "lambda_order = 0, residues_size\|m_matrix\.size\|f_q_basis_derivative\.size\|lambda_log_r_size"
#             done
#         done
#     done
# done

# for order in 20 30 40; do
#     for keptPoleOrder in 10 20; do
#         for lambda in 4 8 12 16; do
#             for jMax in 20 40; do
#                 echo "--------"
#                 echo "order =" $order
#                 echo "kept_pole_order =" $keptPoleOrder
#                 echo "lambda =" $lambda
#                 echo "jMax =" $jMax
#                 /usr/bin/time -v build/blocks_3d --debug 1 -o test/out/scalars/0/derivs_{:.1f}.json --j-external "0, 0, 0, 0" --j-internal "0-$jMax" --j-12 0 --j-43 0 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order $order --lambda $lambda --kept-pole-order $keptPoleOrder --num-threads 16 --precision 1024 --coordinates="xt" --delta-1-plus-2="6" | grep "lambda_order = 0, residues_size\|m_matrix\.size\|f_q_basis_derivative\.size\|lambda_log_r_size"
#             done
#         done
#     done
# done

# for order in 20 30 40; do
#     for keptPoleOrder in 10 20; do
#         for lambda in 4 8 12 16; do
#             for jMax in 20 40; do
#                 echo "--------"
#                 echo "order =" $order
#                 echo "kept_pole_order =" $keptPoleOrder
#                 echo "lambda =" $lambda
#                 echo "jMax =" $jMax
#                 /usr/bin/time -v build/blocks_3d --debug 1 -o test/out/mixed/0/derivs_{:.1f}.json --j-external "2, 2, 0, 0" --j-internal "0-$jMax" --j-12 2 --j-43 0 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order $order --lambda $lambda --kept-pole-order $keptPoleOrder --num-threads 16 --precision 1024 --coordinates="xt" --delta-1-plus-2="6" | grep "lambda_order = 0, residues_size\|m_matrix\.size\|f_q_basis_derivative\.size\|lambda_log_r_size"
#             done
#         done
#     done
# done


# build/blocks_3d --debug 1 -o test/out/scalar/0/derivs_{}.json --j-external "2, 2, 2, 2" --j-internal "0-70" --j-12 4 --j-43 4 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 60 --lambda 1 --kept-pole-order 10 --num-threads 1 --precision 1024 --coordinates="xt" --delta-1-plus-2="6"

# build/blocks_3d --debug 1 -o test/out/scalar/0/derivs_{}.json --j-external "1, 1, 1, 1" --j-internal "0-5" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 1" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 1 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"

#/usr/bin/time -v build/blocks_3d --debug 1 -o test/out/scalars/0/derivs_{:.1f}.json --j-external "2, 2, 2, 2" --j-internal "0-40" --j-12 0 --j-43 0 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 21 --lambda 2 --kept-pole-order 20 --num-threads 16 --precision 1024 --coordinates="xt" --delta-1-plus-2="6"

#/usr/bin/time -v build/blocks_3d --debug 1 -o test/out/scalars/0/derivs_{:.1f}.json --j-external "0, 0, 0, 0" --j-internal "0-40" --j-12 0 --j-43 0 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 2 --kept-pole-order 20 --num-threads 16 --precision 1024 --coordinates="xt" --delta-1-plus-2="6"

/usr/bin/time -v build/blocks_3d --debug 1 -o test/out/scalars/0/derivs_{:.1f}.json --j-external "0, 0, 0, 0" --j-internal "0-40" --j-12 0 --j-43 0 --delta-12 0 --delta-43 0 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 4 --kept-pole-order 10 --num-threads 16 --precision 1024 --coordinates=xt --delta-1-plus-2=6
