#!/bin/sh

# # Set working directory to sdpb root
script_path="$0" # blocks_3d/test/run_test.sh
cd "$(dirname "$script_path")" || exit 1 # blocks_3d/test/
cd .. # blocks_3d/
echo "root directory: $PWD"

python3 test/run_test.py || { exit $?; }

echo "$0: ALL TESTS PASSED"

# build/blocks_3d --debug 1 -o test/out/scalar/0/derivs_{}.json --j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 1 --precision 665 --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"
