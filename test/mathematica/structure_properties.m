(* ::Package:: *)

Get[FileNameJoin[{DirectoryName[$InputFileName],"util.m"}]];

GetRelative["conventions.m"];


(* Computes tensor product of two spins
(Int,Int) \[Rule] Int[] *)
so3Tensor[twoj1_, twoj2_]:=Table[j, {j, Abs[twoj1 - twoj2], twoj1 + twoj2, 2}];

(* Computes tensor product of two spins twoj1 and twoj2, but only outputs spins of given pseudoparity relative to twoj1 and twoj2
*)
so3TensorParity[p_,twoj1_,twoj2_]:=Cases[so3Tensor[twoj1, twoj2],
                                         j_/;EvenQ[(twoj1 + twoj2 - j)/2 - p]];


(*

Three-point stuructres come in two types,

SO3struct = {Int, Int}
Q3struct  = {Int, Int, Int}

* Valid representative q3struct of Q3struct always satisfies Total[q3struct]\[Equal]0
* In addition, we say q3struct is valid for set of spins twojs={twoj1,twoj2,twoj3} if Abs[q3struct[[i]]]\[LessEqual]twojs[[i]] and EvenQ[qstruct[[i]]-twojs[[i]]]
  (twojs themselves should satisfy EvenQ[Total[twojs]])
* A representative so3struct of SO3struct is valid for set of spins twojs={twoj1,twoj2,twoj3} if 
     - so3struct[[1]] is in so3Tensor[twoj1,twoj2] 
     - and so3struct[[2]] is in so3Tensor[so3struct[[1]],twoj3]

*)

(* This function verifies that SO3struct given by {twoj12,twoj120} is allowed for {*,*,twoj} (assuming twoj12 is allowed for the first two operators)  *)
structureAllowed[twoj_, {twoj12_, twoj120_}]:=
        EvenQ[twoj120 - twoj12 - twoj] &&
        (Abs[twoj12 - twoj] <= twoj120 <= twoj12 + twoj);

(*

Four-point structures come in two types

Q4struct = {Int, Int, Int, Int}
Q4PMstruct = {{Int, Int, Int, Int}, Sign} (Here Sign is enum with values +1 and -1)

* Representative q4struct of Q4struct is valid for set of spins twojs = {twoj1,twoj2,twoj3,twoj4} if 
    - Abs[q3struct[[i]]]\[LessEqual]twojs[[i]] 
    - and EvenQ[qstruct[[i]]-twojs[[i]]]

* Representative q4pmstruct = {q4struct, sign} of Q4PMstruct is valid for set of spins twojs = {twoj1,twoj2,twoj3,twoj4} if 
    - q4struct is valid for twojs
    - Total[q4struct]\[GreaterEqual]0 (this code will work if this condition is not satisfied, it is just that q4struct is essentially the same as -q4struct,
                         so perhaps not enforce it?)
    - if q4struct = {0,0,0,0} then sign = +1 (again, the code will work if sign is -1, but will output 0)
    
*)

(*

Computes parity of q4struct. 
* q4struct must be valid for twojs 
* twojs\[GreaterEqual]0

({Int,Int,Int,Int}, Q4struct) \[Rule] Parity (=enum with 0 (even) or 1 (odd))

*)

fourPointParity[twojs_,q4struct_]:=Mod[Total[twojs - q4struct]/2, 2];


(*
This function converts parity to pseudoparity and back.
Valid input has twoj1,twoj2,twoj12\[GreaterEqual]0 and twoj12 in so3Tensor[twoj1,twoj2].
*)
pseudoParityToParity[twoj1_, twoj2_, twoj12_][p_]:=
        Mod[(twoj1 - twoj2 - twoj12)/2 + p, 2];


(*
parityTypes[[parity+1]] tells which parity pairs of 3pts we should use for 4pt of parity parity.
*)
parityTypes={
  {{0,0},{1,1}},
  {{0,1},{1,0}}
  };
