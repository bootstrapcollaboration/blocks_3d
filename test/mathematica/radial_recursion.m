(* ::Package:: *)

Get[FileNameJoin[{DirectoryName[$InputFileName],"util.m"}]];

GetRelative["conventions.m"];
GetRelative["pole_classification.m"];
GetRelative["structure_properties.m"];
GetRelative["residue_matrices.m"];
GetRelative["hinfinity.m"];


(*

This function produces a list of poles that appear in conformal block with
intermediate spin twoj, SO3struct thee-pt structures with j12 and j43, and at r-order order. 

(Int,Int,Int,Int) \[Rule] Pole[]

Pole here is {PoleFamily,Int}
*)


poleTypes[twoj_,twoj12_,twoj43_,order_]:=Flatten[Table[
  Table[{family,k},{k,1,poleRangeK[family,twoj,twoj12,twoj43,order]}],
  {family,poleFamilies}
  ],1];


(* This function detects whether we have a non-empty matrix. We can avoid its use (TODO below). *)
nonEmptyMatrixQ[mat_]:=MatrixQ[mat]&&(Dimensions[mat][[1]]>=1)&&(Dimensions[mat][[2]]>=1);

(*

This function performs radial recursion given the vectorized hInfinity and other standard parameters.

pseudoparities = {{pL1, pR1},{pL2,pR2}}
This tells us which pseudoparities to use. When onverted to parities (depends on external spins which are not an input),
this becomes {{0,0},{1,1}} or {{0,1},{1,0}} depending on the parity of the 4-pt structure.

Valid input has 

* hInfinity of valid dimensions (infer from code)
* twojMax,twoj12,twoj43,order\[GreaterEqual]0
* EvenQ[twojMax-twoj12]&&EvenQ[twojMax-twoj43]
* pseudoparities = {{pL1, pR1},{pL2,pR2}} with all entries being 0 or 1. Furthermore, Mod[pseudoparities[[1]]+1,2]==pseudoparities[[2]]

(Real[...], Int, Real, Int, Real, Int, {{Parity, Parity},{Parity,Parity}}, Int) \[Rule] Real[...] (see structure of "residues" variable)

*)
radialRecursion[hInfinity_,twojMax_,Delta12LP_,twoj12_,Delta43LP_,twoj43_,pseudoparities_,order_]:=Module[
        {twojValues,poleTypes,twojMaxRecursion,rOrderMax,residues,Delta12=highPrecision@Delta12LP,Delta43=highPrecision@Delta43LP},

        Assert[Mod[twoj12,2]==Mod[twoj43,2]];
        Assert[Mod[twojMax,2]==Mod[twoj12,2]];

        (* The values of 2*j for which we need to compute blocks at rOrder-th order in r *)
        twojValues[rOrder_]:=Range[Mod[twoj12,2],twojMax+2*(order-rOrder),2];

        (* The types of poles that a block with 2*j=twoj has *)
        (* TODO: double check the upper bounds on these families *)
        poleTypes[twoj_]:= Flatten[Table[
                Table[{family,k},{k,1,poleRangeK[family,twoj,twoj12,twoj43,order]}],
                {family,poleFamilies}
                                  ],1];

        (* The lists poleTypes[...] and so3Tensor[...] are not actually needed here -- only their lengths,
           but we can optimize that later if necessary. *)
        (* PK: We could save some space by only storing those poles which have non-zero coefficients at given rOrder; ignoring this for now -- up to Walter *)
        (* PK: I am using paritytype as the first index, but that is perhaps stupid, might want to change this *)
        residues=Table[
                0,
                {paritytype,{1,2}}, (* May want to move 3 levels down? *)
                {rOrder,order},
                {twoj,twojValues[rOrder]},
                {pType,poleTypes[twoj]}, (* Can implement the above suggestion by making poleTypes take rOrder as an argument to use instead of order; will require a change of indexation below. *)
                {twoj120,so3TensorParity[pseudoparities[[paritytype,1]],twoj,twoj12]},
                {twoj430,so3TensorParity[pseudoparities[[paritytype,2]],twoj,twoj43]}
                 ];

        Module[
                {twoj,pType,rShift,twojPrime,deltaPrime,LM,RM,DeltaVector,Qval,sourceparitytype},
                Do[
                        twoj=twojValues[rOrder][[twojN]];
                        pType=poleTypes[twoj][[pTypeN]];
                        rShift=poleShift[pType];
                        
                        (* This check would need to be changed if we implement the optimization about the structure of "residues"*)
                        If[rShift<=rOrder, (* PK: changed < to <= *)
                           twojPrime=poleTwoJ[twoj,pType];
                           deltaPrime=poleDelta[twoj,pType]+rShift;
                           
                           Do[
                                   
                                   sourceparitytype=Mod[paritytype-1+poleParity[pType[[1]]],2]+1;
                                   
                                   LM=LMatrix[pType,Delta12,twoj12,pseudoparities[[sourceparitytype,1]],twoj];
                                   RM=RMatrix[pType,Delta43,twoj43,pseudoparities[[sourceparitytype,2]],twoj];
                                   Qval=Q[twoj,pType];
                                   
                                   If[(!nonEmptyMatrixQ[LM])||(!nonEmptyMatrixQ[RM]),Continue[];];
                                   (* We can handle this better by figuring out the dimensions of LM and RM without calls to LMatrix/RMatrix *)
                                   
                                   DeltaVector=Table[
                                           If[deltaPrime!=poleDelta[twojPrime,pTypePrime],
                                              1/(deltaPrime-poleDelta[twojPrime,pTypePrime]),
                                              0 (* deltaPrime=poleDelta[...] can only happen when pType is type III or IV and pTypePrime is type II in which case the prescription seems to be to exclude the pole, since both left and right residue matrices compose to give 0 *)
                                           ]
                                               ,{pTypePrime,poleTypes[twojPrime]}
                                               ];
                                   
                                   residues[[paritytype,rOrder,twojN,pTypeN]]=Qval*LM . (
                                           hInfinity[[sourceparitytype,rOrder-rShift+1,jFloor[twojPrime]+1]]+
                                           If[rOrder>rShift,
                                              Sum[
                                                      residues[[sourceparitytype,rOrder-rShift,jFloor[twojPrime]+1,i]]*DeltaVector[[i]],
                                                      {i,Length[DeltaVector]}
                                              ]
                                             ,0]) . Transpose[RM];
                                   
                                   
                                  ,{paritytype,{1,2}}]
                        ];,
                          {rOrder,order},
                        {twojN,Length[twojValues[rOrder]]},
                        {pTypeN,Length[poleTypes[twojValues[rOrder][[twojN]]]]}
                ];
        ];

        residues];

transposeSafe[{},n_]:=Table[{},{i,n}];
transposeSafe[l_,n_]:=Transpose[l];

(* zip two lists together *)
zip[xs_,ys_]:=Transpose[{xs,ys}];


(* A matrix representing the action of multiplication by Exp[Delta*t] on a set of (r,\[Eta]) derivatives, with an overall factor of r^Delta stripped off. *)
rDeltaMatrix[rders_]:=Table[
        Binomial[knew, kold] * Delta^(knew - kold),
        {knew,0,rders},
        {kold,0,rders}]

(* To get g from h, we must multiply by r^Delta.  This function performs that transformation on the PoleSeries of derivatives, resulting in a list of PartialFractions. (Derivatives of r^Delta result in polynomials in Delta, which is why we have PartialFractions.) *)
restoreRDelta[PartialFraction[const_,poles_],nmax_]:=
        Module[
                {rDeltaM,pols,poleProducts},
                rDeltaM = rDeltaMatrix[nmax];
                poleProducts = Table[
                        Map[Function[PolynomialQuotientRemainder[#,Delta-p[[1]],Delta]],rDeltaM . p[[2]]],
                        {p,poles}];
                pols = rDeltaM . const+Total[Map[Function[Map[First,#]],poleProducts]];
                Thread[PartialFraction[Expand[pols],
                                       Map[Function[zip[Map[First,poles],#]],
                                           transposeSafe[Map[Function[Map[Last,#]],poleProducts],
                                                         Length[pols]]]]]
        ];


conformalBlockDerivatives[twojs_, Delta12_, twoj12_, Delta43_,
                               twoj43_, q4pmstruct_,
                               twojMax_,order_,n_,rders_]:=
        Module[
                {paritytypes, pseudoparitytypes, hinfinity, residues,
                 twoj, ppindex, coeffsToDerivatives},

                paritytypes = parityTypes[[fourPointParity[twojs,
                                                           q4pmstruct[[1]]] + 1]];
                pseudoparitytypes = 
                {pseudoParityToParity[twojs[[1]], twojs[[2]], twoj12]/@#[[1]],
                 pseudoParityToParity[twojs[[4]], twojs[[3]], twoj43]/@#[[2]]
                }&@Transpose[paritytypes];
                pseudoparitytypes = Transpose[pseudoparitytypes];

                (* A factor of 1/2 was introduced relative to original prototype *)
                hinfinity =
                hInfinityVector[twojs, Delta12, Delta43][twoj12, twoj43, q4pmstruct[[1]]][twojMax, order, n]/2
                + (-1)^(Total[twojs]/2) * q4pmstruct[[2]]
                  * hInfinityVector[twojs, Delta12, Delta43][twoj12, twoj43, -q4pmstruct[[1]]][twojMax, order, n]/2;
               
                residues = radialRecursion[hinfinity, twojMax, Delta12, twoj12,
                                           Delta43, twoj43, pseudoparitytypes, order];

                coeffsToDerivatives = Table[
                        If[rOrder==0 && k==0, 1, rOrder^k]
                        * (twoPointA0*rCrossing)^rOrder,
                                      {k, 0, rders},
                        {rOrder, 0, order}];

                Table[restoreRDelta[PartialFraction
                                    [coeffsToDerivatives . hinfinity[[ppindex, All, jFloor[twoj] + 1,
                                                                    leftSO3structN, rightSO3structN]],
                                     Table[{poleDelta
                                            [twoj,
                                             poleTypes[twoj, twoj12, twoj43, order][[pTypeN]]],
                                            coeffsToDerivatives . (Join[{0},
                                                                      residues[[ppindex, All, jFloor[twoj] + 1,
                                                                                pTypeN,
                                                                                leftSO3structN,
                                                                                rightSO3structN]]])},
                                           {pTypeN, Length[poleTypes[twoj, twoj12, twoj43, order]]}]],
                                    rders],
                      {twoj, Mod[twojMax, 2], twojMax, 2},
                      {ppindex, {1, 2}},
                      {leftSO3structN, Length[so3TensorParity[pseudoparitytypes[[ppindex, 1]],
                                                              twoj, twoj12]]},
                      {rightSO3structN, Length[so3TensorParity[pseudoparitytypes[[ppindex, 2]],
                                                               twoj, twoj43]]}
               ]
        ]


(* This version of truncation attempts to approximate discarded poles in terms of kept poles.  The approximation is exact at x=0 (the unitarity bound for spin l \[GreaterEqual] 1, and at x=Infinity, and in general is better by a factor of 10^-2 or 10^-3 than naively discarding a pole. *)
(* We must make sure the 'to' poles are not exact numbers -- otherwise we get major memory usage! *)
shiftPoles[from_,to_]:=Module[
        {n=Length[to]},
        zip[to, LinearSolve[
                Table[SetPrecision[to[[i]], prec]^m,
                      {m, -Floor[n/2], n - 1 - Floor[n/2]},
                      {i, n}],
                Table[Sum[p[[2]] * SetPrecision[p[[1]], prec]^m,
                          {p, from}],
                      {m, -Floor[n/2], n - 1 - Floor[n/2]}]]]];

togetherWithFactors[factors_,x_][PartialFraction[poly_,rs_]]:=Module[
        {mult},
        mult = Product[x - f, {f, factors}];
        N[poly*mult + Sum[r[[2]]*Product[x - f,
                                         {f, Complement[factors,
                                                        {r[[1]]}]}], {r, rs}],
          prec]//Expand];

(* If a pole p satisfies protectPole[p]=True, leave its residue untouched (neither shift the pole elsewhere, nor shift other poles to it), meanwhile shift all other poles in the given partial fraction to polesToKeep *)
polynomialTruncate[PartialFraction[polynomial_,polePart_],protectPole_,polesToKeep_,x_]:=Module[
        {protected, unprotected, keep, allKeptPoles},
        protected = Select[polePart, Function[protectPole[First[#]]]];
        unprotected = Select[polePart, Function[!protectPole[First[#]]]];
        keep = Select[polesToKeep, Function[!protectPole[#]]];
        allKeptPoles = Join[protected, shiftPoles[unprotected, keep]];
        togetherWithFactors[Map[First,allKeptPoles], x][PartialFraction[polynomial, allKeptPoles]]];

shiftPartialFraction[PartialFraction[pol_, rs_], Delta_, a_]:=
        PartialFraction[pol/.Delta->Delta + a,
                        Table[{r[[1]] - a, r[[2]]}, {r, rs}]];

blockPolynomials[block_,twoj_,twoj12_,twoj43_,keptPoleOrder_]:= Module[
        {
                keep=Map[Function[#-If[twoj <= 1, twoj/2+1/2, twoj/2+1]],
                         Map[Function[poleDelta[twoj,#]],
                             poleTypes[twoj,twoj12,twoj43,keptPoleOrder]]],
                protect = Function[p,p==0]
        },
        Map[Function[polynomialTruncate[shiftPartialFraction[#, Delta, If[twoj <=1, twoj/2 + 1/2, twoj/2 + 1]],
                                        protect, keep, Delta]],block]];


writeLogRLambdaDerivTable[tableDir_, twojs_,delta12LowPrecision_,
                          delta43LowPrecision_,twoj12_,twoj43_,
                          q4pmstruct_,twojsIntermediate_, rders_,
                          n_, keptPoleOrder_, order_] := Module[
    {
        twojMax = Max[twojsIntermediate],
        blocks, shiftedblocksPols, file,
        delta12 = SetPrecision[delta12LowPrecision, prec],
        delta43 = SetPrecision[delta43LowPrecision, prec]
    },
    blocks = conformalBlockDerivatives[twojs, delta12, twoj12, delta43,
                                       twoj43, q4pmstruct, twojMax,
                                       order, n, rders];
    Table[
               blockPolynomials[blocks[[jFloor[twoj] + 1, ppindex,
                                        leftstruct, rightstruct]],
                                twoj, twoj12, twoj43, keptPoleOrder]/.Delta->x,
               {twoj, twojsIntermediate},
               {ppindex, {1, 2}},
               {leftstruct, Length[blocks[[jFloor[twoj] + 1, ppindex]]]},
               {rightstruct, Length[blocks[[jFloor[twoj] + 1, ppindex, 1]]]}]];


(* Note that this Do loop can be completely paralellized *)

writePartialTables[tableDir_, twojs_, delta12LowPrecision_, delta43LowPrecision_,
                           twoj12_, twoj43_, q4pmstruct_, twojsIntermediate_,
                           Lambda_, keptPoleOrder_, order_] :=
        Table[
                writeLogRLambdaDerivTable[tableDir, twojs, delta12LowPrecision,
                                     delta43LowPrecision, twoj12, twoj43,
                                     q4pmstruct, twojsIntermediate,
                                     Lambda - n, n, keptPoleOrder, order],
                {n, (1 - q4pmstruct[[2]])/2, Lambda, 2}]
