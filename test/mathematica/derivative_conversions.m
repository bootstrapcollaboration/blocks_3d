(* ::Package:: *)

(*
    This uses >>dynamic programming<< to compute a table of Striling numbers of the first kind. 
    Using built-in StirlingS1 in Mathematica is faster, but we provide this implementation for reference. 
    Since we need a table, the only way we could improve this is to use fewer multiplications by utilizing some smarter way of computing the Stirling numbers.
*)
log2linDs[order_]:=Module[{result,n,k},
		result=ConstantArray[0,{order+1,order+1}];
		result[[1,1]]=1;
		Do[
			result[[n+1,k+1]]=-(n-1)result[[n,k+1]]+result[[n,k]]
		,{n,1,order}
		,{k,1,n}];
		result
	]


log2linDsWithPrefactor[order_]:=DiagonalMatrix[Table[(3-2Sqrt[2])^-n,{n,0,order}]].log2linDs[order]


l\[Rho]2\[Rho]RO[{parity_,in_}]:=
        Module[{order},
               (* Print["lrho2rho: ",Dimensions[in], *)
               (*     (\*   in[[1,1]],"\n" *\) *)
               (*     (\* , *\) *)
               (*       (\* log2linDsWithPrefactor[Length[in[[1]]]-1][[1,1]],"\n", *\) *)
               (*       (\* N[log2linDsWithPrefactor[Length[in[[1]]]-1]] *\) *)
               (*      (\* ,"\n" *\) *)
               (*     (\* , *\) *)
               (*       N[in.Transpose@log2linDsWithPrefactor[Length[in[[1]]]-1]] *)
               (* ]; *)
               {parity,in.Transpose@log2linDsWithPrefactor[Length[in[[1]]]-1]}
        ]
(*
  parity : 0 or 1, determines the parity of our function under lambda \[Rule] -lambda. 
  input: the array of lambda and log[r] derivatives. The first index is for lambda derivative order, the second index is for log[r] derivative order. 
         since parity is fixed, input only contains even (when parity=0) or odd (when parity=1) lambda-derivatives.
         the array is triangular, so that (lambda-order) + (log[r]-order) \[LessEqual] order, where order is some positive integer.
         
the output is a pair {parity, output}. here

  output: the array of log[rho], log[rhob] derivatives. The first index is for log[rho] derivatives. The second index is for log[rhob] derivatives.
          since parity is fixed, we store not all derivatives. If i is log[rho]-order and j is log[rhob]-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
  
  Log[\[Rho]]\[Equal]Log[r]+\[Lambda], Log[\[Rho]b]\[Equal]Log[r]-\[Lambda]
*)
\[Lambda]lr2l\[Rho]l\[Rho]b[{parity_,input_}]:=Module[{order},
	order=Length@input[[1]]-1+parity;
	{parity,
	 Table[
			Sum[(-1)^(j-l) 2^(-i-j) Binomial[i,k] Binomial[j,l] input[[(i+j-k-l-parity)/2+1,k+l+1]],{k,0,i},{l,Mod[i+j-k-parity,2],j,2}]
	 ,{i,0,order},{j,0,Min[i,order-i]}]
	}
]


(*Function for convenience of implementing the next two steps. Does the following:
		trMat[order].input.Transpose@trMat[order]
	where input is a lower-triangular matrix (i.e. for elements 
	input[[i,j]] where j\[LessEqual]i. Returns a lower triangular matrix with its parity.
  *)
congruenceTransformation[trMat_][{parity_,input_}]:=
	Expand@Module[{order=Length@input-1,transf=trMat[Length@input-1]},
		{parity,
		Table[Sum[If[j<=i,transf[[l+1,i+1]]input[[i+1,j+1]]transf[[k+1,j+1]],
		(-1)^parity transf[[l+1,i+1]]input[[j+1,i+1]]transf[[k+1,j+1]]],{i,0,order},{j,0,order-i}],{l,0,order},{k,0,Min[l,order-l]}]
		}
		];


(*
  parity : 0 or 1, determines the parity of our function under rho \[TwoWayRule] rhobar. 
  input:  the array of log[rho], log[rhob] derivatives. The first index is for log[rho] derivatives. The second index is for log[rhob] derivatives.
          since parity is fixed, we store not all derivatives. If i is log[rho]-order and j is log[rhob]-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
         
	the output is a pair {parity, output}. here

  output: the array of rho, rhob derivatives. The first index is for log[rho] derivatives. The second index is for log[rhob] derivatives.
          since parity is fixed, we store not all derivatives. If i is log[rho]-order and j is log[rhob]-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order. 
          *)
l\[Rho]l\[Rho]b2\[Rho]\[Rho]b[{parity_,input_}]:=
	congruenceTransformation[log2linDsWithPrefactor][{parity,input}]

z\[Rho]matelem[n_,p_]/;p>=n:=z\[Rho]matelem[n,p]=p!/n!Sum[Binomial[n,k](1/2)^n (-1)^k (4Sqrt[2])^(n-k) Binomial[-2n,p-k-n](4-2Sqrt[2])^(-n-p+k),{k,0,n}];
z\[Rho]matelem[n_,p_]/;p<n:=z\[Rho]matelem[n,p]=0

z\[Rho]mat[order_]:=z\[Rho]mat[order]=Table[z\[Rho]matelem[n,p],{p,0,order},{n,0,order}]

\[Rho]zmat[order_]:=\[Rho]zmat[order]=Inverse@z\[Rho]mat[order];

z\[Rho]mat[order_]:=z\[Rho]mat[order]=Table[z\[Rho]matelem[n,p],{p,0,order},{n,0,order}]

\[Rho]2zRO[{parity_,in_}]:=
        Module[{order},
               (* Print["rho2z: ",N[in.Transpose@\[Rho]zmat[Length[in[[1]]]-1]]]; *)
               (* Print["rho2z: ",N[Transpose[\[Rho]zmat[Length[in[[1]]]-1]]]]; *)
               (* Print["rho2z: ",N[Transpose[log2linDsWithPrefactor[Length[in[[1]]]-1]]]]; *)
               (* Print["rho2z: ",N[Transpose[log2linDsWithPrefactor[Length[in[[1]]]-1]]].N[Transpose[\[Rho]zmat[Length[in[[1]]]-1]]]]; *)
               {parity,in.Transpose@\[Rho]zmat[Length[in[[1]]]-1]}
        ];

(*
  parity : 0 or 1, determines the parity of our function under rho \[TwoWayRule] rhobar. 
  input:  the array of rho, rhob derivatives. The first index is for rho derivatives. The second index is for rhob derivatives.
          since parity is fixed, we store not all derivatives. If i is rho-order and j is rhob-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order. 
         
	the output is a pair {parity, output}. here

  output: the array of z, zb derivatives. The first index is for z derivatives. The second index is for zb derivatives.
          since parity is fixed, we store not all derivatives. If i is z-order and j is z-order, then we store derivatives that satisfy the conditions
          i\[GreaterEqual]j and i+j \[LessEqual] order. 
          
          z\[Equal](4\[Rho])/(1+\[Rho])^2
          *)
\[Rho]\[Rho]b2zzb[{parity_,input_}]:= congruenceTransformation[\[Rho]zmat][{parity,input}]


(*
  parity : 0 or 1, determines the parity of our function under z \[TwoWayRule] zbar. 
  input:  the array of z, zb derivatives of f. The first index is for z derivatives. The second index is for zb derivatives.
          since parity is fixed, we store not all derivatives. If i is z-order and j is zb-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
         
	the output is a pair {parity, output}. here

  output: the array of x, t derivatives of f for parity 0 and f/((z-zb)/2)\[Equal]f/Sqrt[t] for parity 1. The first index is for x derivatives. 
          The second index is for t derivatives. since parity is fixed, we store not all derivatives. If i is x-order and j is t-order, 
          then we store derivatives that satisfy the condition i+2*j \[LessEqual] order. 
          
          x\[Equal](z+zb-1)/2, t=((z-zb)/2)^2
          *)
zzb2xt[{parity_,input_}]:=
        {
                parity,
                Expand[
                        Module[
                                {
                                        order=Length@input-1
                                },
                                Table[
                                        Sum[
                                                Module[
                                                        {
                                                                i=n+m-k-l+1,
                                                                j=k+l+1
                                                        },
                                                        (-1)^l (((m-parity)/2)!)/m! *
                                                        Binomial[n,k] *
                                                        Binomial[m,l] *
                                                        If[i>=j,
                                                           input[[i,j]],
                                                           (-1)^parity input[[j,i]]
                                                        ]
                                                ],
                                                {k,0,n},
                                                {l,0,m}
                                        ],
                                        {n,0,order-parity},
                                        {m,parity,order-n,2}
                                ]
                        ]
                ]
        }


zymatelem[m_,n_]/;EvenQ[m-n]:=zymatelem[m,n]=m!/n! Binomial[-n,(m-n)/2];
zymatelem[m_,n_]/;OddQ[m-n]=0;
zymat[order_]:=zymat[order]=Table[zymatelem[m,n],{m,0,order},{n,0,order}];


z2yRO[{parity_,input_}]:={parity,input.Transpose@zymat[Length[input[[1]]]-1]}


(*
  parity : 0 or 1, determines the parity of our function under z \[TwoWayRule] zbar. 
  input:  the array of z, zb derivatives. The first index is for z derivatives. The second index is for zb derivatives.
          since parity is fixed, we store not all derivatives. If i is z-order and j is zb-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
         
	the output is a pair {parity, output}. here

  output: the array of y, yb derivatives. The first index is for x derivatives. 
          The second index is for t derivatives. since parity is fixed, we store not all derivatives. If i is y-order and j is yb-order, 
          then we store derivatives that satisfy the conditions of i\[GreaterEqual]j and i+j \[LessEqual] order. 
          
          z\[Equal](1+y)^2/(2(1+y^2))
          *)
zzb2yyb[{parity_,input_}]:=congruenceTransformation[zymat][{parity,input}]


(*
  NOTE:  This function is merely an alias for zzb2xt, since the calculation is identical.

  parity : 0 or 1, determines the parity of our function under y \[TwoWayRule] ybar. 
  input:  the array of y, yb derivatives of f. The first index is for y derivatives. The second index is for yb derivatives.
          since parity is fixed, we store not all derivatives. If i is y-order and j is yb-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
         
	the output is a pair {parity, output}. here

  output: the array of w, s derivatives of f for parity 0 and f/((y-yb)/2)\[Equal]f/Sqrt[s] for parity 1. The first index is for w derivatives. 
          The second index is for s derivatives. since parity is fixed, we store not all derivatives. If i is w-order and j is s-order, 
          then we store derivatives that satisfy the condition i+j*2 \[LessEqual] order. 
          
          w\[Equal](y+yb)/2, s\[Equal]((y-yb)/2)^2
          *)
yyb2ws[{parity_,input_}]:=zzb2xt[{parity,input}]


(*
  This function produces the transformation matrix that converts derivatives of f[z] into derivatives of z^-\[CapitalDelta]f[z].
 
  \[CapitalDelta]: power of the prefactor
  z: the value of z (canonically 1/2 at the crossing-symmetric point)
  order: the highest order of derivatives wrt z
  
  output: the transformation matrix, an (order+1)\[Times](order+1) matrix with the column index representing the order of 
          f[z] derivative and the row matrix representing the order of z^-\[CapitalDelta]f[z] derivative

*)
powerPrefactorMatrix[\[CapitalDelta]_,z_][order_]:=
        Table[
                FactorialPower[
                        -\[CapitalDelta],n-k]z^(k-n-\[CapitalDelta]) Binomial[n,k] ,
                {n,0,order},
                {k,0,order}
        ]//FunctionExpand//Expand;


(*
  This function produces the transformation matrix that converts derivatives of f[z] into derivatives of (1-z)^-\[CapitalDelta]f[z].
 
  \[CapitalDelta]: power of the prefactor
  z: the value of z (canonically 1/2 at the crossing-symmetric point)
  order: the highest order of derivatives wrt z
  
  output: the transformation matrix, an (order+1)\[Times](order+1) matrix with the column index representing the order of 
          f[z] derivative and the row matrix representing the order of (1-z)^-\[CapitalDelta]f[z] derivative

*)
oneMinusPowerPrefactorMatrix[\[CapitalDelta]_,z_][order_]:=
        Table[
                FactorialPower[
                        -\[CapitalDelta],n-k](-1)^(n-k) (1-z)^(k-n-\[CapitalDelta]) Binomial[n,k] ,
                {n,0,order},
                {k,0,order}
        ]//FunctionExpand//Expand;

(*
  This function takes derivatives of f[z,zb] and returns derivatives of (z zb)^{-\[CapitalDelta]/2}f[z,zb] at the crossing-symmetric point of z=1/2
  \[CapitalDelta]: power of the prefactor
  parity : 0 or 1, determines the parity of our function under z \[TwoWayRule] zbar. 
  input:  the array of z, zb derivatives. The first index is for z derivatives. The second index is for zb derivatives.
          since parity is fixed, we store not all derivatives. If i is z-order and j is zb-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
          
  output: the same format as above, but with the prefactor included.
*)
applyStandardPrefactor[\[CapitalDelta]_][{parity_,input_}]:=
        congruenceTransformation[
                powerPrefactorMatrix[
                        \[CapitalDelta]/2,1/2]][{parity,input}]

(*
  This function takes derivatives of f[z] and returns derivatives of z^-\[CapitalDelta]f[z] at z=1/2.
  NOTE: for parity-odd matrices, there is an additional prefactor needed to account for the \[Lambda]-derivative.
        This function accounts for the factor.
  
  \[CapitalDelta]: power of the prefactor
  parity : 0 or 1, determines the parity of our function under z \[TwoWayRule] zbar. 
  input:  the array of z, zb derivatives. The first index is for z derivatives. The second index is for zb derivatives.
          since parity is fixed, we store not all derivatives. If i is z-order and j is zb-order, then we store derivatives that satisfy the conditinos
          i\[GreaterEqual]j and i+j \[LessEqual] order.
          
  output: the same format as above, but with the prefactor included.
 *)

radialOnlyPrefactorRO[\[CapitalDelta]_][{parity_,input_}]:=
        Module[
                {
                        order,
                        preMat
                },
                order=Length[input[[1]]]-1;
                preMat=Switch[parity,
			      0,powerPrefactorMatrix[\[CapitalDelta],1/2][order],
			      1,oneMinusPowerPrefactorMatrix[1/2,1/2][order].powerPrefactorMatrix[\[CapitalDelta]+1,1/2][order]
		       ];
                Assert[Length[preMat[[1]]]==Length@input];
                {parity,input.Transpose@preMat}
        ]

radialOnly[{parity_,input_}]:={parity,input[[1]]}


(* ::Section:: *)
(*actually converting block files:*)


(*The output format of these transformations is a .m file
  containing a nested list structure. The outer index
  corresponds to the relative parities of the choices of
  3pt structures (i.e. for an even 4pt structure they are
  ++ or --, and for an odd 4pt structure they are +- or -+)
  which runs between 1 and 2. The 2nd and 3rd indices are
  the choices of 3pt tensor structures for the left and 
  right i.e. the 3pt structure containing operators 1 and
  2 and operators 4 and 3; the possible values of these
  indices depends on the value of the first index and are
  generically different for both choices. The 4th and 5th
  indices are the order of derivatives in the variables
  chosen for each function; because of the parity-definite
  -ness of the blocks, to save space we store only half
  of the total number of derivatives, namely the lower
  -triangular half, so if the indices are i and j, they 
  satisfy j \[LessEqual] i and i + j \[LessEqual] n. The allowed range of the 
  4th and 5th indices is independent of the other indices. *)
  


safeTranspose[args___]:=Quiet[Transpose[args]]/. {HoldPattern[Transpose[x_,___]]:>x}

importTables[tableDir_, twojs_, delta12LowPrecision_, delta43LowPrecision_,
                     twoj12_,twoj43_,q4pmstruct_,twoj_, Lambda_,
                     keptPoleOrder_, order_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec]
                },
                Table[Module[
                        {file},
                        file= blockLogRhoLambdaTablePath[tableDir, twojs,
                                                         delta12, delta43,
                                                         twoj12, twoj43,
                                                         q4pmstruct, twoj,
                                                         Lambda-n, n,
                                                         keptPoleOrder, order];
                        Print[file];
                        Import[file]
                      ],{n,(1-q4pmstruct[[2]])/2,Lambda,2}
                ]
        ]


write\[Lambda]lr2zzb[partialTables_, tableDir_, twojs_, delta12LowPrecision_,
                             delta43LowPrecision_,twoj12_,twoj43_,
                             q4pmstruct_,twoj_, twojIndex_, Lambda_, keptPoleOrder_, order_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec]
                       ,zzb
                },
                zzb=Map[Function
                        [\[Rho]\[Rho]b2zzb
                         [l\[Rho]l\[Rho]b2\[Rho]\[Rho]b
                          [\[Lambda]lr2l\[Rho]l\[Rho]b[{(1-q4pmstruct[[2]])/2,#}]]
                         ]
                        ],
                        Map[Function[safeTranspose[#,{3,1,2}]],
                            Transpose[Table[partialTables[[n,twojIndex,ppindex]],
                                            {n,Length[partialTables]},
                                            {ppindex, {1, 2}}]]
                        ],
                        {3}
                    ];
                Do[
                   Do[
                      Do[
                         Do[
                            Do[
                               Do[
                                  Do[
                                       Print["0 ",twojIndex-1," ",i-1," ",j-1," ",k-1," ",l-1," ",m-1," ",n-1
                                            ," ",o-1," ",
                                             CoefficientList[zzb[[i]][[j,k,l]][[m]][[n]],x][[o]]
                                       ],
                                       {o,Length[CoefficientList[zzb[[i]][[j,k,l]][[m]][[n]],x]]}
                                  ],
                                  {n,Length[zzb[[i]][[j,k,l]][[m]]]}
                               ],
                               {m,Length[zzb[[i]][[j,k,l]]]}
                            ],
                            {l,Dimensions[zzb[[i]]][[3]]}
                         ],
                         {k,Dimensions[zzb[[i]]][[2]]}
                      ],
                      {j,Dimensions[zzb[[i]]][[1]]}
                   ],
                   {i,Length[zzb]}
                ];
        ]



write\[Lambda]lr2yyb[partialTables_, tableDir_, twojs_,
                                  delta12LowPrecision_,
                                  delta43LowPrecision_,
                                  twoj12_,twoj43_,q4pmstruct_,
                                  twoj_, twojIndex_, Lambda_,
                                  keptPoleOrder_, order_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec]
                },
                yyb=Map[Function
                        [zzb2yyb
                         [\[Rho]\[Rho]b2zzb
                          [l\[Rho]l\[Rho]b2\[Rho]\[Rho]b
                           [\[Lambda]lr2l\[Rho]l\[Rho]b[{(1-q4pmstruct[[2]])/2,#}]]
                          ]
                         ]
                        ],
                        Map[Function[safeTranspose[#,{3,1,2}]],
                            Transpose[Table[partialTables[[n,twojIndex,ppindex]],
                                            {n,Length[partialTables]},
                                            {ppindex, {1, 2}}]]
                        ],
                        {3}
                    ];
                Do[
                   Do[
                      Do[
                         Do[
                            Do[
                               Do[
                                  Do[
                                       Print["0 ",twojIndex-1," ",i-1," ",j-1," ",k-1," ",l-1," ",m-1," ",n-1
                                            ," ",o-1," ",
                                             CoefficientList[yyb[[i]][[j,k,l]][[m]][[n]],x][[o]]
                                       ],
                                       {o,Length[CoefficientList[yyb[[i]][[j,k,l]][[m]][[n]],x]]}
                                  ],
                                  {n,Length[yyb[[i]][[j,k,l]][[m]]]}
                               ],
                               {m,Length[yyb[[i]][[j,k,l]]]}
                            ],
                            {l,Dimensions[yyb[[i]]][[3]]}
                         ],
                         {k,Dimensions[yyb[[i]]][[2]]}
                      ],
                      {j,Dimensions[yyb[[i]]][[1]]}
                   ],
                   {i,Length[yyb]}
                ];

        ]


write\[Lambda]lr2xt[partialTables_, tableDir_, twojs_,
                                 delta12LowPrecision_,
                                 delta43LowPrecision_,
                                 twoj12_, twoj43_, q4pmstruct_,
                                 twoj_, twojIndex_, Lambda_,
                                 keptPoleOrder_, order_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec]
                },
                xt=Map[Function
                       [zzb2xt
                        [\[Rho]\[Rho]b2zzb
                         [l\[Rho]l\[Rho]b2\[Rho]\[Rho]b
                          [\[Lambda]lr2l\[Rho]l\[Rho]b[{(1-q4pmstruct[[2]])/2,#}]]
                         ]
                        ]
                       ],
                       Map[Function[safeTranspose[#,{3,1,2}]],
                           Transpose[Table[partialTables[[n,twojIndex,ppindex]],
                                           {n,Length[partialTables]},
                                           {ppindex, {1, 2}}]]
                       ],
                       {3}
                   ];
                Do[
                   Do[
                      Do[
                         Do[
                            Do[
                               Do[
                                  Do[
                                       Print["0 ",twojIndex-1," ",i-1," ",j-1," ",k-1," ",l-1," ",m-1," ",n-1
                                            ," ",o-1," ",
                                             CoefficientList[xt[[i]][[j,k,l]][[m]][[n]],x][[o]]
                                       ],
                                       {o,Length[CoefficientList[xt[[i]][[j,k,l]][[m]][[n]],x]]}
                                  ],
                                  {n,Length[xt[[i]][[j,k,l]][[m]]]}
                               ],
                               {m,Length[xt[[i]][[j,k,l]]]}
                            ],
                            {l,Dimensions[xt[[i]]][[3]]}
                         ],
                         {k,Dimensions[xt[[i]]][[2]]}
                      ],
                      {j,Dimensions[xt[[i]]][[1]]}
                   ],
                   {i,Length[xt]}
                ];
        ]


write\[Lambda]lr2ws[partialTables_, tableDir_, twojs_,
                                 delta12LowPrecision_,
                                 delta43LowPrecision_,
                                 twoj12_, twoj43_, q4pmstruct_,
                                 twoj_, twojIndex_, Lambda_,
                                 keptPoleOrder_, order_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec]
                },
                ws=Map[Function
                       [yyb2ws
                        [zzb2yyb
                         [\[Rho]\[Rho]b2zzb
                          [l\[Rho]l\[Rho]b2\[Rho]\[Rho]b
                           [\[Lambda]lr2l\[Rho]l\[Rho]b[{(1-q4pmstruct[[2]])/2,#}]]
                          ]
                         ]
                        ]
                       ],
                       Map[Function[safeTranspose[#,{3,1,2}]],
                           Transpose[Table[partialTables[[n,twojIndex,ppindex]],
                                           {n,Length[partialTables]},
                                           {ppindex, {1, 2}}]]
                       ],
                       {3}
                   ];
                Do[
                   Do[
                      Do[
                         Do[
                            Do[
                               Do[
                                  Do[
                                       Print["0 ",twojIndex-1," ",i-1," ",j-1," ",k-1," ",l-1," ",m-1," ",n-1
                                            ," ",o-1," ",
                                             CoefficientList[ws[[i]][[j,k,l]][[m]][[n]],x][[o]]
                                       ],
                                       {o,Length[CoefficientList[ws[[i]][[j,k,l]][[m]][[n]],x]]}
                                  ],
                                  {n,Length[ws[[i]][[j,k,l]][[m]]]}
                               ],
                               {m,Length[ws[[i]][[j,k,l]]]}
                            ],
                            {l,Dimensions[ws[[i]]][[3]]}
                         ],
                         {k,Dimensions[ws[[i]]][[2]]}
                      ],
                      {j,Dimensions[ws[[i]]][[1]]}
                   ],
                   {i,Length[ws]}
                ];
        ]


forLoopConversion[conversionOperation_,namePrefix_][tableDir_, twojs_,
                                                            delta12LowPrecision_, delta43LowPrecision_,
                                                            twoj12_,twoj43_, q4pmstruct_, twoj_,
                                                            Lambda_, keptPoleOrder_, order_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec],
                        inTables = importTables[tableDir, twojs,
                                                delta12LowPrecision, delta43LowPrecision,
                                                twoj12, twoj43,
                                                q4pmstruct, twoj, Lambda,
                                                keptPoleOrder, order],
                        outTables,
                        oldDims,
                        newDims},
                oldDims={Dimensions[inTables][[1]],Dimensions/@(inTables[[1]])};
                newDims=If[Length[#]==3,Insert[#,oldDims[[1]],3],#]&/@oldDims[[2]];
                outTables=Table[
                        inTables[[\[Lambda]Order,ppindex,left,right,lrOrder]],
                        {
                                ppindex,
                                {1,2}
                        },
                        {
                                left,
                                newDims[[ppindex,1]]
                        },
                        {
                                right,
                                newDims[[ppindex,2]]
                        },
                        {
                                \[Lambda]Order,
                                newDims[[ppindex,3]]
                        },
                        {
                                lrOrder,
                                newDims[[ppindex,4]]-2(\[Lambda]Order-1)
                        }
                          ];
                outTables=Map[conversionOperation[{(1-q4pmstruct[[2]])/2,#}]&,outTables,{3}];
                safeExport[
                        blockTablePath[
                                tableDir, namePrefix, twojs, delta12, delta43,
                                twoj12, twoj43, q4pmstruct, twoj, Lambda,
                                keptPoleOrder, order],
                        outTables
                ]
        ];


varsMap=<|"zzb"->(applyStandardPrefactor[
        \[CapitalDelta]]@\[Rho]\[Rho]b2zzb@l\[Rho]l\[Rho]b2\[Rho]\[Rho]b@\[Lambda]lr2l\[Rho]l\[Rho]b[#]&),
"xt"->(zzb2xt@applyStandardPrefactor[
        \[CapitalDelta]]@\[Rho]\[Rho]b2zzb@l\[Rho]l\[Rho]b2\[Rho]\[Rho]b@\[Lambda]lr2l\[Rho]l\[Rho]b[#]&),
"yyb"->(zzb2yyb@applyStandardPrefactor[
        \[CapitalDelta]]@\[Rho]\[Rho]b2zzb@l\[Rho]l\[Rho]b2\[Rho]\[Rho]b@\[Lambda]lr2l\[Rho]l\[Rho]b[#]&),
"ws"->(yyb2ws@zzb2yyb@applyStandardPrefactor[
        \[CapitalDelta]]@\[Rho]\[Rho]b2zzb@l\[Rho]l\[Rho]b2\[Rho]\[Rho]b@\[Lambda]lr2l\[Rho]l\[Rho]b[#]&)|>


radialVarsMap=<|"xt"->(radialOnlyPrefactorRO[\[CapitalDelta]]@\[Rho]2zRO@l\[Rho]2\[Rho]RO[#]&),"ws"->(z2yRO@radialOnlyPrefactorRO[\[CapitalDelta]]@\[Rho]2zRO@l\[Rho]2\[Rho]RO[#]&)|>

writeConvertedBlocks[var_][
        partialTables_, tableDir_, twojs_,
                     delta12LowPrecision_, delta43LowPrecision_,
                     twoj12_, twoj43_, q4pmstruct_,
                     twoj_, twojIndex_, Lambda_,
                     keptPoleOrder_, order_,
                     delta1plus2LowPrecision_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec],
                        delta1plus2 = SetPrecision[delta1plus2LowPrecision, prec]
                },
                derivs=Map[(varsMap[var]/.\[CapitalDelta]->delta1plus2)[
                        {(1-q4pmstruct[[2]])/2,#}]&,
                                                                         Map[Function[safeTranspose[#,{3,1,2}]],
                                                                             Transpose[Table[partialTables[[n,twojIndex,ppindex]],
                                                                                             {n,Length[partialTables]},
                                                                                             {ppindex, {1, 2}}]]
                                                                         ],
                           {3}
                       ];
                Do[
                   Do[
                      Do[
                         Do[
                            Do[
                               Do[
                                  Do[
                                       Print["0 ",twojIndex-1," ",i-1," ",j-1," ",k-1," ",l-1," ",m-1," ",n-1
                                            ," ",o-1," ",
                                             CoefficientList[derivs[[i]][[j,k,l]][[m]][[n]],x][[o]]
                                       ],
                                       {o,Length[CoefficientList[derivs[[i]][[j,k,l]][[m]][[n]],x]]}
                                  ],
                                  {n,Length[derivs[[i]][[j,k,l]][[m]]]}
                               ],
                               {m,Length[derivs[[i]][[j,k,l]]]}
                            ],
                            {l,Dimensions[derivs[[i]]][[3]]}
                         ],
                         {k,Dimensions[derivs[[i]]][[2]]}
                      ],
                      {j,Dimensions[derivs[[i]]][[1]]}
                   ],
                   {i,Length[derivs]}
                ];
        ]


writeRadialOnlyBlocks[var_][
        partialTables_, tableDir_, twojs_,
                     delta12LowPrecision_, delta43LowPrecision_,
                     twoj12_, twoj43_, q4pmstruct_,
                     twoj_, twojIndex_, Lambda_,
                     keptPoleOrder_, order_,
                     delta1plus2LowPrecision_]:=
        Module[
                {
                        delta12 = SetPrecision[delta12LowPrecision, prec],
                        delta43 = SetPrecision[delta43LowPrecision, prec],
                        delta1plus2 = SetPrecision[delta1plus2LowPrecision, prec]
                },
                radialVar=Map[Function
                              [(radialVarsMap[var]/.\[CapitalDelta]->delta1plus2)[
                                      {(1-q4pmstruct[[2]])/2,#}]],
                              Map[Function[safeTranspose[#,{3,1,2}]],
                                  Transpose[Table[partialTables[[n,twojIndex,ppindex]],
                                                  {n,{(1-q4pmstruct[[2]])/2 + 1}},
                                                  {ppindex, {1, 2}}]]
                              ]
                            ,
                              {3}
                          ];
                (* Exit[0]; *)
                Do[
                   Do[
                      Do[
                         Do[
                            Do[
                               Do[
                                  Do[
                                       Print["0 ",twojIndex-1," ",i-1," ",j-1," ",k-1," ",l-1," ",m-1," ",n-1
                                            ," ",o-1," ",
                                             CoefficientList[radialVar[[i]][[j,k,l]][[m]][[n]],x][[o]]
                                       ],
                                       {o,Length[CoefficientList[radialVar[[i]][[j,k,l]][[m]][[n]],x]]}
                                  ],
                                  {n,Length[radialVar[[i]][[j,k,l]][[m]]]}
                               ],
                               {m,Length[radialVar[[i]][[j,k,l]]]}
                            ],
                            {l,Dimensions[radialVar[[i]]][[3]]}
                         ],
                         {k,Dimensions[radialVar[[i]]][[2]]}
                      ],
                      {j,Dimensions[radialVar[[i]]][[1]]}
                   ],
                   {i,Length[radialVar]}
                ];
        ]
