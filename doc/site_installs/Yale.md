# Yale Grace HPC

- [Yale HPC documentation](https://docs.ycrc.yale.edu/clusters-at-yale/)

## Load modules

    module load Boost/1.83.0-GCC-12.2.0 CMake/3.24.3-GCCcore-12.2.0 GMP/6.2.1-GCCcore-12.2.0 Eigen/3.4.0-GCCcore-12.2.0

You may run `module -t list` to view loaded modules,
and `module purge` to unload all modules.

## Install

### fmt

    git clone --branch 10.1.1 https://github.com/fmtlib/fmt.git
    cd fmt
    mkdir build
    cd build
    cmake .. -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$HOME/install
    make && make install
    cd ../..

### blocks_3d

    git clone https://gitlab.com/bootstrapcollaboration/blocks_3d.git
    cd blocks_3d
    python3 ./waf configure --prefix=$HOME/install --fmt-dir=$HOME/install
    python3 ./waf # -j1
    ./test/run_test.sh
    python3 ./waf install
    cd ..
