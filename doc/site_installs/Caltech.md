# Caltech HPC

- [Caltech HPC documentation](https://hpc.sites.caltech.edu/documentation)
- [Software and Modules](https://hpc.sites.caltech.edu/documentation/software-and-modules)

## Load modules

    module load boost/1.84.0-gcc-11.3.1-zauawkb gcc/13.2.0-gcc-13.2.0-w55nxkl eigen/3.4.0-gcc-13.2.0-zf6tgxp

You may run `module -t list` to view loaded modules,
and `module purge` to unload all modules.

## Install

### fmt

    git clone --branch 10.1.1 https://github.com/fmtlib/fmt.git
    cd fmt
    mkdir build
    cd build
    cmake .. -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$HOME/install
    make && make install
    cd ../..

### blocks_3d

    git clone https://gitlab.com/bootstrapcollaboration/blocks_3d.git
    cd blocks_3d
    ./waf configure --prefix=$HOME/install --eigen-incdir=/software/eigen-b3f3d4950030/ --fmt-dir=$HOME/install --fmt-libdir=$HOME/install/lib64 --gmp-dir=`pkg-config --variable=prefix gmp`
    ./waf -j 1
    ./test/run_test.sh
    ./waf install
    cd ..
