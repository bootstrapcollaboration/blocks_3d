# Expanse HPC

- [Expanse HPC User Guide](https://www.sdsc.edu/support/user_guides/expanse.html)

## Load modules

    module load cpu/0.15.4 intel/19.1.1.217 eigen/3.3.7 gcc/10.2.0 cmake/3.18.2 boost/1.74.0 gmp/6.1.2

You may run `module -t list` to view loaded modules,
and `module purge` to unload all modules.

## Install

### fmt

    git clone --branch 10.1.1 https://github.com/fmtlib/fmt.git
    cd fmt
    mkdir build
    cd build
    cmake .. -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$HOME/install
    make && make install
    cd ../..

### blocks_3d

    git clone https://gitlab.com/bootstrapcollaboration/blocks_3d.git
    cd blocks_3d
    ./waf configure --prefix=$HOME/install --eigen-incdir=/cm/shared/apps/spack/cpu/opt/spack/linux-centos8-zen2/intel-19.1.1.217/eigen-3.3.7-plaog3szjnn3gh6wq5co55xxjuswwo7f/include/eigen3 --fmt-dir=$HOME/install --fmt-libdir=$HOME/install/lib64
    ./waf -j 1
    ./test/run_test.sh
    ./waf install
    cd ..
