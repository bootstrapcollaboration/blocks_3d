\documentclass[11pt, oneside]{article}   	

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amsbsy}
\usepackage{array}
\usepackage[linesnumbered,lined,boxed,commentsnumbered]{algorithm2e}

\usepackage{mathtools}
\usepackage[usenames,dvipsnames]{xcolor}

\usepackage{subcaption}

\usepackage{dsdshorthand}


\makeatletter
\def\@fpheader{\ }
\makeatother

\title{General 3d conformal blocks}
\date{}


\begin{document}

\maketitle

\section{Conventions for the blocks}

The conventions used by \verb|blocks_3d| are precisely those of~\cite{Erramilli:2019njx} with a few modifications described below. We use the terminology and notation from~\cite{Erramilli:2019njx} freely. Refer to section 6 of~\cite{Erramilli:2019njx} for a brief summary of these conventions.

The code computes the derivatives
\be 
\ptl_{x_1}^m\ptl_{x_2}^n \p{p_\pm(x_1,x_2)g_{\De,j,[q_1q_2q_3q_4],\pm}^{(j_{12},j_{12\cO}),(j_{43},j_{43\cO})}(z,\bar z)}\Big\vert_{z=\bar z=\half}
\ee
where $x_1,x_2$ are some coordinates chosen by the user from the options described below (for example, $z,\bar z$), and $m,n$ are constrained so that the set of derivatives obtained is equivalent to $\ptl_z^m\ptl_{\bar z}^n$ with $m+n\leq \L$ (more precise definition below). The factor $p_\pm(x_1,x_2)$ depends on the choice of coordinates and is described below.

The functions 
\be\label{eq:newfns0}
g_{\De,j,[q_1q_2q_3q_4],\pm}^{(j_{12},j_{12\cO}),(j_{43},j_{43\cO})}(z,\bar z)
\ee
are a modification of the functions 
\be\label{eq:oldfns}
g_{\De,j,[q_1q_2q_3q_4]}^{(j_{12},j_{12\cO}),(j_{43},j_{43\cO})}(z,\bar z)
\ee
defined in~\cite{Erramilli:2019njx}. Note that the latter include the factor 
\be
(z\bar z)^{-\frac{\De_1+\De_2}{2}}
\ee
which is typically omitted from conformal blocks. This factor is controlled by~\texttt{--delta1-plus-2} option to \texttt{blocks\_3d}, and is by default omitted (i.e. \texttt{--delta1-plus-2}$=0$, equivalently $\De_1+\De_2=0$).

There are two reasons for the modification from~\eqref{eq:oldfns} to~\eqref{eq:newfns0}. First, the functions~\eqref{eq:oldfns} are not necessarily real. Second, they do not have a definite parity under exchange $z\leftrightarrow \bar z$. For this reason we define
\be\label{eq:newfns}
g_{\De,j,[q_1q_2q_3q_4],\pm}^{(j_{12},j_{12\cO}),(j_{43},j_{43\cO})}(z,\bar z)	
=&\frac{i^{-F_\cO}}{2}\Big(g_{\De,j,[q_1q_2q_3q_4]}^{(j_{12},j_{12\cO}),(j_{43},j_{43\cO})}(z,\bar z) \nn\\
&\pm(-1)^{\sum_{i=1}^4 j_i}g_{\De,j,[-q_1-q_2-q_3-q_4]}^{(j_{12},j_{12\cO}),(j_{43},j_{43\cO})}(z,\bar z)\Big),
\ee
where $F_\cO\in\{0,1\}$ is the fermion number of the exchanged operator $\cO$. The functions~\eqref{eq:newfns} are always
real and are even under $z\leftrightarrow \bar z$ for $(+)$ sign and odd for $(-)$ sign.

Each derivative is approximated by
\be
	r^\De P(x)/Q(x),
\ee
where $x=\De-\De_0$, $r=3-2\sqrt{2}$, $P(x)$ is a polynomial and $Q(x)=\prod_i(x-x_i)$. The value $\De_0$ is chosen so that $x\geq 0$ is the unitarity bound. The value of $\De_0$ is reported in the output \texttt{JSON} file in the field \texttt{delta\_minus\_x}. Similarly, the set of poles $x_i$ is reported in the field \texttt{pole\_list\_x}. In general, the set of poles $x_i$ consists of the poles in Table~1 of~\cite{Erramilli:2019njx} for which $n_i\leq$\texttt{kept\_pole\_order}, appropriately shifted by $\De_0$. In any event, the values in the output files supersede the definitions given here.

\subsection{Choices of coordinates}

We use the definition $\L_+=\L$ and $\L_-=\L-1$.
\begin{itemize}
	\item \texttt{zzb}: Derivatives $\ptl_z^m\ptl_{\bar z}^n$ with $m+n\leq \L$, only $n\leq m$ are reported due to the symmetry~$z\leftrightarrow \bar z$.
	The function $p_\pm(z,\bar z)\equiv 1$.
	\item \texttt{xt}: Derivatives $\ptl_x^m\ptl_{t}^n$ with $m+2n\leq \L_\pm$. The function $p_+(x,t)\equiv 1$, $p_-(x,t)\equiv 2/(z-\bar z)$. The coordinates are defined as
	\be
		x=\frac{z+\bar z-1}{2}, \quad t=\p{\frac{z-\bar z}{2}}^2.
	\ee
	The function $p_\pm(x,t)$ is needed to obtain a smooth function of $x,t$ near $t=0$. 
	\item \texttt{xt\_radial}: Same as \texttt{xt}, but only $n=0$ (i.e.\ no $t$-derivatives) is computed and reported.
	
	\item \texttt{yyb}: Derivatives $\ptl_y^m\ptl_{\bar y}^n$ with $m+n\leq \L$, only $n\leq m$ are reported due to the symmetry~$y\leftrightarrow \bar y$.
	The function $p_\pm(y,\bar y)\equiv 1$. The coordinates are defined by
	\be
		z&=\frac{(1+y)^2}{2(1+y^2)},\qquad
		\bar z=\frac{(1+\bar y)^2}{2(1+\bar y^2)}.
	\ee
	These coordinates map $z\in \C\setminus[1,+\oo)$ to $y$ in the unit disc, and $z=\bar z=\half$ to $y=\bar y=0$.
	\item \texttt{ws}: Derivatives $\ptl_w^m\ptl_{s}^n$ with $m+2n\leq \L_\pm$. The function $p_+(w,s)\equiv 1$, $p_-(w,s)\equiv 2/(y-\bar y)$. The coordinates are defined as
	\be
	w=\frac{y+\bar y}{2}, \quad s=\p{\frac{y-\bar y}{2}}^2.
	\ee
	The function $p_\pm(w,s)$ is needed to obtain a smooth function of $w,s$ near $s=0$. 
	\item \texttt{ws\_radial}: Same as \texttt{ws}, but only $n=0$ (i.e.\ no $s$-derivatives) is computed and reported.
\end{itemize}

\section{Approximation details}

The code computes the blocks using the following procedure. First, the $r$-expansion of the $\eta$-derivatives of the blocks on the diagonal $z=\bar z$ is computed, up to order $r^N$ which is supplied by the user through parameter \verb|--order|. This expansion is then used to determine
the derivatives near $z=\bar z=\half$, up to order $\L$ given by \verb|--lambda|. 

Then the approximation is simplified by only keeping the poles in the recursion relation whose residues start with $r^{n}$ with $n\leq\kappa$. The order $\kappa$ is supplied through \verb|--kept-pole-order|. The other poles are not discarded, but instead their contribution is approximated by adjusting the residues of the kept poles (``pole-shifting'') as described in~\cite{Kos:2013tga}. The poles at the unitarity bound are never discarded nor are their residues modified.


\section{Attribution}

The code of \verb|blocks_3d| is based on Zamolodchikov-like recursion relations for general 3d blocks derived in~\cite{Erramilli:2019njx}. The recursion relations were introduced in the context of scalar higher-dimensional blocks in~\cite{Kos:2013tga, Kos:2014bka}, and discussed in spinning case in~\cite{Penedones:2015aga}. The pole-shifting procedure was described in~\cite{Kos:2013tga}. 

\bibliographystyle{JHEP}
\bibliography{refs}

\end{document}  
