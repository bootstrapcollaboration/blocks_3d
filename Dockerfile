# syntax=docker/dockerfile:1

# This file contains three stages: 'build', 'install', 'test'
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#use-multi-stage-builds
#
# 'build' builds blocks_3d binaries. This image is heavy, because it contains all sources etc.
# 'install' contains only blocks_3d binaries (in /usr/local/bin/) + necessary dynamic libraries
# 'test' is based on 'install', but also copies blocks_3d/build/ and blocks_3d/test/ folders to /home/testuser/blocks_3d/
# - this allows to in order to run unit and integration tests by calling blocks_3d/test/run_all_tests.sh
# Final (lightweight) image is made from 'install' target.
#
# How to check that blocks_3d image works:
#
# docker build . --tag blocks_3d
# docker run blocks_3d blocks_3d --help
#
# How to run tests:
#
# docker build . --tag blocks_3d-test --target test
# docker run blocks_3d-test ./test/run_test.sh

FROM alpine:3.18 AS build

RUN apk add \
    boost-dev \
    eigen-dev \
    fmt-dev \
    g++ \
    gmp-dev \
    python3
WORKDIR /usr/local/src/blocks_3d
# Build blocks_3d from current sources
COPY . .
RUN ./waf configure --prefix=/usr/local && \
    python3 ./waf && \
    python3 ./waf install

# Take only blocks_3d binaries + load necessary dynamic libraries
FROM alpine:3.18 as install
RUN apk add \
    boost1.82-program_options \
    boost1.82-system \
    eigen \
    fmt \
    gmp \
    libgmpxx \
    libstdc++
COPY --from=build /usr/local/bin /usr/local/bin
COPY --from=build /usr/local/lib /usr/local/lib

# Separate test target, see
# https://docs.docker.com/language/java/run-tests/#multi-stage-dockerfile-for-testing
# Contains /home/testuser/blocks_3d/build and /home/testuser/blocks_3d/test folders,
# which is sufficient to run tests as shown below:
#
# docker build . --tag blocks_3d-test --target test
# docker run blocks_3d-test ./test/run_test.sh
FROM install as test
RUN apk add python3
# Create testuser to run Docker non-root
RUN addgroup --gid 10000 testgroup && \
    adduser --disabled-password --uid 10000 --ingroup testgroup --shell /bin/sh testuser
WORKDIR /home/testuser/blocks_3d
COPY --from=build /usr/local/src/blocks_3d/build build
COPY --from=build /usr/local/src/blocks_3d/test test
RUN chown -R testuser test
USER testuser:testgroup

# Resulting image
FROM install
# TODO best practices suggest to run containter as non-root.
# https://github.com/dnaprawa/dockerfile-best-practices#run-as-a-non-root-user
# But this requires some extra work with permissions when mounting folders for docker run.