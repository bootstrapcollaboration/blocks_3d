#include "../Bigfloat.hxx"
#include "../Q4pm.hxx"

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include <set>

void handle_input(const int &argc,
                  char *argv[],
                  std::string &delta_12_string,
                  std::string &delta_43_string,
                  int64_t &order,
                  std::array<int64_t, 4> &two_js,
                  std::string &composite_block_file,
                  Q4pm &q4pm,
                  int64_t &lambda_deriv,
                  size_t &precision_base_2,
                  size_t &num_threads,
                  std::string &output_file,
                  bool &debug,
                  bool &profile)
{
  namespace po = boost::program_options;

  po::options_description options("3D Blocks options");

  std::string j_external_string, q4_string;
  int64_t q4_sign;

  options.add_options()("help,h", "Show this helpful message.");
  options.add_options()(
    "j-external", po::value<std::string>(&j_external_string)->required(),
    "Comma separated list of 4 integer/half-integer spins of the external "
    "operators j₁,j₂,j₃,j₄ (e.g. '1,0.5,1,0.5').");
  options.add_options()("delta-12",
                        po::value<std::string>(&delta_12_string)->required(),
                        "Δ₁₂=Δ₁-Δ₂");
  options.add_options()("delta-43",
                        po::value<std::string>(&delta_43_string)->required(),
                        "Δ₄₃=Δ₄-Δ₃");
  options.add_options()("four-pt-struct",
                        po::value<std::string>(&q4_string)->required(),
                        "Comma separated list of 4 integers/half-integers "
                        "representing the four - point structure q₁, q₂, "
                        "q₃, q₄, (e.g.'1,0.5,1,0.5' or '0.5,0,0,-0.5') ");
  options.add_options()(
    "four-pt-sign", po::value<int64_t>(&q4_sign)->required(),
    "1 or -1 representing the sign of the four-point structure under z "
    "<-> zbar");
  options.add_options()("lambda_deriv", po::value<int64_t>(&lambda_deriv)->required(),
                        "Number of derivatives with respect to lambda=log(rho/rhobar)/2");
  options.add_options()("order", po::value<int64_t>(&order)->required(),
                        "Depth of recursion");
  options.add_options()("num-threads",
                        po::value<size_t>(&num_threads)->required(),
                        "Number of threads");
  options.add_options()("precision",
                        po::value<size_t>(&precision_base_2)->required(),
                        "The precision, in the number of bits, for "
                        "numbers in the computation.");
  options.add_options()(
    ",c", po::value<std::string>(&composite_block_file)->required(),
    "array of composite blocks in json format");
  options.add_options()(
    ",o", po::value<std::string>(&output_file)->required(),
    "output file, will be written in mathematica format");
  options.add_options()("debug", po::value<bool>(&debug)->default_value(false),
                        "Write out debugging output.");
  options.add_options()(
    "profile", po::value<bool>(&profile)->default_value(true),
    "Write profiling file. The filename is obtained by taking the "
    "output file with j_internal = 0 and appending .profile to it.");

  po::variables_map variables_map;

  po::store(po::parse_command_line(argc, argv, options), variables_map);
  if(variables_map.count("help"))
    {
      std::cout << options << '\n';
      exit(0);
    }
  po::notify(variables_map);

  // Round up the size to work around bugs in boost::multiprecision
  const size_t precision_base_2_rounded_up(
    (((precision_base_2 - 1) / (sizeof(mp_limb_t) * 8)) + 1)
    * sizeof(mp_limb_t) * 8);
  const size_t precision_base_10(
    boost::multiprecision::detail::digits2_2_10(precision_base_2_rounded_up));
  boost::multiprecision::mpf_float::default_precision(precision_base_10);

  two_js = ([&] {
    std::vector<std::string> string_vector;
    boost::split(string_vector, j_external_string, boost::is_any_of(","));
    if(string_vector.size() != 4)
      {
        throw std::runtime_error("wrong number of elements supplied to "
                                 "'j-external' option.  Expected 4, "
                                 "but got: "
                                 + std::to_string(string_vector.size()));
      }
    return std::array<int64_t, 4>(
      {static_cast<int64_t>(2 * std::stod(string_vector[0])),
       static_cast<int64_t>(2 * std::stod(string_vector[1])),
       static_cast<int64_t>(2 * std::stod(string_vector[2])),
       static_cast<int64_t>(2 * std::stod(string_vector[3]))});
  }());

  q4pm = ([&] {
    std::vector<std::string> string_vector;
    boost::split(string_vector, q4_string, boost::is_any_of(","));
    if(string_vector.size() != 4)
      {
        throw std::runtime_error(
          "wrong number of elements supplied to 'four-pt-struct' option.  "
          "Expected 4, but got: "
          + std::to_string(string_vector.size()));
      }
    if(q4_sign != 1 && q4_sign != -1)
      {
        throw std::runtime_error("Four-pt-sign must be 1 or -1, but got: "
                                 + std::to_string(q4_sign));
      }
    Q4pm result({static_cast<int64_t>(2 * std::stod(string_vector[0])),
                 static_cast<int64_t>(2 * std::stod(string_vector[1])),
                 static_cast<int64_t>(2 * std::stod(string_vector[2])),
                 static_cast<int64_t>(2 * std::stod(string_vector[3]))},
                q4_sign);

    for(size_t index(0); index < 4; ++index)
      {
        if((result.vec[index] - two_js[index]) % 2 != 0)
          {
            throw std::runtime_error("For each element i, j_external[i] - "
                                     "four_pt_struct[i] must be "
                                     "an integer, but in index "
                                     + std::to_string(index)
                                     + " found j_external[i] = "
                                     + std::to_string(two_js[index] / 2.0)
                                     + ", q4[i] = " + string_vector[index]);
          }
        if(result.vec[index] < -two_js[index]
           || result.vec[index] > two_js[index])
          {
            throw std::runtime_error("For each element i, "
                                     "-j_external[i] <= four_pt_struct[i] "
                                     "<= j_external[i], but in index "
                                     + std::to_string(index)
                                     + " found j_internal[i] = "
                                     + std::to_string(two_js[index] / 2.0)
                                     + ", q4[i] = " + string_vector[index]);
          }
      }
    return result;
  }());
}
