// #include "../Log_r_Derivatives.hxx"
#include "../Q4pm.hxx"
#include "../Coordinate.hxx"
#include "../lambda_log_r_tables/lambda_log_r_deriv/Delta_Fraction.hxx"
#include "../lambda_log_r_tables/Cache.hxx"
#include "../so3_tensor_parity.hxx"
#include "../compute_pseudo_parity_types.hxx"
#include "../timers/Timers.hxx"

#include <fmt/format.h>
#include <boost/math/tools/polynomial.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp> // needed for boost >= 1.79
#include <boost/multiprecision/gmp.hpp>
#include <numeric>
#include <utility>
#include <set>
#include <sstream>

// *********** This hasn't been updated yet for new residue addressing *************

void handle_input(const int &argc,
                  char *argv[],
                  std::string &delta_12_string,
                  std::string &delta_43_string,
                  int64_t &order,
                  std::array<int64_t, 4> &two_js,
                  std::string &composite_block_file,
                  Q4pm &q4pm,
                  int64_t &lambda_deriv,
                  size_t &precision_base_2,
                  size_t &num_threads,
                  std::string &output_file,
                  bool &debug,
                  bool &profile);

void fill_binomials(const Bigfloat &delta_12, const Bigfloat &delta_43,
                    const std::array<int64_t, 4> &q4, const int64_t &order,
                    std::vector<Bigfloat> &binomial_a,
                    std::vector<Bigfloat> &binomial_ab,
                    std::vector<Bigfloat> &binomial_b,
                    std::vector<Bigfloat> &binomial_bb);

std::vector<std::vector<Bigfloat>>
h_infinity_double_series(const int64_t &order,
                         const std::vector<Bigfloat> &binomial_a,
                         const std::vector<Bigfloat> &binomial_ab,
                         const std::vector<Bigfloat> &binomial_b,
                         const std::vector<Bigfloat> &binomial_bb);

std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
h_infinity(const std::array<int64_t, 4> &two_js,
           const std::vector<std::vector<Bigfloat>> &series,
           const int64_t &two_j12, const int64_t &two_j43,
           const std::array<int64_t, 4> &q4, const int64_t &i,
           const int64_t &two_j_max, const int64_t &order, const int64_t &n,
           const F_SO3_Basis_Derivative &f_so3_basis_derivative,
           const size_t &num_threads, Timers &timers);

std::vector<
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
radial_recursion(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const M_Matrix &m_matrix, const Pole_Types &pole_types,
  const size_t &num_threads);

template <typename T> T negate(const T &vec)
{
  T result(vec);
  for(auto &v : result)
    {
      v = -v;
    }
  return result;
}

class PartialFraction {
public:
  Bigfloat constant;
  std::vector<std::pair<Bigfloat, Bigfloat>> pole_residues;
  PartialFraction(const Bigfloat &constant,
                  const std::vector<std::pair<Bigfloat, Bigfloat>> &pole_residues):
    constant(constant),
    pole_residues(pole_residues)
  {}
};

Bigfloat
eval_partial_fraction_no_pole(const PartialFraction &pf, const Bigfloat x)
{
  // TODO: better definition of epsilon
  Bigfloat epsilon("1e-50");
  Bigfloat result = pf.constant;
  for (size_t i = 0; i < pf.pole_residues.size(); ++i)
    {
      Bigfloat pole_loc = pf.pole_residues[i].first;
      Bigfloat pole_res = pf.pole_residues[i].second;
      if (abs(x - pole_loc) > epsilon) {
        result += pole_res / (x - pole_loc);
      }
    }
  return result;
}

typedef std::pair<int64_t,int64_t> SO3StructPair;
typedef std::map<int64_t,std::map<SO3StructPair,std::vector<PartialFraction>>> BlockSeriesMap;

BlockSeriesMap
conformal_block_series(const std::array<int64_t, 4> &two_js,
                       const Bigfloat &delta_12,
                       const int64_t &two_j12,
                       const Bigfloat &delta_43,
                       const int64_t &two_j43,
                       const Q4pm &q4pm,
                       const std::set<int64_t> &two_js_intermediate,
                       const int64_t &order,
                       const int64_t &lambda_deriv,
                       const M_Matrix &m_matrix,
                       const Pole_Types &pole_types,
                       const F_SO3_Basis_Derivative &f_so3_basis_derivative,
                       const size_t &num_threads,
                       Timers &timers)
{
  // This code is copied from the definition of
  // conformal_block_derivatives. TODO: deduplicate.
  
  timers.enter("conformal_block_derivatives");

  Timer &init_timer = timers.add_and_start("init");

  // find the two combinations of pseudoparities that need to be computed
  const std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4pm.vec,
                                {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));

  // precompute some binomial coefficients and power series for h_infinity
  std::vector<Bigfloat> binomial_a, binomial_ab, binomial_b, binomial_bb;
  fill_binomials(delta_12, delta_43, q4pm.vec, order, binomial_a, binomial_ab,
                 binomial_b, binomial_bb);

  const std::vector<std::vector<Bigfloat>> series(h_infinity_double_series(
    order, binomial_a, binomial_ab, binomial_b, binomial_bb)),
    series_b(h_infinity_double_series(order, binomial_ab, binomial_a,
                                      binomial_bb, binomial_b));

  const int64_t two_j_max(*two_js_intermediate.rbegin());

  init_timer.stop();

  // compute h_infinity for q4pm and -q4pm
  auto &h_inf_timer = timers.add_and_start("h_infinity");
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    h_inf(h_infinity(two_js, series, two_j12, two_j43, q4pm.vec, 0, two_j_max,
                     order, lambda_deriv, f_so3_basis_derivative, num_threads, timers));
  const std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    h_temp(h_infinity(two_js, series_b, two_j12, two_j43, negate(q4pm.vec), 1,
                      two_j_max, order, lambda_deriv, f_so3_basis_derivative, num_threads, timers));
  h_inf_timer.stop();

  Timer &combine_timer = timers.add_and_start("combine_h_infinities");

  const int64_t factor(
    q4pm.i
    * (std::accumulate(two_js.begin(), two_js.end(), 0) % 4 == 0 ? 1 : -1));

  // combine into h_infinity of definite y-parity
  for(size_t pp_index(0); pp_index != h_inf.size(); ++pp_index)
    for(size_t r_order(0); r_order != h_inf[pp_index].size(); ++r_order)
      for(size_t two_j(0); two_j != h_inf[pp_index][r_order].size(); ++two_j)
        for(size_t two_j120(0);
            two_j120 != h_inf[pp_index][r_order][two_j].size(); ++two_j120)
          for(size_t two_j430(0);
              two_j430 != h_inf[pp_index][r_order][two_j][two_j120].size();
              ++two_j430)
            {
              // The factor of 1/2 was introduced relative to original
              // Mathematica prototype in order to match scalar_blocks
              // (Mathematica code now also has this factor)
              h_inf[pp_index][r_order][two_j][two_j120][two_j430]
                += factor
                   * h_temp[pp_index][r_order][two_j][two_j120][two_j430];
              h_inf[pp_index][r_order][two_j][two_j120][two_j430] *= 0.5;
            }

  combine_timer.stop();

  // using h_infinity as the initial condition, perform radial recursion to
  // obtain power series in r; most of the magic is here
  auto &recursion_timer = timers.add_and_start("recursion");
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    residues(radial_recursion(h_inf, two_j_max, two_j12, two_j43,
                              pseudo_parity_types, order, m_matrix, pole_types,
                              num_threads));
  recursion_timer.stop();

  // End code copied from conformal_block_derivatives
  
  BlockSeriesMap result;

  for(auto &two_j : two_js_intermediate)
    {
      // two_js_intermediate may skip values of two_j, but h_inf and
      // residues both cover the entire range.  So we compute an index
      // for two_j based on its value.
      const size_t two_j_index(two_j / 2);
      std::map<std::pair<int64_t,int64_t>,std::vector<PartialFraction>> struct_map;

      for(size_t pp_index(0); pp_index != 2; ++pp_index)
        {
          std::vector<int64_t> j120s(so3_tensor_pseudo_parity(pseudo_parity_types[pp_index][0], two_j, two_j12));
          std::vector<int64_t> j430s(so3_tensor_pseudo_parity(pseudo_parity_types[pp_index][1], two_j, two_j43));

          for (size_t left_so3_index(0); left_so3_index != j120s.size(); ++left_so3_index)
            {
              for (size_t right_so3_index(0); right_so3_index != j430s.size(); ++right_so3_index)
                {
                  int64_t j120 = j120s[left_so3_index];
                  int64_t j430 = j430s[right_so3_index];
                  SO3StructPair struct_pair(j120,j430);

                  std::vector<PartialFraction> terms;
                  for (int64_t i = 0; i < order + 1; ++i)
                    {
                      const Bigfloat constant =
                        h_inf.at(pp_index)
                        .at(i)
                        .at(two_j_index)
                        .at(left_so3_index)
                        .at(right_so3_index);

                      const std::vector<Pole> &poles(pole_types.eval(two_j, two_j12, two_j43, order));
                      std::vector<std::pair<Bigfloat,Bigfloat>> pole_residues;
                      for (size_t pole_index = 0; pole_index < poles.size(); ++pole_index)
                        {
                          int64_t r_shift = poles[pole_index].shift();
                          int64_t ii = i - r_shift;
                          if (ii >= 0)
                            {
                              const Bigfloat residue = 
                                residues.at(pp_index)
                                .at(ii)
                                .at(two_j_index)
                                .at(Pole_Types::position(
                                  two_j, two_j12, two_j43,
                                  order - ii, poles[pole_index]))
                                .at(left_so3_index)
                                .at(right_so3_index);
                              pole_residues.emplace_back(std::make_pair(poles[pole_index].delta(two_j),
                                                                        residue));
                            }
                        }
                      const PartialFraction pf(constant,pole_residues);
                      terms.emplace_back(pf);
                    }

                  struct_map.emplace(struct_pair, terms);
                }
            }
        }
      result.emplace(two_j, struct_map);
    }
  recursion_timer.stop();
  timers.exit();
  return result;
}

typedef std::pair<int64_t,int64_t> SO3Struct;

typedef std::vector<std::pair<SO3Struct,Bigfloat>> CompositeSO3Struct;

CompositeSO3Struct
parse_CompositeSO3Struct(const boost::property_tree::ptree& pt)
{
  CompositeSO3Struct composite_so3_struct;

  for (const auto& item : pt) {
    const auto& so3Node = item.second.get_child("so3_struct");
    int64_t two_j12 = so3Node.get<int64_t>("two_j12");
    int64_t two_j120 = so3Node.get<int64_t>("two_j120");
    SO3Struct so3_struct(two_j12, two_j120);
    
    std::string coefficient_string = item.second.get<std::string>("coefficient");
    Bigfloat coefficient(coefficient_string);
    
    composite_so3_struct.push_back({so3_struct, coefficient});
  }

  return composite_so3_struct;
}

class Series {
public:
  std::vector<Bigfloat> coefficients;

  Series(int64_t order):
    coefficients(order+1)
  {}
};

class CompositeBlock {
public:
  Bigfloat delta;
  int64_t two_j;
  CompositeSO3Struct composite_struct_left;
  CompositeSO3Struct composite_struct_right;

  CompositeBlock(const Bigfloat &delta,
                 const int64_t two_j,
                 const CompositeSO3Struct &composite_struct_left,
                 const CompositeSO3Struct &composite_struct_right):
    delta(delta),
    two_j(two_j),
    composite_struct_left(composite_struct_left),
    composite_struct_right(composite_struct_right)
  {}
};

CompositeBlock
parse_CompositeBlock(const boost::property_tree::ptree& pt)
{
  Bigfloat delta(pt.get<std::string>("delta"));
  int64_t two_j = pt.get<int64_t>("two_j");

  const auto& composite_struct_left_pt = pt.get_child("composite_struct_left");
  const auto& composite_struct_right_pt = pt.get_child("composite_struct_right");

  CompositeSO3Struct composite_struct_left = parse_CompositeSO3Struct(composite_struct_left_pt);
  CompositeSO3Struct composite_struct_right = parse_CompositeSO3Struct(composite_struct_right_pt);

  return CompositeBlock(delta, two_j, composite_struct_left, composite_struct_right);
}

std::vector<CompositeBlock>
parse_CompositeBlocks_from_file(const std::string& filename)
{
  std::vector<CompositeBlock> composite_blocks;
  
  boost::property_tree::ptree pt;
  read_json(filename, pt);
  
  for (const auto& item : pt) {
    CompositeBlock block = parse_CompositeBlock(item.second);
    composite_blocks.push_back(block);
  }
  
  return composite_blocks;
}

class BlockSeriesCache {
public:
  std::array<int64_t, 4> two_js;
  Bigfloat delta_12;
  Bigfloat delta_43;
  Q4pm q4pm;
  std::set<int64_t> two_js_intermediate;
  int64_t order;
  int64_t lambda_deriv;
  size_t num_threads;
  Timers &timers;
  std::map<std::pair<int64_t,int64_t>,BlockSeriesMap> block_struct_map;
  BlockSeriesCache(const std::array<int64_t, 4> &two_js,
                   const Bigfloat &delta_12,
                   const Bigfloat &delta_43,
                   const Q4pm &q4pm,
                   const std::set<int64_t> &two_js_intermediate,
                   const int64_t order,
                   const int64_t lambda_deriv,
                   const size_t &num_threads,
                   Timers &timers_ref):
    two_js(two_js),
    delta_12(delta_12),
    delta_43(delta_43),
    q4pm(q4pm),
    two_js_intermediate(two_js_intermediate),
    order(order),
    lambda_deriv(lambda_deriv),
    num_threads(num_threads),
    timers(timers_ref)
  {}

  BlockSeriesMap &get_block(int64_t two_j12, int64_t two_j43)
  {
    std::pair<int64_t,int64_t> struct_key(two_j12, two_j43);
    if (block_struct_map.find(struct_key) == block_struct_map.end()) {

      // Cache initialization basically copied from
      // lambda_log_r_table. TODO: deduplicate?  TODO: Some of these
      // caches, like factorial and wigner don't need to be computed
      // each time. We could factor them out.

      Timer &cache_init = timers.add_and_start("cache_init");
      timers.enter("cache_init");

      // Define caches and initialize them
      Timer &construtors = timers.add_and_start("constructors");
      Clebsch_Gordan clebsch_gordan_cache;
      Factorial factorial;
      M_Matrix m_matrix(delta_12, delta_43);
      Wigner_Cache wigner_cache;
      F_Q_Basis_Derivative f_q_basis_derivative;
      F_W_Conversion_Matrix f_w_conversion_matrix;
      F_SO3_Basis_Derivative f_so3_basis_derivative;
      construtors.stop();

      int64_t two_j_max = *two_js_intermediate.rbegin();

      Pole_Types pole_types;
      pole_types.initialize(two_j_max, two_j12, two_j43, order);

      Timer &factorial_timer = timers.add_and_start("factorial");
      factorial.initialize(two_j_max + 2 * order + 2);
      factorial_timer.stop();
      std::cout << "Done with factorial" << std::endl;

      Timer &wigner_timer = timers.add_and_start("wigner_cache");
      wigner_cache.initialize(two_j_max + 2 * order, two_js, q4pm.vec,
                              lambda_deriv,
                              factorial);
      wigner_timer.stop();
      std::cout << "Done with wigner_cache" << std::endl;

      Timer &cg_timer = timers.add_and_start("clebsch_gordan_cache");
      clebsch_gordan_cache.initialize(two_j12, two_j_max + 2 * order, factorial);
      clebsch_gordan_cache.initialize(two_j43, two_j_max + 2 * order, factorial);
      cg_timer.stop();
      std::cout << "Done with clebsch_gordan_cache" << std::endl;

      Timer &m_matrix_timer = timers.add_and_start("m_matrix");
      m_matrix.initialize(two_j12, two_j43, two_j_max,
                          four_point_parity(two_js, q4pm.vec), order, pole_types,
                          clebsch_gordan_cache, factorial);
      m_matrix_timer.stop();
      std::cout << "Done with m_matrix" << std::endl;

      Timer &f_q_timer = timers.add_and_start("f_q_basis_derivative");
      f_q_basis_derivative.initialize(two_js, two_j_max + 2 * order,
                                      lambda_deriv, 
                                      wigner_cache, timers);
      f_q_timer.stop();
      std::cout << "Done with f_q_basis_derivative" << std::endl;

      Timer &f_so3_timer = timers.add_and_start("f_so3_basis_derivative");
      f_so3_basis_derivative.initialize(
                                        two_js, two_j_max + 2 * order, two_j12, two_j43, q4pm.vec, lambda_deriv,
                                        clebsch_gordan_cache, factorial, f_q_basis_derivative,
                                        f_w_conversion_matrix);
      f_so3_timer.stop();
      std::cout << "Done with f_so3_basis_derivative" << std::endl;

      timers.exit();
      cache_init.stop();
      
      block_struct_map.emplace(struct_key,
                               conformal_block_series(two_js,
                                                      delta_12,
                                                      two_j12,
                                                      delta_43,
                                                      two_j43,
                                                      q4pm,
                                                      two_js_intermediate,
                                                      order,
                                                      lambda_deriv,
                                                      m_matrix,
                                                      pole_types,
                                                      f_so3_basis_derivative,
                                                      num_threads,
                                                      timers));

    }
    return block_struct_map.at(struct_key);
  }

  Series
  get_composite_block(const CompositeBlock &b)
  {
    Series result(order);
    Bigfloat four(4);
    Bigfloat four_to_the_delta = boost::multiprecision::pow(four, b.delta);

    for (auto struct_left: b.composite_struct_left)
      {
        int64_t two_j12 = struct_left.first.first;
        int64_t two_j120 = struct_left.first.second;
        Bigfloat coeff_left = struct_left.second;

        for (auto struct_right: b.composite_struct_right)
          {
            int64_t two_j43 = struct_right.first.first;
            int64_t two_j430 = struct_right.first.second;
            Bigfloat coeff_right = struct_right.second;

            BlockSeriesMap &block_series_map = get_block(two_j12, two_j43);

            const std::vector<PartialFraction> coeffs_pf =
              block_series_map.at(b.two_j).at(std::make_pair(two_j120, two_j430));

            for (size_t i = 0; i < coeffs_pf.size(); ++i)
              {
                result.coefficients[i] +=
                  four_to_the_delta *
                  coeff_left * coeff_right *
                  eval_partial_fraction_no_pole(coeffs_pf[i], b.delta);
              }
          }
      }
    return result;
  }
};

std::string replace_scientific_notation(const std::string &str) {
    std::string result = str;
    std::replace(result.begin(), result.end(), 'e', '*');
    std::replace(result.begin(), result.end(), 'E', '*');
    size_t pos = result.find("*");
    if (pos != std::string::npos) {
        result.insert(pos + 1, "^");
    }
    return result;
}

void write_Series_to_mathematica(const std::vector<Series> &data,
                                 const std::string &filename)
{
  std::cout << "Writing file: " << filename << std::endl;
  boost::filesystem::path outfile_path(filename);
  boost::filesystem::ofstream outfile(outfile_path);

  outfile << "{\n";
  
  for (size_t i = 0; i < data.size(); ++i)
    {
      const auto &series = data[i];
      outfile << "  {";
    
      for (size_t j = 0; j < series.coefficients.size(); ++j)
        {
          // Use string stream to output in scientific notation. Ensures trailing zeroes.
          std::stringstream ss;
          ss << std::setprecision(boost::multiprecision::mpf_float::default_precision()) << std::scientific << series.coefficients[j];
          outfile << replace_scientific_notation(ss.str());
          if (j < series.coefficients.size() - 1)
            {
              outfile << ", ";
            }
        }

      outfile << "}";
    
      if (i < data.size() - 1)
        {
          outfile << ",\n";
        }
    }

  outfile << "\n}";  // End of the outer list
  outfile.close();

}

int main(int argc, char *argv[])
{
  try
    {
      std::string delta_12_string, delta_43_string;
      int64_t order, lambda_deriv;
      std::array<int64_t, 4> two_js;
      std::string composite_block_file;
      Q4pm q4pm;
      size_t precision_base_2, num_threads;
      std::string output_file;
      bool debug, profile;

      handle_input(argc,
                   argv,
                   delta_12_string,
                   delta_43_string,
                   order,
                   two_js,
                   composite_block_file,
                   q4pm,
                   lambda_deriv,
                   precision_base_2,
                   num_threads,
                   output_file,
                   debug,
                   profile);
      const Bigfloat delta_12(delta_12_string), delta_43(delta_43_string);

      std::vector<CompositeBlock> composite_blocks =
        parse_CompositeBlocks_from_file(composite_block_file);

      std::set<int64_t> two_js_intermediate;
      for (const auto &composite_block: composite_blocks)
        {
          two_js_intermediate.insert(composite_block.two_j);
        }
      
      Timers timers(false);
      auto &main_timer(timers.add_and_start("main"));
      timers.enter("main");

      BlockSeriesCache block_series_cache(two_js,
                                          delta_12,
                                          delta_43,
                                          q4pm,
                                          two_js_intermediate,
                                          order,
                                          lambda_deriv,
                                          num_threads,
                                          timers);


      std::vector<Series> result;
      for (const auto &composite_block: composite_blocks)
        {
          result.emplace_back(block_series_cache.get_composite_block(composite_block));
        }

      main_timer.stop();
      write_Series_to_mathematica(result, output_file);
    }
  catch(std::exception &e)
    {
      std::cerr << "Error: " << e.what() << "\n";
      exit(1);
    }
  catch(...)
    {
      std::cerr << "Unknown error\n";
      exit(1);
    }
}
