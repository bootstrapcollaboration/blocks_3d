#include "../../../Log_r_Derivatives.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lambda_lr_to_lrho_lrhob(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>> &partial)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result;

  const int64_t order(
    partial.front()[two_j_index][parity_index][left_index][right_index]
      .numerator.size()
    - 1 + parity);

  const int64_t i_size(order + 1);
  result.resize(i_size);
  for(int64_t i(0); i != i_size; ++i)
    {
      const int64_t j_size(std::min(i, order - i) + 1);
      result[i].resize(j_size);
      for(int64_t j(0); j != j_size; ++j)
        {
          boost::math::tools::polynomial<Bigfloat> &sum(result[i][j]);
          sum.set_zero();
          for(int64_t k(0); k <= i; ++k)
            // We have to add '2' so that the mod '%' is always >=0
            for(int64_t l((i + j - k - parity + 2) % 2); l <= j; l += 2)
              {
                sum += ((j - l) % 2 == 0 ? 1 : -1) * pow(Bigfloat(2), (-i - j))
                       * binomial_coefficient(i, k)
                       * binomial_coefficient(j, l)
                       * partial[(i + j - k - l - parity) / 2][two_j_index]
                                [parity_index][left_index][right_index]
                                  .numerator[k + l];
              }
        }
    }
  return result;
}
