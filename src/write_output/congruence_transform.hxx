#pragma once

#include "Transforms.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>
#include <map>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
congruence_transform(
  const int64_t &parity, const Transforms &transforms,
  const Transforms::Index &index,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &input);
