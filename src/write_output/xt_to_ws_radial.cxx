#include "Transforms.hxx"
#include "../Log_r_Derivatives.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
xt_to_ws_radial(
  const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &xt_radial)
{
  // auto &xt_front(xt_radial.front());
  const int64_t order(xt_radial.size() - 1);
  const Eigen_Matrix &transform(transforms.get(order, Transforms::Index::z_y));

  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(
    xt_radial.size());
  for(auto &a : result)
    {
      a.reserve(1);
    }
  for(size_t row(0); row != xt_radial.size(); ++row)
    {
      result[row].emplace_back();
      auto &temp(result[row].back());
      for(size_t column(0); column != xt_radial.size(); ++column)
        {
          temp += transform(row, column) * xt_radial[column][0];
        }
    }
  return result;
}
