#include "../../../Eigen.hxx"

Eigen_Matrix one_minus_power_prefactor(const int64_t &order)
{
  Eigen_Matrix result(order + 1, order + 1);
  const Bigfloat one_minus_z(0.5), Delta(0.5);
  for(int64_t n(0); n <= order; ++n)
    for(int64_t k(0); k <= order; ++k)
      {
        if(n >= k)
          {
            result(n, k) = ((n - k) % 2 == 0 ? 1 : -1)
                           * factorial_power(-Delta, n - k)
                           * pow(one_minus_z, (k - n - Delta))
                           * binomial_coefficient(n, k);
          }
        else
          {
            result(n, k) = 0;
          }
      }
  return result;
}
