#include "../../../Eigen.hxx"

Eigen_Matrix z_y_transform(const int64_t &order)
{
  Eigen_Matrix z_y(order + 1, order + 1);
  for(int64_t m(0); m <= order; ++m)
    for(int64_t n(0); n <= order; ++n)
      {
        if((m - n) % 2 == 0)
          {
            z_y(m, n) = binomial_coefficient(-n, (m - n) / 2)
                        * factorial_slow(m) / factorial_slow(n);
          }
        else
          {
            z_y(m, n) = 0;
          }
      }
  return z_y;
}
