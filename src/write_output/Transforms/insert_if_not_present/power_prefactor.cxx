#include "../../../Eigen.hxx"

Eigen_Matrix power_prefactor(const int64_t &order, const Bigfloat &Delta)
{
  Eigen_Matrix result(order + 1, order + 1);
  const Bigfloat z(0.5);
  for(int64_t n(0); n <= order; ++n)
    for(int64_t k(0); k <= order; ++k)
      {
        if(n >= k)
          {
            result(n, k) = factorial_power(-Delta, n - k)
                           * pow(z, (k - n - Delta))
                           * binomial_coefficient(n, k);
          }
        else
          {
            result(n, k) = 0;
          }
      }
  return result;
}
