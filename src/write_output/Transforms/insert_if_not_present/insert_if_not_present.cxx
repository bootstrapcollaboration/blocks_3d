#include "../../Transforms.hxx"

Eigen_Matrix stirling_transform(const int64_t &order);
Eigen_Matrix rho_z_transform(const int64_t &order);
Eigen_Matrix z_y_transform(const int64_t &order);
Eigen_Matrix power_prefactor(const int64_t &order, const Bigfloat &Delta);
Eigen_Matrix one_minus_power_prefactor(const int64_t &order);

void Transforms::insert_if_not_present(const int64_t &order)
{
  if(map.find(order) == map.end())
    {
      Eigen_Matrix prefactor(power_prefactor(order, Delta / 2)),
        radial_prefactor(power_prefactor(order, Delta)),
        radial_prefactor_minus(power_prefactor(order, Delta+1)),
        one_minus(one_minus_power_prefactor(order));
      map[order] = {stirling_transform(order),   rho_z_transform(order),
                    z_y_transform(order),        prefactor,
                    one_minus * prefactor,       radial_prefactor,
                    one_minus * radial_prefactor_minus};
    }
}
