#include "../Bigfloat.hxx"
#include "../Coordinate.hxx"

#include <fstream>
#include <boost/math/tools/polynomial.hpp>

void write_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &derivs,
  std::ofstream &outfile)
{
  // boost::epsilon is wrong, so we need compute it ourselves from the
  // precision.

  // Multiply by 0.9 to include some variation from the absolute
  // smallest number.
  const int64_t scaled_precision(0.9 * log2(10)
                                 * Bigfloat::default_precision());
  Bigfloat epsilon(0.5);
  mpf_div_2exp(epsilon.backend().data(), epsilon.backend().data(),
               scaled_precision);

  outfile << "          [\n";
  for(size_t deriv_0(0); deriv_0 < derivs.size(); ++deriv_0)
    {
      outfile << "            [\n";
      for(size_t deriv_1(0); deriv_1 < derivs[deriv_0].size(); ++deriv_1)
        {
          outfile << "              [\n";
          const auto &poly = derivs[deriv_0][deriv_1];
          if(poly.is_zero())
            {
              outfile << "\"0\"";
            }
          else
            {
              // Truncate all high-order coefficients,
              // if they are of order of epsilon * (prev coefficient),
              // i.e. effectively zero.
              size_t truncated_size = poly.size();
              {
                Bigfloat max_coeff = 0;
                for(int degree = poly.size() - 1; degree >= 0; --degree)
                  {
                    const Bigfloat curr_coeff = abs(poly[degree]);
                    if(max_coeff < epsilon * curr_coeff)
                      truncated_size = degree + 1;
                    max_coeff = max(max_coeff, curr_coeff);
                  }
              }

              for(size_t polynomial_coeff(0);
                  polynomial_coeff < truncated_size; ++polynomial_coeff)
                {
                  if(polynomial_coeff != 0)
                    {
                      outfile << ",\n";
                    }
                  outfile << "\"" << poly[polynomial_coeff] << "\"";
                }
            }
          outfile << "\n              ]";
          if(deriv_1 + 1 != derivs[deriv_0].size())
            {
              outfile << ",\n";
            }
        }
      outfile << "\n            ]";
      if(deriv_0 + 1 != derivs.size())
        {
          outfile << ",\n";
        }
    }
  outfile << "\n          ]";
}
