#include "congruence_transform.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb_to_yyb(
  const int64_t &parity, const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> &zzb)
{
  return congruence_transform(parity, transforms, Transforms::Index::z_y,
                              zzb);
}
