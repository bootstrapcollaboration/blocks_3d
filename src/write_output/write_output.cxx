#include "Transforms.hxx"
#include "../Log_r_Derivatives.hxx"
#include "../Q4pm.hxx"
#include "../Coordinate.hxx"
#include "../lambda_log_r_tables/Pole_Types.hxx"

#include <filesystem>
#include <fstream>
#include <fmt/format.h>

#include <set>
#include <map>
#include <thread>
#include <atomic>

void write_header(
  std::ofstream &outfile, const std::string &output_template,
  const std::array<int64_t, 4> &two_js, const int64_t &two_j_intermediate,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const std::set<Bigfloat> &output_poles,
  const size_t &precision_base_2,
  const int &argc, char *argv[]);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
rho_rhob_to_zzb(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const Transforms &transforms,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>> &lambda_log_r);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb_to_yyb(
  const int64_t &parity, const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &zzb);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb_to_xt(
  const int64_t &parity,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &zzb);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lrho_to_xt_radial(const int64_t &parity, const Transforms &transforms,
                  const Log_r_Derivatives &spin);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
xt_to_ws_radial(
  const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &xt_radial);

void write_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &derivs,
  std::ofstream &outfile);

std::vector<Bigfloat>
pole_locations_x(const int64_t &two_j,
                 const int64_t &two_j12,
                 const int64_t &two_j43,
                 const int64_t &order,
                 const Pole_Types &pole_types);

namespace
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> get_zzb(
    const std::set<Coordinate> &coordinates, const int64_t &parity,
    size_t two_j_index, size_t parity_index, size_t left_index,
    size_t right_index, const Transforms &transforms,
    const std::vector<
      std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
      &lambda_log_r)
  {
    if(coordinates.find(Coordinate::zzb) != coordinates.end()
       || coordinates.find(Coordinate::xt) != coordinates.end()
       || coordinates.find(Coordinate::yyb) != coordinates.end()
       || coordinates.find(Coordinate::ws) != coordinates.end())
      {
        return rho_rhob_to_zzb(parity, two_j_index, parity_index, left_index,
                               right_index, transforms, lambda_log_r);
      }
    return {};
  }

  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> get_yyb(
    const std::set<Coordinate> &coordinates, const int64_t &parity,
    const Transforms &transforms,
    const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
      &zzb)
  {
    if(coordinates.find(Coordinate::yyb) != coordinates.end()
       || coordinates.find(Coordinate::ws) != coordinates.end())
      {
        return zzb_to_yyb(parity, transforms, zzb);
      }
    return {};
  }

  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
  get_xt_radial(
    const std::set<Coordinate> &coordinates, const int64_t &parity,
    size_t two_j_index, size_t parity_index, size_t left_index,
    size_t right_index, const Transforms &transforms,
    const std::vector<
      std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
      &lambda_log_r)
  {
    if(coordinates.find(Coordinate::xt_radial) != coordinates.end()
       || coordinates.find(Coordinate::ws_radial) != coordinates.end())
      {
        return lrho_to_xt_radial(
          parity, transforms,
          lambda_log_r[0][two_j_index][parity_index][left_index]
                      [right_index]);
      }
    return {};
  }

  void write_derivs_for_coordinate(
    std::ofstream &outfile, const Coordinate &coordinate,
    const int64_t &parity, const Transforms &transforms,
    const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
      &zzb,
    const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
      &yyb,
    const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
      &xt_radial)
  {
    switch(coordinate)
      {
      case Coordinate::zzb: write_derivs(zzb, outfile); break;
      case Coordinate::yyb: write_derivs(yyb, outfile); break;
      case Coordinate::xt:
        write_derivs(zzb_to_xt(parity, zzb), outfile);
        break;
      case Coordinate::ws:
        write_derivs(zzb_to_xt(parity, yyb), outfile);
        break;
      case Coordinate::xt_radial: write_derivs(xt_radial, outfile); break;
      case Coordinate::ws_radial:
        write_derivs(xt_to_ws_radial(transforms, xt_radial), outfile);
        break;
      default:
        throw std::runtime_error("Unknown coordinate: "
                                 + to_string(coordinate));
      }
  }
}

void write_output(
  const std::string &output_template, const int64_t &parity,
  const std::array<int64_t, 4> &two_js,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const size_t &precision_base_2, const int &argc, char *argv[],
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>> &lambda_log_r,
  const Pole_Types &pole_types, const bool &radial_only,
  const size_t &num_threads, const bool &debug)
{
  const Bigfloat delta_1_plus_2(delta_1_plus_2_string);
  Transforms transforms(delta_1_plus_2);
  // order==0 is used for radial-only  coordinates
  transforms.insert_if_not_present(0);
  for(auto &lambda_order : lambda_log_r)
    for(auto &two_j : lambda_order)
      for(auto &parities : two_j)
        for(auto &left : parities)
          for(auto &right : left)
            {
              const int64_t order(right.numerator.size() - 1 + (radial_only ? 0 : parity) );
              transforms.insert_if_not_present(order);
            }
  const size_t two_j_size(two_js_intermediate.size());

  std::atomic_size_t global_index(0);
  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      threads.emplace_back(
        [&](const size_t &thread_rank) {
          try
            {
              for(size_t curr_index = global_index++; curr_index < two_j_size;
                  curr_index = global_index++)
                {
                  // Iterate over two_j_intermediate in reverse order,
                  // starting from larger values.
                  // This should improve load balancing.
                  // The first several spins require less computations.
                  // Tasks are assigned to threads in a round-robin way,
                  // so we want to start with heavier tasks.
                  size_t two_j_index = two_j_size - curr_index - 1;
                  int64_t two_j_intermediate(
                    *std::next(two_js_intermediate.begin(), two_j_index));
                  std::filesystem::path outfile_path(
                    fmt::format(output_template, (two_j_intermediate / 2.0)));
                  if(debug)
                    {
                      std::stringstream ss;
                      ss << "write_output: " << thread_rank << " "
                         << two_j_index << " " << outfile_path << "\n";
                      std::cout << ss.str() << std::flush;
                    }
                  if(!outfile_path.parent_path().empty())
                    {
                      std::filesystem::create_directories(
                        outfile_path.parent_path());
                    }
                  std::ofstream outfile(outfile_path);
                  outfile.precision(
                    boost::multiprecision::mpf_float::default_precision());

                  std::vector<Bigfloat> output_poles =
                    pole_locations_x(two_j_intermediate, two_j12, two_j43, kept_pole_order, pole_types);

                  // That write_header takes a set of output poles
                  // relies on the fact that no poles occur with
                  // multiplicity > 1.
                  std::set<Bigfloat> output_poles_set(output_poles.begin(), output_poles.end());
                  
                  write_header(outfile, output_template, two_js,
                               two_j_intermediate, two_js_intermediate,
                               two_j12, two_j43, delta_12_string,
                               delta_43_string, delta_1_plus_2_string, q4pm,
                               order, lambda, coordinates, kept_pole_order,
                               output_poles_set, precision_base_2,
                               argc, argv);
                  outfile << "  \"derivs\":\n  [\n";

                  const size_t parity_size(
                    lambda_log_r.front()[two_j_index].size());
                  for(size_t parity_index(0); parity_index != parity_size;
                      ++parity_index)
                    {
                      outfile << "    [\n";
                      const size_t left_size(
                        lambda_log_r.front()[two_j_index][parity_index].size());
                      for(size_t left_index(0); left_index != left_size;
                          ++left_index)
                        {
                          outfile << "      [\n";
                          const size_t right_size(
                            lambda_log_r
                              .front()[two_j_index][parity_index][left_index]
                              .size());
                          for(size_t right_index(0); right_index != right_size;
                              ++right_index)
                            {
                              const auto zzb = get_zzb(
                                coordinates, parity, two_j_index, parity_index,
                                left_index, right_index, transforms,
                                lambda_log_r);
                              const auto yyb = get_yyb(coordinates, parity,
                                                       transforms, zzb);
                              const auto xt_radial = get_xt_radial(
                                coordinates, parity, two_j_index, parity_index,
                                left_index, right_index, transforms,
                                lambda_log_r);

                              outfile << "        [\n";

                              for(auto c(coordinates.begin());
                                  c != coordinates.end(); ++c)
                                {
                                  if(c != coordinates.begin())
                                    outfile << ",\n";
                                  write_derivs_for_coordinate(outfile, *c, parity,
                                                     transforms, zzb, yyb,
                                                     xt_radial);
                                }
                              outfile << "\n        ]";
                              if(right_index + 1 != right_size)
                                {
                                  outfile << ",\n";
                                }
                            }
                          outfile << "\n      ]";
                          if(left_index + 1 != left_size)
                            {
                              outfile << ",\n";
                            }
                        }
                      outfile << "\n    ]";
                      if(parity_index + 1 != parity_size)
                        {
                          outfile << ",\n";
                        }
                    }
                  outfile << "\n  ]";
                  outfile << "\n}\n";
                }
            }
          catch(fmt::format_error &e)
            {
              std::stringstream ss;
              ss << "Formatting error on thread " << thread_rank
                 << " for the template '" << output_template
                 << "': " << e.what() << "\n";
              std::cerr << ss.str();
              exit(1);
            }
          catch(std::exception &e)
            {
              std::stringstream ss;
              ss << "Error on thread " << thread_rank << ": " << e.what()
                 << "\n";
              std::cerr << ss.str();
              exit(1);
            }
          catch(...)
            {
              std::stringstream ss;
              ss << "Unknown error on thread " << thread_rank << "\n";
              std::cerr << ss.str();
              exit(1);
            }
        },
        thread_rank);
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
}
