#include "../Bigfloat.hxx"
#include "../Q4pm.hxx"
#include "../ostream_vector.hxx"
#include "../compute_pseudo_parity_types.hxx"
#include "../so3_tensor_parity.hxx"
#include "../Coordinate.hxx"

#include <filesystem>
#include <fstream>
#include <boost/multiprecision/gmp.hpp>

#include <set>

void write_header(
  std::ofstream &outfile, const std::string &output_template,
  const std::array<int64_t, 4> &two_js, const int64_t &two_j_intermediate,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const std::set<Bigfloat> &output_poles, const size_t &precision_base_2,
  const int &argc, char *argv[])
{
  outfile << "{\n";
  std::array<double, 4> j_external;
  for(size_t index(0); index < j_external.size(); ++index)
    {
      j_external[index] = two_js[index] / 2.0;
    }
  outfile << "  \"j_external\": " << j_external << ",\n";
  outfile << "  \"j_internal\": " << (two_j_intermediate / 2.0) << ",\n";
  outfile << "  \"delta_minus_x\": "
          << (two_j_intermediate <= 1 ? (two_j_intermediate / 2.0 + 0.5)
                                      : (two_j_intermediate / 2.0 + 1))
          << ",\n";

  outfile << "  \"j_12\": " << (two_j12 / 2.0) << ",\n";
  outfile << "  \"j_43\": " << (two_j43 / 2.0) << ",\n";
  outfile << "  \"delta_12\": \"" << delta_12_string << "\",\n";
  outfile << "  \"delta_43\": \"" << delta_43_string << "\",\n";
  outfile << "  \"delta_1_plus_2\": \"" << delta_1_plus_2_string << "\",\n";
  std::array<double, 4> q4_vec_input;
  for(size_t index(0); index < q4_vec_input.size(); ++index)
    {
      q4_vec_input[index] = q4pm.vec[index] / 2.0;
    }
  outfile << "  \"four_pt_struct\": " << q4_vec_input << ",\n";
  outfile << "  \"four_pt_sign\": " << q4pm.i << ",\n";
  outfile << "  \"order\": " << order << ",\n";
  outfile << "  \"lambda\": " << lambda << ",\n";
  outfile << "  \"kept_pole_order\": " << kept_pole_order << ",\n";
  outfile << "  \"coordinates\": [";
  for(auto c(coordinates.begin()); c != coordinates.end(); ++c)
    {
      if(c != coordinates.begin())
        {
          outfile << ",";
        }
      outfile << "\"" << *c << "\"";
    }
  outfile << "],\n";
  outfile << "  \"pole_list_x\": [";
  for(auto pole(output_poles.begin()); pole != output_poles.end(); ++pole)
    {
      if(pole != output_poles.begin())
        {
          outfile << ", ";
        }
      outfile << *pole;
    }
  outfile << "],\n";
  outfile << "  \"precision_input\": " << precision_base_2 << ",\n";
  outfile << "  \"precision_actual\": "
          << static_cast<int64_t>(
               (boost::multiprecision::mpf_float::default_precision() + 1)
               * log(10.0) / log(2.0))
          << ",\n";
  outfile << "  \"j_internal_input\": [";
  for(auto two_j(two_js_intermediate.begin());
      two_j != two_js_intermediate.end();)
    {
      outfile << (*two_j) / 2.0;
      ++two_j;
      if(two_j != two_js_intermediate.end())
        {
          outfile << ", ";
        }
    }
  outfile << "],\n";
  outfile << "  \"output_template\": \"" << output_template << "\",\n";
  outfile << "  \"args\": \"";
  outfile << argv[0];
  for(int index = 1; index < argc; ++index)
    {
      outfile << " " << argv[index];
    }
  outfile << "\",\n";

  outfile << "  \"index_values\": [\n";
  std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4pm.vec,
                                {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));
  for(int64_t pp_index(0); pp_index != 2; ++pp_index)
    {
      if(pp_index != 0)
        {
          outfile << ",\n";
        }
      // Convert pseudo_parity to parity
      // Make sure that we do %2 to a non-negative number because -1%2 == -1
      outfile << "    {\n"
              << "     \"three_pt_parity_120\": "
              << std::abs((two_j12 - two_js[0] + two_js[1])/2
                  + pseudo_parity_types[pp_index][0])%2 << ",\n"
              << "     \"three_pt_parity_430\": "
              << std::abs((two_j43 - two_js[3] + two_js[2])/2
                  + pseudo_parity_types[pp_index][1])%2 << ",\n"
              << "     \"j_120\": ";
      std::vector<double> temp_j;
      for(auto &two_j_120 : so3_tensor_pseudo_parity(pseudo_parity_types[pp_index][0],
                                              two_j12, two_j_intermediate))
        {
          temp_j.push_back(two_j_120 / 2.0);
        }
      outfile << temp_j << ",\n"
              << "     \"j_430\": ";
      temp_j.clear();
      for(auto &two_j_430 : so3_tensor_pseudo_parity(pseudo_parity_types[pp_index][1],
                                              two_j43, two_j_intermediate))
        {
          temp_j.push_back(two_j_430 / 2.0);
        }
      outfile << temp_j << ",\n";
      outfile << "     \"coordinates\": [";
      for(auto c(coordinates.begin()); c != coordinates.end(); ++c)
        {
          if(c != coordinates.begin())
            {
              outfile << ", ";
            }
          outfile << '"' << *c << '"';
        }
      outfile << "]\n";
      outfile << "    }";
    }
  outfile << "\n  ],\n";

  outfile << "  \"index_names\": [\"three_pt_parity_index\",\n"
          << "                  \"j_120_index\",\n"
          << "                  \"j_430_index\",\n"
          << "                  \"coordinate_index\",\n"
          << "                  \"deriv_0\",\n"
          << "                  \"deriv_1\",\n"
          << "                  \"monomial_degree\"],\n";
}
