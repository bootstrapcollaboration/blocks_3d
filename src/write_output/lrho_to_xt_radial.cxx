#include "Transforms.hxx"
#include "../Log_r_Derivatives.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lrho_to_xt_radial(const int64_t &parity, const Transforms &transforms,
                  const Log_r_Derivatives &log_r_derivs)
{
  const size_t derivs_size(log_r_derivs.numerator.size());
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(derivs_size);
  for(auto &a: result)
  {
    a.reserve(1);
  }

  const int64_t order(derivs_size - 1);
  const Eigen_Matrix &stirling(
    transforms.get(order, Transforms::Index::stirling)),
    &rho_z(transforms.get(order, Transforms::Index::rho_z)),
    &radial_prefactor(
      parity == 0
        ? transforms.get(order, Transforms::Index::radial_power_prefactor)
        : transforms.get(order,
                         Transforms::Index::radial_one_minus_power_product));

  const Eigen_Matrix total_transform(radial_prefactor * rho_z * stirling);
  for(size_t ii(0); ii < derivs_size; ++ii)
    {
      result[ii].emplace_back();
      auto &element(result[ii].back());
      for(size_t jj(0); jj < derivs_size; ++jj)
        {
          element += log_r_derivs.numerator[jj] * total_transform(ii, jj);
        }
    }
  return result;
}
