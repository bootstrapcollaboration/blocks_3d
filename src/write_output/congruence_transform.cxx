#include "Transforms.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>
#include <map>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
congruence_transform(
  const int64_t &parity, const Transforms &transforms,
  const Transforms::Index &index,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &input)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result;
  const int64_t order(input.size() - 1);
  const int64_t i_size(order + 1);
  result.resize(i_size);
  const Eigen_Matrix &transform(transforms.get(order, index));

  // Add explicit temps to reduce memory churn
  Bigfloat temp_float;
  boost::math::tools::polynomial<Bigfloat> temp_poly;

  // Mulitply transform(i,k) input(k,l) transform(j,l)
  // intermediate(i,l) = transform(i,k) input(k,l)
  // result(i,j)=intermediate(i,l) transform(j,l)
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    intermediate(i_size);
  for(int64_t i(0); i != i_size; ++i)
    {
      intermediate[i].resize(i_size);
      for(auto &element : intermediate[i])
        {
          element.set_zero();
        }
      for(int64_t k(0); k != i_size; ++k)
        for(int64_t l(0); l != i_size - k; ++l)
          {
            temp_float = transform(i, k);
            if(l <= k)
              {
                temp_poly = input[k][l];
                temp_poly *= temp_float;
                intermediate[i][l] += temp_poly;
              }
            else
              {
                temp_poly = input[l][k];
                temp_poly *= temp_float;
                if(parity % 2 == 0)
                  {
                    intermediate[i][l] += temp_poly;
                  }
                else
                  {
                    intermediate[i][l] -= temp_poly;
                  }
              }
          }
    }

  for(int64_t i(0); i != i_size; ++i)
    {
      const int64_t j_size(std::min(i, order - i) + 1);
      result[i].resize(j_size);
      for(int64_t j(0); j != j_size; ++j)
        {
          boost::math::tools::polynomial<Bigfloat> &sum(result[i][j]);
          sum.set_zero();
          for(int64_t l(0); l != order; ++l)
            {
              temp_float = transform(j, l);
              temp_poly = intermediate[i][l];
              temp_poly *= temp_float;
              sum += temp_poly;
            }
        }
    }
  return result;
}
