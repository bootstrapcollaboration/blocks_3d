#include "Log_r_Derivatives.hxx"
#include "Q4pm.hxx"
#include "Coordinate.hxx"
#include "lambda_log_r_tables/Pole_Types.hxx"

#include "timers/Timers.hxx"

#include <set>
#include <fmt/format.h>

void handle_input(const int &argc, char *argv[], std::string &delta_12_string,
                  std::string &delta_43_string,
                  std::string &delta_1_plus_2_string,
                  std::set<Coordinate> &coordinates, int64_t &lambda,
                  int64_t &order, int64_t &kept_pole_order, int64_t &two_j12,
                  int64_t &two_j43, std::array<int64_t, 4> &two_js,
                  std::set<int64_t> &two_js_intermediate, Q4pm &q4pm,
                  size_t &precision_base_2, size_t &num_threads,
                  std::string &output_template, bool &debug, bool &profile,
                  bool &is_radial);
size_t lambda_log_r_size(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
    &lambda_log_r);

std::vector<
  std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
lambda_log_r_tables(const std::array<int64_t, 4> &two_js,
                    const Bigfloat &delta_12, const Bigfloat &delta_43,
                    const int64_t &two_j12, const int64_t &two_j43,
                    const Q4pm &q4pm,
                    const std::set<int64_t> &two_js_intermediate,
                    const int64_t &lambda, const bool &is_radial,
                    const int64_t &kept_pole_order, const int64_t &order,
                    const size_t &num_threads, const bool &debug,
                    const Pole_Types &pole_types, Timers &timers);

void write_output(
  const std::string &output_template, const int64_t &parity,
  const std::array<int64_t, 4> &two_js,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const size_t &precision_base_2, const int &argc, char *argv[],
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
    &lambda_log_r,
  const Pole_Types &pole_types, const bool &radial_only,
  const size_t &num_threads, const bool &debug);

int main(int argc, char *argv[])
{
  try
    {
      std::string delta_12_string, delta_43_string, delta_1_plus_2_string;
      int64_t lambda, order, kept_pole_order, two_j12, two_j43;
      std::array<int64_t, 4> two_js;
      std::set<int64_t> two_js_intermediate;
      Q4pm q4pm;
      std::set<Coordinate> coordinates;
      size_t precision_base_2, num_threads;
      std::string output_template;
      bool debug, profile, is_radial;

      handle_input(argc, argv, delta_12_string, delta_43_string,
                   delta_1_plus_2_string, coordinates, lambda, order,
                   kept_pole_order, two_j12, two_j43, two_js,
                   two_js_intermediate, q4pm, precision_base_2, num_threads,
                   output_template, debug, profile, is_radial);
      const Bigfloat delta_12(delta_12_string), delta_43(delta_43_string);

      Timers timers(false);
      auto &main_timer(timers.add_and_start("main"));
      timers.enter("main");

      auto &lambda_log_r_tables_timer(
        timers.add_and_start("lambda_log_r_tables"));

      Pole_Types pole_types;
      int64_t two_j_max = *two_js_intermediate.rbegin();
      pole_types.initialize(two_j_max, two_j12, two_j43, order);

      // this returns lambda,log(rho)-derivatives
      // conversion to appropriate coordinates happens in write_output
      auto lambda_log_r(lambda_log_r_tables(
        two_js, delta_12, delta_43, two_j12, two_j43, q4pm,
        two_js_intermediate, lambda, is_radial, kept_pole_order, order,
        num_threads, debug, pole_types, timers));
      lambda_log_r_tables_timer.stop();
      std::cout << "Finished with lambda_log_r" << std::endl;

      // TODO: This does nontrivial computation. Only do it at high verbosity.
      // if (debug)
      //   {
      //     std::cout << "lambda_log_r_size(lambda_log_r) = "
      //               << lambda_log_r_size(lambda_log_r)
      //               << std::endl;
      //   }

      int64_t parity((1 - q4pm.i) / 2);

      auto &output_timer(timers.add_and_start("write_output"));

      // convert the derivatives and write the output
      write_output(output_template, parity, two_js, two_js_intermediate,
                   two_j12, two_j43, delta_12_string, delta_43_string,
                   delta_1_plus_2_string, q4pm, order, lambda, coordinates,
                   kept_pole_order, precision_base_2, argc, argv, lambda_log_r,
                   pole_types, is_radial, num_threads, debug);
      output_timer.stop();
      main_timer.stop();

      // output the performance profiling data
      if(profile)
        {
          // NB: Here, it is important to use 0.0 (instead of 0) so
          // that the argument to format has type double, which is the
          // same type used to create block file names. In the case of
          // block files, doubles are used to represent half-integers.
          auto profile_path = fmt::format(output_template, 0.0) + ".profile";
          timers.write_profile(coordinates, lambda, order, kept_pole_order,
                               two_j12, two_j43, two_js, two_js_intermediate,
                               q4pm, precision_base_2, num_threads,
                               profile_path);
        }
    }
  catch(std::exception &e)
    {
      std::cerr << "Error: " << e.what() << "\n";
      exit(1);
    }
  catch(...)
    {
      std::cerr << "Unknown error\n";
      exit(1);
    }
}
