#pragma once

#include <vector>
#include <cstdint>

std::vector<int64_t> so3_tensor_pseudo_parity(const int64_t &p, const int64_t &two_j1,
                                       const int64_t &two_j2);
