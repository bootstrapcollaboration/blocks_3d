#pragma once

#include "ostream_array.hxx"

struct Q4pm
{
  std::array<int64_t, 4> vec;
  int64_t i;
  Q4pm() = default;
  Q4pm(const std::array<int64_t, 4> &Vec, const int64_t I) : vec(Vec), i(I) {}
};

inline std::ostream &operator<<(std::ostream &os, const Q4pm &q4pm)
{
  os << "{ \"vec\": " << q4pm.vec << ", \"parity\": " << q4pm.i << "}";
  return os;
}
