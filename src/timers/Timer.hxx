#pragma once

#include <chrono>
#include <iostream>

struct Timer
{
  // Prevent accidental copying
  Timer(const Timer &other) = delete;
  Timer(Timer &&other) noexcept = default;
  Timer &operator=(const Timer &other) = delete;
  Timer &operator=(Timer &&other) noexcept = default;

  std::chrono::time_point<std::chrono::high_resolution_clock> start_time,
    stop_time;
  int64_t accumulator = 0;
  int64_t runs = 0;
  Timer() : start_time(std::chrono::high_resolution_clock::now()) {}
  auto stop() 
  { 
    stop_time = std::chrono::high_resolution_clock::now(); 
    accumulator += elapsed_nanoseconds();
    ++ runs;
  }

  auto start()
  {
    start_time = std::chrono::high_resolution_clock::now(); 
  }

  int64_t total_nanoseconds() const
  {
    return accumulator;
  }

  int64_t elapsed_nanoseconds() const
  {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(stop_time
                                                                 - start_time)
      .count();
  }

  int64_t elapsed_microseconds() const
  {
    return std::chrono::duration_cast<std::chrono::microseconds>(stop_time
                                                                 - start_time)
      .count();
  }
  int64_t elapsed_milliseconds() const
  {
    return std::chrono::duration_cast<std::chrono::milliseconds>(stop_time
                                                                 - start_time)
      .count();
  }
  int64_t elapsed_seconds() const
  {
    return std::chrono::duration_cast<std::chrono::seconds>(stop_time
                                                            - start_time)
      .count();
  }
};

inline std::ostream &operator<<(std::ostream &os, const Timer &timer)
{
  os << (timer.total_nanoseconds()/1000000000.0);
  return os;
}
