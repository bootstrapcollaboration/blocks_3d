//=======================================================================
// Copyright 2014-2015 David Simmons-Duffin.
// Distributed under the MIT License.
// (See accompanying file LICENSE or copy at
//  http://opensource.org/licenses/MIT)
//=======================================================================

#pragma once

#include "Timer.hxx"

#include "../Q4pm.hxx"
#include "../Coordinate.hxx"

#include<boost/noncopyable.hpp>

#include <fstream>
#include <string>
#include <list>
#include <set>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <mutex>

struct Timers : public std::list<std::pair<std::string, Timer>>,
                boost::noncopyable
{
private:
  std::mutex mutex_{};

public:
  bool debug = false;
  std::list<std::string> prefixes;
  std::string prefix;

  explicit Timers(const bool &Debug) : debug(Debug) {}
  Timers(const bool &Debug, const std::list<std::string> &Prefixes) : debug(Debug), prefixes(Prefixes) 
  {
    set_prefix_string();
  }

  void enter(const std::string &new_prefix)
  {
    prefixes.push_back(new_prefix);
    prefix = prefix+"."+new_prefix;
  }

  void set_prefix_string()
  {
    std::stringstream ss;
    for(auto &p : prefixes)
    {
      ss << "." << p;
    }
    prefix=ss.str();
  }

  void exit()
  {
    prefixes.pop_back();
    set_prefix_string();
  }

  Timer &add_and_start(const std::string &name_raw)
  {
    // TODO: make other functions thread-safe too
    std::lock_guard guard(mutex_);

    std::string name(prefix+"."+name_raw);
    if(debug)
      {
        std::ifstream stat_file("/proc/self/statm");
        if(stat_file.good())
          {
            std::string stats;
            std::getline(stat_file, stats);
            // El::Output(El::mpi::Rank(), " ", name, " ", stats);
            std::cout << "Timer started: " << name << " statm == " << stats << std::endl << std::flush;
          }
      }
    emplace_back(name, Timer());
    return back().second;
  }

  void write_profile(std::set<Coordinate> &coordinates, int64_t &lambda,
                  int64_t &order, int64_t &kept_pole_order, int64_t &two_j12,
                  int64_t &two_j43, std::array<int64_t, 4> &two_js,
                  std::set<int64_t> &two_js_intermediate, Q4pm &q4pm,
                  size_t &precision_base_2, size_t &num_threads, const std::string &filename) const
  {
    std::ofstream f(filename);
    f << std::setprecision(10);
    f << "{{" << '\n';

    std::array<double, 4> j_external;
    for(size_t index(0); index < j_external.size(); ++index)
      {
        j_external[index] = two_js[index] / 2.0;
      }
    f << "  {\"num_threads\", " << num_threads << "},\n";
    f << "  {\"j_external\", " << j_external << "},\n";

    f << "  {\"j_12\", " << (two_j12 / 2.0) << "},\n";
    f << "  {\"j_43\", " << (two_j43 / 2.0) << "},\n";
    std::array<double, 4> q4_vec_input;
    for(size_t index(0); index < q4_vec_input.size(); ++index)
      {
        q4_vec_input[index] = q4pm.vec[index] / 2.0;
      }
    f << "  {\"four_pt_struct\", " << q4_vec_input << "},\n";
    f << "  {\"order\", " << order << "},\n";
    f << "  {\"lambda\", " << lambda << "},\n";
    f << "  {\"kept_pole_order\", " << kept_pole_order << "},\n";
    f << "  {\"coordinates\", {";
    for(auto c(coordinates.begin()); c != coordinates.end(); ++c)
      {
        if(c != coordinates.begin())
          {
            f << ",";
          }
        f << "\"" << *c << "\"";
      }
    f << "}},\n";
    f << "  {\"precision_input\", " << precision_base_2 << "},\n";
    // f << "  {\"precision_actual\", "
    //         << static_cast<int64_t>(
    //             (boost::multiprecision::mpf_float::default_precision() + 1)
    //             * log(10.0) / log(2.0))
    //         << "},\n";
    f << "  {\"j_internal_input\", {";
    for(auto two_j(two_js_intermediate.begin());
        two_j != two_js_intermediate.end();)
      {
        f << (*two_j) / 2.0;
        ++two_j;
        if(two_j != two_js_intermediate.end())
          {
            f << ", ";
          }
      }
    f << "}}\n";

                
    f << "},{" << '\n';
    for(auto it(begin()); it != end();)
      {
        f << "    {\"" << it->first << "\", " << it->second << ", " << it->second.runs << " }";
        ++it;
        if(it != end())
          {
            f << ",";
          }
        f << '\n';
      }
    f << "}}" << '\n';

    if(!f.good())
      {
        throw std::runtime_error("Error when writing to: " + filename);
      }
  }

  int64_t elapsed_milliseconds(const std::string &s) const
  {
    auto iter(std::find_if(rbegin(), rend(),
                           [&s](const std::pair<std::string, Timer> &timer) {
                             return timer.first == s;
                           }));
    if(iter == rend())
      {
        throw std::runtime_error("Could not find timing for " + s);
      }
    return iter->second.elapsed_milliseconds();
  }
};
