#include <vector>
#include <cstdint>
#include <cmath>

std::vector<int64_t>
so3_tensor_pseudo_parity(const int64_t &pseudo_parity, const int64_t &two_j1,
                  const int64_t &two_j2)
{
  std::vector<int64_t> result;
  for(int64_t two_j(std::abs(two_j1 - two_j2)); two_j <= two_j1 + two_j2;
      two_j += 2)
    {
      if(((two_j1 + two_j2 - two_j) - 2 * pseudo_parity) % 4 == 0)
        {
          result.push_back(two_j);
        }
    }
  return result;
}
