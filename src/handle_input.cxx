#include "Bigfloat.hxx"
#include "Q4pm.hxx"
#include "Coordinate.hxx"

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem/fstream.hpp>

#include <set>

void handle_input(const int &argc, char *argv[], std::string &delta_12_string,
                  std::string &delta_43_string,
                  std::string &delta_1_plus_2_string,
                  std::set<Coordinate> &coordinates, int64_t &lambda,
                  int64_t &order, int64_t &kept_pole_order, int64_t &two_j12,
                  int64_t &two_j43, std::array<int64_t, 4> &two_js,
                  std::set<int64_t> &two_js_intermediate, Q4pm &q4pm,
                  size_t &precision_base_2, size_t &num_threads,
                  std::string &output_template, bool &debug, bool &profile,
                  bool &is_radial)
{
  namespace po = boost::program_options;

  po::options_description options("3D Blocks options");

  std::string j_internal_string, j_external_string, j12_string, j43_string,
    q4_string, coordinates_string;
  int64_t q4_sign;

  boost::filesystem::path param_path;

  options.add_options()("help,h", "Show this helpful message.");
  options.add_options()(
    "j-external", po::value<std::string>(&j_external_string)->required(),
    "Comma separated list of 4 integer/half-integer spins of the external "
    "operators j₁,j₂,j₃,j₄ (e.g. '1,0.5,1,0.5').");
  options.add_options()(
    "j-internal", po::value<std::string>(&j_internal_string)->required(),
    "Comma separated list of integer/half-integer ranges of spin of the "
    "internal operators.  For example, '0.5-3.5,5.5,8.5-16.5-2' will use "
    "spins at 0.5, 1.5, 2.5, 3.5, 5.5, 8.5, 10.5, 12.5, 14.5, and 16.5.");
  options.add_options()(
    "j-12", po::value<std::string>(&j12_string)->required(), "j₁₂");
  options.add_options()(
    "j-43", po::value<std::string>(&j43_string)->required(), "j₄₃");
  options.add_options()("delta-12",
                        po::value<std::string>(&delta_12_string)->required(),
                        "Δ₁₂=Δ₁-Δ₂");
  options.add_options()("delta-43",
                        po::value<std::string>(&delta_43_string)->required(),
                        "Δ₄₃=Δ₄-Δ₃");
  options.add_options()(
    "delta-1-plus-2",
    po::value<std::string>(&delta_1_plus_2_string)->default_value("0"),
    "Δ₁+Δ₂");
  options.add_options()("four-pt-struct",
                        po::value<std::string>(&q4_string)->required(),
                        "Comma separated list of 4 integers/half-integers "
                        "representing the four - point structure q₁, q₂, "
                        "q₃, q₄, (e.g.'1,0.5,1,0.5' or '0.5,0,0,-0.5') ");
  options.add_options()(
    "four-pt-sign", po::value<int64_t>(&q4_sign)->required(),
    "1 or -1 representing the sign of the four-point structure under z "
    "<-> zbar");
  options.add_options()("order", po::value<int64_t>(&order)->required(),
                        "Depth of recursion");
  options.add_options()("lambda", po::value<int64_t>(&lambda)->required(),
                        "Max number of derivatives (Λ)");
  options.add_options()(
    "coordinates",
    po::value<std::string>(&coordinates_string)->default_value("zzb"),
    "Comma separated list of coordinates: zzb, xt, xt_radial, yyb, ws, or "
    "ws_radial.  The _radial coordinates only compute derivatives in the "
    "radial direction (x or w).");
  options.add_options()("kept-pole-order",
                        po::value<int64_t>(&kept_pole_order)->required(),
                        "Pole order to keep.");
  options.add_options()("num-threads",
                        po::value<size_t>(&num_threads)->required(),
                        "Number of threads");
  options.add_options()("precision",
                        po::value<size_t>(&precision_base_2)->required(),
                        "The precision, in the number of bits, for "
                        "numbers in the computation.");
  options.add_options()(
    "out-template,o", po::value<std::string>(&output_template)->required(),
    "Template for output files. '{}' will be replaced by j_internal. "
    "Parent directories are automatically created. For example, the "
    "options '--j-internal=0,1 -o my_model/run_5/zzb_{}.json' will create the "
    "directory 'my_model/run_5/' and write the files "
    "my_model/run_5/zzb_0.json' and 'my_model/run_5/zzb_1.json'");
  options.add_options()("debug", po::value<bool>(&debug)->default_value(false),
                        "Write out debugging output.");
  options.add_options()(
    "profile", po::value<bool>(&profile)->default_value(true),
    "Write profiling file. The filename is obtained by taking the "
    "output file with j_internal = 0 and appending .profile to it.");
  options.add_options()(
    "param-file,p", po::value<boost::filesystem::path>(&param_path),
    "Any parameter can optionally be set via this file in key=value "
    "format. Command line arguments override values in the parameter "
    "file.");
  
  po::variables_map variables_map;

  po::store(po::parse_command_line(argc, argv, options), variables_map);
  if(variables_map.count("help"))
    {
      std::cout << options << '\n';
      exit(0);
    }

  if(variables_map.count("param-file"))
    {
      param_path
        = variables_map["param-file"].as<boost::filesystem::path>();
      std::ifstream ifs(param_path.string().c_str());
      if(!ifs.good())
        {
          throw std::runtime_error("Could not open '"
                                   + param_path.string() + "'");
        }

      po::store(po::parse_config_file(ifs, options),
                variables_map);
    }
  
  po::notify(variables_map);

  two_j12 = 2 * std::stod(j12_string);
  two_j43 = 2 * std::stod(j43_string);
  if((two_j12 % 2) != (two_j43 % 2))
    {
      throw std::runtime_error("j_12 and j_43 must both be either "
                               "integers or half integers, but found:\n  j_12="
                               + j12_string + ", j_43=" + j43_string);
    }

  // Round up the size to work around bugs in boost::multiprecision
  const size_t precision_base_2_rounded_up(
    (((precision_base_2 - 1) / (sizeof(mp_limb_t) * 8)) + 1)
    * sizeof(mp_limb_t) * 8);
  const size_t precision_base_10(
    boost::multiprecision::detail::digits2_2_10(precision_base_2_rounded_up));
  boost::multiprecision::mpf_float::default_precision(precision_base_10);

  two_js = ([&] {
    std::vector<std::string> string_vector;
    boost::split(string_vector, j_external_string, boost::is_any_of(","));
    if(string_vector.size() != 4)
      {
        throw std::runtime_error("wrong number of elements supplied to "
                                 "'j-external' option.  Expected 4, "
                                 "but got: "
                                 + std::to_string(string_vector.size()));
      }
    return std::array<int64_t, 4>(
      {static_cast<int64_t>(2 * std::stod(string_vector[0])),
       static_cast<int64_t>(2 * std::stod(string_vector[1])),
       static_cast<int64_t>(2 * std::stod(string_vector[2])),
       static_cast<int64_t>(2 * std::stod(string_vector[3]))});
  }());

  two_js_intermediate = ([&] {
    std::vector<std::string> string_vector;
    boost::split(string_vector, j_internal_string, boost::is_any_of(","));
    std::set<int64_t> result;
    for(auto &s : string_vector)
      {
        std::vector<std::string> spin_vector;
        boost::split(spin_vector, s, boost::is_any_of("-"));
        if(spin_vector.empty())
          {
            throw std::runtime_error(
              "Found invalid empty element in j-internal");
          }
        if(spin_vector.size() > 3)
          {
            throw std::runtime_error("Found invalid element in j-internal: '"
                                     + s + "'");
          }
        const double two_j_double_first(2 * std::stod(spin_vector.front()));
        const double two_j_double_last(spin_vector.size() > 1
                                         ? (2 * std::stod(spin_vector[1]))
                                         : two_j_double_first);

        const int64_t two_j_first(two_j_double_first),
          two_j_last(two_j_double_last);
        if(two_j_double_first != two_j_first
           || two_j_double_last != two_j_last)
          {
            throw std::runtime_error("The elements of j_internal must be "
                                     "a whole or half integer, but found: "
                                     + s);
          }
        if((two_j_first % 2) != (two_j12 % 2))
          {
            throw std::runtime_error(
              "j_12 and j_internal must all be either whole or half "
              "integers, but found:\n  j_12="
              + j12_string + ", j_internal element=" + s);
          }
        const int64_t two_j_step(
          spin_vector.size() > 2 ? 2 * std::stoi(spin_vector[2]) : 2);
        if((two_j_last - two_j_first) % two_j_step != 0)
          {
            throw std::runtime_error(
              "The step of a spin range in j_internal must exactly "
              "divide the difference between the first and last element, but "
              "found: "
              + s);
          }
        for(int64_t two_j(two_j_first); two_j <= two_j_last;
            two_j += two_j_step)
          {
            result.emplace(two_j);
          }
      }
    return result;
  }());

  q4pm = ([&] {
    std::vector<std::string> string_vector;
    boost::split(string_vector, q4_string, boost::is_any_of(","));
    if(string_vector.size() != 4)
      {
        throw std::runtime_error(
          "wrong number of elements supplied to 'four-pt-struct' option.  "
          "Expected 4, but got: "
          + std::to_string(string_vector.size()));
      }
    if(q4_sign != 1 && q4_sign != -1)
      {
        throw std::runtime_error("Four-pt-sign must be 1 or -1, but got: "
                                 + std::to_string(q4_sign));
      }
    Q4pm result({static_cast<int64_t>(2 * std::stod(string_vector[0])),
                 static_cast<int64_t>(2 * std::stod(string_vector[1])),
                 static_cast<int64_t>(2 * std::stod(string_vector[2])),
                 static_cast<int64_t>(2 * std::stod(string_vector[3]))},
                q4_sign);

    for(size_t index(0); index < 4; ++index)
      {
        if((result.vec[index] - two_js[index]) % 2 != 0)
          {
            throw std::runtime_error("For each element i, j_external[i] - "
                                     "four_pt_struct[i] must be "
                                     "an integer, but in index "
                                     + std::to_string(index)
                                     + " found j_external[i] = "
                                     + std::to_string(two_js[index] / 2.0)
                                     + ", q4[i] = " + string_vector[index]);
          }
        if(result.vec[index] < -two_js[index]
           || result.vec[index] > two_js[index])
          {
            throw std::runtime_error("For each element i, "
                                     "-j_external[i] <= four_pt_struct[i] "
                                     "<= j_external[i], but in index "
                                     + std::to_string(index)
                                     + " found j_internal[i] = "
                                     + std::to_string(two_js[index] / 2.0)
                                     + ", q4[i] = " + string_vector[index]);
          }
      }
    return result;
  }());

  is_radial = true;
  std::vector<std::string> string_vector;
  Coordinate coordinate;
  boost::split(string_vector, coordinates_string, boost::is_any_of(","));
  for(auto &s : string_vector)
    {
      coordinate = to_Coordinate(s);
      coordinates.insert(coordinate);
      is_radial = is_radial && (coordinate == Coordinate::xt_radial || coordinate == Coordinate::ws_radial);
    }
}
