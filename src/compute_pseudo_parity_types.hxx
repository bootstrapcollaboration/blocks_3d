#pragma once

#include <array>

std::array<std::array<int64_t, 2>, 2>
compute_pseudo_parity_types(const std::array<int64_t, 4> &two_js,
                            const std::array<int64_t, 4> &q4,
                            const std::array<int64_t, 3> &j12_terms,
                            const std::array<int64_t, 3> &j43_terms);

int64_t four_point_parity(const std::array<int64_t, 4> &two_js,
                          const std::array<int64_t, 4> &q4);
