#pragma once

#include "Factorial.hxx"

#include <map>
#include <tuple>

struct Clebsch_Gordan
{
  std::map<std::tuple<int64_t, int64_t, int64_t, int64_t>, Bigfloat> cache;
  const Bigfloat &
  eval(const int64_t &two_j12, const int64_t &two_j120, const int64_t &two_j,
       const int64_t &two_m) const
  {
    std::tuple<int64_t, int64_t, int64_t, int64_t> key(two_j12, two_j120,
                                                       two_j, two_m);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        // return calculate(two_j12, two_j120, two_j, two_m, factorial);
        throw std::runtime_error("Clebsch_Gordan not precomputed");
      }
  }
  const Bigfloat &
  calculate(const int64_t &two_j12, const int64_t &two_j120,
            const int64_t &two_j, const int64_t &two_m, const Factorial &factorial);

  void initialize(const int64_t &two_j12, const int64_t &two_j_max, const Factorial &factorial);
};
