#pragma once

#include "Factorial.hxx"
#include "Wigner_Cache.hxx"
#include "../timers/Timers.hxx"

#include <map>
#include <tuple>
#include <array>

Bigfloat
f_q_basis_derivative(const std::array<int64_t, 4> &two_js,
                     const int64_t &two_j,
                     const std::array<int64_t, 3> &left_q3,
                     const std::array<int64_t, 3> &right_q3,
                     // const std::array<int64_t, 4> &q4,
                     const int64_t &i, const int64_t &n,
                     const Wigner_Cache &wigner_cache, Timers &timers);

struct F_Q_Basis_Derivative
{
  // pre-computed sets of q-basis structures, grouped by {[q1,q2,q3],
  // [-q1,-q2,-q3]} the structure [0,0,0] appears alone as {[0,0,0]}
  //  index structure is [j, struct_id, group_id]
  // each structure is stored together with its flattened index
  std::vector<
    std::vector<std::vector<std::pair<int64_t, std::array<int64_t, 3>>>>>
    left_q3_sets;
  std::vector<
    std::vector<std::vector<std::pair<int64_t, std::array<int64_t, 3>>>>>
    right_q3_sets;

  // index structure [j,q3_left,q3_right, n, i]
  // the structure indices are flattened out from the grouping described above
  std::vector<std::vector<std::vector<std::vector<std::array<Bigfloat, 2>>>>>
    cache;

  void initialize(const std::array<int64_t, 4> &two_js,
                  const int64_t two_j_max, const int64_t Nmax,
                  const Wigner_Cache &wigner_cache, Timers &timers);

  const Bigfloat &
  eval_by_3pt_indices(const int64_t &two_j, const int64_t &left_ind,
                      const int64_t &right_ind, const int64_t &i,
                      const int64_t &n) const
  {
    return cache.at(two_j / 2).at(left_ind).at(right_ind).at(n).at(i);
  }

  size_t size() const {
    // prec_to_bytes_num/prec_to_bytes_den is a rational approximation
    // for (log 10 / log 2) / 8
    const size_t prec_to_bytes_num = 267;
    const size_t prec_to_bytes_den = 643;
    
    size_t total_size = sizeof(*this);

    total_size += sizeof(left_q3_sets);
    for (const auto& i1 : left_q3_sets)
      {
        total_size += sizeof(i1);
        for (const auto& i2 : i1)
          {
            total_size += sizeof(i2);
            for (const auto& pair : i2)
              {
                total_size += sizeof(pair);
              }
          }
      }

    total_size += sizeof(right_q3_sets);
    for (const auto& i1 : right_q3_sets)
      {
        total_size += sizeof(i1);
        for (const auto& i2 : i1)
          {
            total_size += sizeof(i2);
            for (const auto& pair : i2)
              {
                total_size += sizeof(pair);
              }
          }
      }

    total_size += sizeof(cache);
    for (const auto &i1 : cache)
      {
        total_size += sizeof(i1);
        for (const auto &i2 : i1)
          {
            total_size += sizeof(i2);
            for (const auto &i3 : i2)
              {
                total_size += sizeof(i3);
                for (const auto &i4 : i3)
                  {
                    total_size += sizeof(i4);
                    total_size += i4[0].precision() * prec_to_bytes_num / prec_to_bytes_den;
                    total_size += i4[1].precision() * prec_to_bytes_num / prec_to_bytes_den;
                  }
              }
          }
      }

    return total_size;
  }

};
