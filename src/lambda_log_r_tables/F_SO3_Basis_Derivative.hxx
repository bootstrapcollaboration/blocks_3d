#pragma once

#include "Factorial.hxx"
#include "Clebsch_Gordan.hxx"
#include "F_Q_Basis_Derivative.hxx"
#include "F_W_Conversion_Matrix.hxx"
#include "../timers/Timers.hxx"
#include "../compute_pseudo_parity_types.hxx"
#include "../so3_tensor_parity.hxx"

#include <map>
#include <tuple>
#include <array>

Bigfloat f_SO3_basis_derivative(
  const std::array<int64_t, 4> &two_js, const int64_t &two_j,
  const SO3 &left_SO3, const SO3 &right_SO3, const int64_t &i_4pt,
  const int64_t &n, const Clebsch_Gordan &clebsch_gordan_cache,
  const Factorial &factorial, const F_Q_Basis_Derivative &f_q_basis_derivative,
  F_W_Conversion_Matrix &f_w_conversion_matrix);

struct F_SO3_Basis_Derivative
{
  std::map<std::tuple<int64_t, SO3, SO3, int64_t>, std::vector<Bigfloat>> cache;

  void
  initialize(const std::array<int64_t, 4> &two_js, const int64_t &two_j_max,
             const int64_t &two_j12, const int64_t &two_j43,
             const std::array<int64_t, 4> &q4, const int64_t &Nmax,
             const Clebsch_Gordan &clebsch_gordan_cache,
             const Factorial &factorial,
             const F_Q_Basis_Derivative &f_q_basis_derivative,
             F_W_Conversion_Matrix &f_w_conversion_matrix);

  const std::vector<Bigfloat> &
  eval(const int64_t &two_j, const SO3 &left_SO3, const SO3 &right_SO3,
       const int64_t &i_4pt) const
  {
    std::tuple<int64_t, SO3, SO3, int64_t> key(two_j, left_SO3, right_SO3,
                                               i_4pt);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        throw std::runtime_error("F_SO3_Basis_Derivative not precomputed!");
      }
  }
};
