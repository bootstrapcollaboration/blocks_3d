#include "F_Q_Basis_Derivative.hxx"

void F_Q_Basis_Derivative::initialize(const std::array<int64_t, 4> &two_js,
                                      const int64_t two_j_max,
                                      const int64_t Nmax,
                                      const Wigner_Cache &wigner_cache,
                                      Timers &timers)
{
  left_q3_sets.resize(two_j_max / 2 + 1);
  right_q3_sets.resize(two_j_max / 2 + 1);

  cache.resize(two_j_max / 2 + 1);

  // initialize structure lists
  for(int64_t two_j = two_j_max % 2; two_j <= two_j_max; two_j += 2)
    {
      const int64_t two_j_index = two_j / 2;
      auto &left_q3_set = left_q3_sets[two_j_index];
      auto &right_q3_set = right_q3_sets[two_j_index];
      auto &cache_at_j = cache[two_j_index];

      left_q3_set.reserve((two_js[0] + 1) * (two_js[1] + 1));
      right_q3_set.reserve((two_js[2] + 1) * (two_js[3] + 1));

      int64_t lsize = 0, rsize = 0;

      // We are grouping q-structures into pairs related by parity
      for(int64_t two_p1(two_js[0] % 2); two_p1 <= two_js[0]; two_p1 += 2)
        for(int64_t two_p2(two_p1 == 0 ? two_js[1] % 2 : -two_js[1]);
            two_p2 <= two_js[1]; two_p2 += 2)
          {
            int64_t two_p(-two_p1 - two_p2);
            if(std::abs(two_p) <= two_j)
              {
                if(two_p1 == 0 && two_p2 == 0)
                  {
                    left_q3_set.push_back({{lsize, {two_p1, two_p2, two_p}}});
                    ++lsize;
                  }
                else
                  {
                    left_q3_set.push_back(
                      {{lsize, {two_p1, two_p2, two_p}}, {lsize+1, {-two_p1, -two_p2, -two_p}}});
                    lsize += 2;
                  }
              }
          }

      // Note that the order of these two loops is switched from left_q3_set.
      for(int64_t two_p4(two_js[3] % 2); two_p4 <= two_js[3]; two_p4 += 2)
        for(int64_t two_p3(two_p4 == 0 ? two_js[2] % 2 : -two_js[2]);
            two_p3 <= two_js[2]; two_p3 += 2)
          {
            int64_t two_p(-two_p4 - two_p3);
            if(std::abs(two_p) <= two_j)
              {
                if(two_p4 == 0 && two_p3 == 0)
                  {
                    right_q3_set.push_back({{rsize, {two_p4, two_p3, two_p}}});
                    ++rsize;
                  }
                else
                  {
                    right_q3_set.push_back(
                      {{rsize, {two_p4, two_p3, two_p}}, {rsize+1, {-two_p4, -two_p3, -two_p}}});
                    rsize += 2;
                  }
              }
          }

      cache_at_j.resize(lsize);
      for(const auto &left_group : left_q3_set)
        for(const auto &left_q3 : left_group)
          {
            cache_at_j.at(left_q3.first).resize(rsize);
            for(const auto &right_group : right_q3_set)
              for(const auto &right_q3 : right_group)
                {
                  auto &cache_cell
                    = cache_at_j.at(left_q3.first).at(right_q3.first);
                  cache_cell.reserve(Nmax + 1);
                  for(int64_t n = 0; n <= Nmax; ++n)
                    {
                      cache_cell.push_back(
                        {f_q_basis_derivative(two_js, two_j, left_q3.second, right_q3.second,
                                              0, n, wigner_cache, timers),
                         f_q_basis_derivative(two_js, two_j, left_q3.second, right_q3.second,
                                              1, n, wigner_cache, timers)});
                    }
                }
          }
    }
}
