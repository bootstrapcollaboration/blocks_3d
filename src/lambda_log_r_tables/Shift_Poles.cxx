#include "../Eigen.hxx"

const std::vector<std::pair<Bigfloat, Bigfloat>> shift_poles(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
  const std::vector<Bigfloat> &keep, const Eigen::PartialPivLU<Eigen_Matrix> &lu)
{
  const int64_t size(keep.size());
  Bigfloat temp;
  Eigen_Vector b(Eigen_Vector::Zero(size));
  for(auto &pole : unprotected_poles)
    {
      temp = pole.second;
      temp *= pow(pole.first, -(size / 2));
      for(int64_t row = 0; row < size; ++row)
        {
          b(row) += temp; // pole.second * pole.first ^ (row - (size / 2))
          if(row != size - 1)
            {
              temp *= pole.first;
            }
        }
    }

  Eigen_Vector x = lu.solve(b);

  std::vector<std::pair<Bigfloat, Bigfloat>> result;
  result.reserve(size);
  for(int64_t row = 0; row < size; ++row)
    {
      result.emplace_back(keep[row], x(row));
    }
  return result;
}
