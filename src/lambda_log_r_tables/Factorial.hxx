#pragma once

#include "../Bigfloat.hxx"

#include <vector>

struct Factorial
{
  std::vector<Bigfloat> cache, inverse_cache;

  const Bigfloat &eval(const int64_t &index) const
  {
    if(static_cast<size_t>(index) >= cache.size())
      {
        throw std::runtime_error("Factorial cache size exceeded");
      }
    return cache[index];
  }
  const Bigfloat &inverse_eval(const int64_t &index) const
  {
    if(static_cast<size_t>(index) >= cache.size())
      {
        throw std::runtime_error("Factorial cache size exceeded");
      }
    return inverse_cache[index];
  }

  void initialize(const int64_t &new_size)
  {
    cache.reserve(new_size);
    inverse_cache.reserve(new_size);
    cache.emplace_back(1);
    inverse_cache.emplace_back(1);
    for(int64_t index = 1; index != new_size; ++index)
      {
        cache.emplace_back(index * cache[index - 1]);
        inverse_cache.emplace_back(1 / cache[index]);
      }
  }
};
