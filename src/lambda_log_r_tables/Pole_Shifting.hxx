#pragma once

#include "../Bigfloat.hxx"
#include "../Eigen.hxx"
#include "Pole_Types.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>
#include <set>

struct Pole_Shifting
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    pole_complement_products;
  std::vector<boost::math::tools::polynomial<Bigfloat>> pole_products;
  std::vector<Eigen::PartialPivLU<Eigen_Matrix>> pole_LUs;
  std::vector<std::vector<Bigfloat>> new_unprotected_pole_lists;
  std::vector<int64_t> result_pole_list_sizes;

  void initialize(const std::set<int64_t> &two_js, const int64_t &two_j12,
                  const int64_t &two_j43, const int64_t &order, const int64_t &kept_pole_order,
                  const Pole_Types &pole_types, bool should_do_pole_shifting);
};
