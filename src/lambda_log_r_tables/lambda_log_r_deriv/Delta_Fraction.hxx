#pragma once

#include "../../Bigfloat.hxx"
#include "../../ostream_pair.hxx"
#include "../../ostream_vector.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <utility>
#include <vector>

// represents a meromorphic function of Delta on the Riemann sphere
// that is, it is meromorphic in C and polynomially-bounded at infintiy
struct Delta_Fraction
{
  // The polynomial part P(x)
  boost::math::tools::polynomial<Bigfloat> polynomial;
  // A list of pairs (r_i, a_i), one for each pole a_i / (x - r_i)
  std::vector<std::pair<Bigfloat, Bigfloat>> residues;

  size_t size() const {
    // prec_to_bytes_num/prec_to_bytes_den is a rational approximation
    // for (log 10 / log 2) / 8
    const size_t prec_to_bytes_num = 267;
    const size_t prec_to_bytes_den = 643;
    
    size_t total_size = sizeof(*this);

    total_size += sizeof(polynomial);
    if (polynomial.size() > 0)
      {
        total_size += polynomial.size() * polynomial.data()[0].precision() * prec_to_bytes_num / prec_to_bytes_den;
      }

    total_size += sizeof(residues);
    for (const auto& r : residues)
      {
        total_size += r.first.precision() * prec_to_bytes_num / prec_to_bytes_den;
        total_size += r.second.precision() * prec_to_bytes_num / prec_to_bytes_den;
      }

    return total_size;
  }
  
};

inline std::ostream &
operator<<(std::ostream &os, const Delta_Fraction &fraction)
{
  os << "{\"polynomial\": " << fraction.polynomial << ", "
     << "\"residues\": " << fraction.residues << "}";
  return os;
}
