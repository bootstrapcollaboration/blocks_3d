#include "Delta_Fraction.hxx"
#include "../Cache.hxx"
#include "../../Log_r_Derivatives.hxx"
#include "../../Q4pm.hxx"

#include "../../timers/Timers.hxx"

#include <boost/filesystem.hpp>

#include <set>
#include <sstream>
#include <thread>

std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
conformal_block_derivatives(
  const std::array<int64_t, 4> &two_js, const Bigfloat &delta_12,
  const int64_t &two_j12, const Bigfloat &delta_43, const int64_t &two_j43,
  const Q4pm &q4pm, const std::set<int64_t> &two_js_intermediate,
  const int64_t &order, const int64_t &n, const int64_t &r_derivs,
  const M_Matrix &m_matrix, const Pole_Types &pole_types,
  const F_SO3_Basis_Derivative &f_so3_basis_derivative,
  const size_t &num_threads, Timers &timers);

Log_r_Derivatives
shift_block(const std::vector<Delta_Fraction> &block_vector,
            const int64_t &two_j, const int64_t &two_j_index,
            const int64_t &two_j12, const int64_t &two_j43,
            const int64_t &kept_pole_order, const Pole_Types &pole_types,
            const Pole_Shifting &pole_shifting,
            const bool should_do_pole_shifting);

size_t
blocks_size(
  const std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
  &blocks)
{
  size_t total_size = 0;
  for (const auto& i1 : blocks)
    {
      for (const auto& i2 : i1)
        {
          for (const auto& i3 : i2)
            {
              for (const auto& i4 : i3)
                {
                  for (const auto& i5 : i4)
                    {
                      total_size += i5.size();
                    }
                }
            }
        }
    }
  return total_size;
}

// returned vector is indexed by
// [spin_index, parity_index, left_3pt_index, right_3pt_index]
std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>
lambda_log_r_deriv(const std::array<int64_t, 4> &two_js,
                   const Bigfloat &delta_12, const Bigfloat &delta_43,
                   const int64_t &two_j12, const int64_t &two_j43,
                   const Q4pm &q4pm,
                   const std::set<int64_t> &two_js_intermediate,
                   const int64_t &r_derivs, const int64_t &n,
                   const int64_t &kept_pole_order, const int64_t &order,
                   const M_Matrix &m_matrix, const Pole_Types &pole_types,
                   const F_SO3_Basis_Derivative &f_so3_basis_derivative,
                   const Pole_Shifting &pole_shifting,
                   const size_t &num_threads, Timers &timers)
{
  std::stringstream ss;
  ss << "lambda_log_r_deriv." << n << "";
  auto &all_timer(timers.add_and_start(ss.str()));
  timers.enter(ss.str());
  auto &blocks_timer(timers.add_and_start("conformal_block_derivatives"));

  // compute rational approximations to derivatives; most of the magic happens
  // here
  std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
    blocks(conformal_block_derivatives(
      two_js, delta_12, two_j12, delta_43, two_j43, q4pm, two_js_intermediate,
      order, n, r_derivs, m_matrix, pole_types, f_so3_basis_derivative,
      num_threads, timers));

  blocks_timer.stop();

  auto &shift_timer(timers.add_and_start("shift"));
  std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>> result;
  result.reserve(two_js_intermediate.size());

  const bool should_do_pole_shifting = kept_pole_order < order;

  // blocks only has entries for the elements of
  // two_js_intermediate, which may skip values of two_j. we use two_j_index to
  // go through two_js_intermediate
  size_t two_j_index(0);

  // simply map through `blocks` (all but one level) and apply
  // scalar-block-type pole shifting procedure `shift_block`
  // `shift_block` currently applies to all log(r)-derivatives at once
  // different left&right structures and parities are different calls
  //
  // TODO: this could be parallelized

  std::vector<std::tuple<const std::vector<Delta_Fraction> &, int64_t, int64_t,
                         Log_r_Derivatives &>>
    tasks;
  tasks.reserve(two_js_intermediate.size() * (two_j12 + 1) * (two_j43 + 1));

  result.reserve(two_js_intermediate.size());
  for(auto &two_j : two_js_intermediate)
    {
      result.emplace_back();
      auto &two_js_out(result.back());
      two_js_out.reserve(blocks.at(two_j_index).size());
      for(auto &parity_in : blocks.at(two_j_index))
        {
          two_js_out.emplace_back();
          auto &parities_out(two_js_out.back());
          parities_out.reserve(parity_in.size());
          for(auto &left_in : parity_in)
            {
              parities_out.emplace_back();
              auto &lefts_out(parities_out.back());
              lefts_out.reserve(left_in.size());
              for(const auto &right_in : left_in)
                {
                  lefts_out.emplace_back();
                  tasks.emplace_back(right_in, two_j, two_j_index,
                                     lefts_out.back());
                }
            }
        }
      ++two_j_index;
    }

  std::mutex task_mtx;
  size_t current_task_index = 0;
  std::vector<std::thread> threads;

  for(size_t i = 0; i < num_threads; ++i)
    {
      threads.emplace_back([&]() {
        while(true)
          {
            std::tuple<const std::vector<Delta_Fraction> &, int64_t, int64_t,
                       Log_r_Derivatives &> *task_ptr
              = nullptr;

            // Synchronized block to fetch the next task
            {
              std::lock_guard<std::mutex> lock(
                task_mtx); // Automatically releases the lock when out of scope
              if(current_task_index < tasks.size())
                {
                  task_ptr = &tasks[current_task_index++];
                }
              else
                {
                  return; // No more tasks to process
                }
            }
            auto [right_in, two_j, two_j_index, target] = *task_ptr;
            target = shift_block(right_in, two_j, two_j_index, two_j12,
                                 two_j43, kept_pole_order, pole_types,
                                 pole_shifting, should_do_pole_shifting);
          }
      });
    }

  for(auto &thread : threads)
    {
      thread.join();
    }

  shift_timer.stop();
  all_timer.stop();
  timers.exit();
  return result;
}
