#include "../Delta_Fraction.hxx"
#include "../../Cache.hxx"

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<std::pair<Bigfloat,Bigfloat>> &poles,
  const boost::math::tools::polynomial<Bigfloat> &polynomial,
  const boost::math::tools::polynomial<Bigfloat> &pole_product,
  const std::vector<boost::math::tools::polynomial<Bigfloat>>
    &pole_complement_product)
{
  std::vector<Bigfloat> pole_locations, pole_residues;
  for (auto &pole : poles)
    {
      pole_locations.push_back(pole.first);
      pole_residues.push_back(pole.second);
    }
  
  boost::math::tools::polynomial<Bigfloat> product(
    polynomial * pole_product);

  boost::math::tools::polynomial<Bigfloat> sum;
  for(size_t index(0); index < pole_residues.size(); ++index)
    {
      sum += pole_residues[index] * pole_complement_product[index];
    }
  return product + sum;
}
