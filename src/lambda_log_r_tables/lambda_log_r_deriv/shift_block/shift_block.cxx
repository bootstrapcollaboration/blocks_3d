#include "../Delta_Fraction.hxx"
#include "../../Cache.hxx"
#include "../../../Log_r_Derivatives.hxx"

#include <set>

Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset);

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &poles,
  const boost::math::tools::polynomial<Bigfloat> &polynomial,
  const boost::math::tools::polynomial<Bigfloat> &pole_product,
  const std::vector<boost::math::tools::polynomial<Bigfloat>>
    &pole_complement_product);

const std::vector<std::pair<Bigfloat, Bigfloat>>
shift_poles(const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
            const std::vector<Bigfloat> &keep,
            const Eigen::PartialPivLU<Eigen_Matrix> &lu);

bool is_protected(const Bigfloat &p);

// Locations of poles in x for the given order, where x = Delta -
// unitarity_bound
std::vector<Bigfloat>
pole_locations_x(const int64_t &two_j, const int64_t &two_j12,
                 const int64_t &two_j43, const int64_t &order,
                 const Pole_Types &pole_types);

// This function does two conceptually different "shift"s. Firstly, it
// shifts Delta -> unitarity_bound + x, which amounts to a simple
// redefinition inside the rational functions. Secondly, it does a
// more nontrivial approximation of a sum of poles in terms of a
// smaller sum of poles.
Log_r_Derivatives
shift_block(const std::vector<Delta_Fraction> &block_vector,
            const int64_t &two_j, const int64_t &two_j_index,
            const int64_t &two_j12, const int64_t &two_j43,
            const int64_t &kept_pole_order, const Pole_Types &pole_types,
            const Pole_Shifting &pole_shifting,
            const bool should_do_pole_shifting)
{
  Log_r_Derivatives result;

  const Bigfloat unitarity_bound
    = two_j <= 1 ? two_j * 0.5 + 0.5 : two_j * 0.5 + 1;

  std::vector<Bigfloat> new_pole_locations
    = pole_locations_x(two_j, two_j12, two_j43, kept_pole_order, pole_types);

  const std::vector<Bigfloat> &new_unprotected_pole_locations
    = pole_shifting.new_unprotected_pole_lists.at(two_j_index);

  for(auto &block_of_delta : block_vector)
    {
      // Convert a function of Delta to a function of x
      const Delta_Fraction block_of_x(
        shift_fraction(block_of_delta, unitarity_bound));

      std::vector<std::pair<Bigfloat, Bigfloat>> result_poles;

      if(should_do_pole_shifting)
        {
          result_poles.reserve(
            pole_shifting.result_pole_list_sizes.at(two_j_index));
          std::vector<std::pair<Bigfloat, Bigfloat>> unprotected_poles;
          for(auto &pole : block_of_x.residues)
            {
              if(is_protected(pole.first))
                {
                  result_poles.push_back(pole);
                }
              else
                {
                  unprotected_poles.push_back(pole);
                }
            }

          // Given unprotected poles \sum_i a_i/(x-r_i), find a best
          // approximation in the form \sum_j a'_j/(x-r_j), where j runs
          // over 'new_unprotected_pole_locations'
          std::vector<std::pair<Bigfloat, Bigfloat>> shifted_poles(
            shift_poles(unprotected_poles, new_unprotected_pole_locations,
                        pole_shifting.pole_LUs.at(two_j_index)));

          for(auto &pole : shifted_poles)
            {
              result_poles.push_back(pole);
            }
        }
      else
        {
          result_poles = block_of_x.residues;
        }

      result.numerator.emplace_back(together_with_factors(
        result_poles, block_of_x.polynomial,
        pole_shifting.pole_products.at(two_j_index),
        pole_shifting.pole_complement_products.at(two_j_index)));
    }
  return result;
}
