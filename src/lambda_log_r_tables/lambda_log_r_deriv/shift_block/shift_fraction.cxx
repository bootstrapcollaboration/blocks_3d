#include "../Delta_Fraction.hxx"

// Given a Delta_Fraction F(x) = P(x) + \sum_i a_i / (x-r_i), compute
// the shifted Delta_Fraction F(x + offset).
Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset)
{
  Delta_Fraction result;
  boost::math::tools::polynomial<Bigfloat> offset_atom({offset, 1});
  boost::math::tools::polynomial<Bigfloat> offset_power({1});
  for(size_t power(0); power < fraction.polynomial.size(); ++power)
    {
      result.polynomial += fraction.polynomial[power] * offset_power;
      offset_power *= offset_atom;
    }

  result.residues.reserve(fraction.residues.size());
  for(auto &residue : fraction.residues)
    {
      result.residues.emplace_back(residue.first - offset, residue.second);
    }
  return result;
}
