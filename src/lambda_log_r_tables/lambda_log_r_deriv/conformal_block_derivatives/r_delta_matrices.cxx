#include "../../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>
#include <utility>

// A matrix representing the action of multiplication by Exp[Delta*t]
// on a set of (r,\[Eta]) derivatives, with an overall factor of
// r^Delta stripped off.

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
r_delta_matrices(const int64_t &r_derivs)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(
    r_derivs + 1);
  for(int64_t k_new(0); k_new != int64_t(result.size()); ++k_new)
    {
      result[k_new].reserve(r_derivs + 1);
      for(int64_t k_old(0); k_old != r_derivs + 1; ++k_old)
        {
          std::vector<Bigfloat> vec(k_old > k_new ? 1 : (k_new - k_old + 1), 0);
          vec.back() = binomial_coefficient(k_new, k_old);
          result[k_new].emplace_back(vec.begin(), vec.end());
        }
    }
  return result;
}
