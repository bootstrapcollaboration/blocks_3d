#include <cstdint>
#include <vector>

// The values of 2*j for which we need to compute blocks at r_order-th
// order in r
// [CHANGED] to use the order inside of the residue block (differs by n_i)
std::vector<int64_t>
two_j_values(const int64_t &r_order_m, const int64_t &order,
             const int64_t &two_j12, const int64_t &two_j_max)
{
  std::vector<int64_t> result;
  for(int64_t v(two_j12 % 2); v <= two_j_max + 2 * (order - r_order_m - 1); v += 2) // Added -1 for tilde m change
    {
      result.push_back(v);
    }
  return result;
}
