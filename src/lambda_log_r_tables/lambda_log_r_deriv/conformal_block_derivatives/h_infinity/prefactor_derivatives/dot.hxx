#pragma once

#include <vector>
#include <stdexcept>
#include <string>

template <typename T> T dot(const std::vector<T> &a, const std::vector<T> &b)
{
  T result(0);
  if(a.size() != b.size())
    {
      throw std::runtime_error(
        "INTERNAL ERROR: In dot(), different sizes for a and b: "
        + std::to_string(a.size()) + " != " + std::to_string(b.size()));
    }
  for(size_t index = 0; index < a.size(); ++index)
    {
      result += a[index] * b[index];
    }
  return result;
}
