#include "../../../Cache.hxx"
#include "../../../../Bigfloat.hxx"
#include "../two_j_values.hxx"
#include "../../../../so3_tensor_parity.hxx"
#include "../../../../compute_pseudo_parity_types.hxx"

#include "../../../../timers/Timers.hxx"

#include <thread>

std::vector<std::vector<Bigfloat>>
prefactor_derivatives(const int64_t &order,
                      const std::vector<std::vector<Bigfloat>> &series,
                      const Bigfloat &c, const int64_t &k_max);

Bigfloat full_derivative(const int64_t &r_order, const int64_t &two_j,
                         const SO3 &left_SO3, const SO3 &right_SO3,
                         const int64_t &i_4pt, const int64_t &n,
                         const std::vector<std::vector<Bigfloat>> &prefactor,
                         const F_SO3_Basis_Derivative &f_so3_basis_derivative,
                         Timers &timers);

std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
h_infinity(const std::array<int64_t, 4> &two_js,
           const std::vector<std::vector<Bigfloat>> &series,
           const int64_t &two_j12, const int64_t &two_j43,
           const std::array<int64_t, 4> &q4, const int64_t &i_4pt,
           const int64_t &two_j_max, const int64_t &order, const int64_t &n,
           const F_SO3_Basis_Derivative &f_so3_basis_derivative, const size_t &num_threads,
           Timers &timers)
{
  timers.enter("h_infinity");
  const Bigfloat c((q4[0] + q4[1]) / 4.0);
  auto &prefactor_timer = timers.add_and_start("prefactor_derivatives");
  const std::vector<std::vector<Bigfloat>> prefactor(
    prefactor_derivatives(order, series, c, n));
  prefactor_timer.stop();

  std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4, {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));

  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    result;

  std::vector<std::tuple<int64_t, int64_t, SO3, SO3, Bigfloat &>> tasks;
  tasks.reserve((order + 1) * (two_j_max / 2 + order + 1) * (two_j12 + 1)
                * (two_j43 + 1));
  auto &full_derivative_timer = timers.add_and_start("full_derivative");
  result.reserve(2);
  for(int64_t pp_index(0); pp_index != 2; ++pp_index)
    {
      result.emplace_back();
      auto &pp_back(result.back());
      pp_back.reserve(order + 1);
      for(int64_t r_order(0); r_order <= order; ++r_order)
        {
          pp_back.emplace_back();
          auto &r_back(pp_back.back());
          // this is a bit of a hacky use of two_j_values.
          std::vector<int64_t> two_j_values_local
            = two_j_values(r_order, order + 1, two_j12, two_j_max);
          r_back.reserve(two_j_values_local.size());
          for(int64_t &two_j : two_j_values_local)
            {
              r_back.emplace_back();
              auto &two_j_back(r_back.back());
              auto left_structs = so3_tensor_pseudo_parity(
                pseudo_parity_types[pp_index][0], two_j12, two_j);
              two_j_back.reserve(left_structs.size());
              for(auto &two_j120 : left_structs)
                {
                  two_j_back.emplace_back();
                  auto &two_j120_back(two_j_back.back());
                  auto right_structs = so3_tensor_pseudo_parity(
                    pseudo_parity_types[pp_index][1], two_j43, two_j);
                  two_j120_back.reserve(right_structs.size());
                  for(auto &two_j430 : right_structs)
                    {
                      two_j120_back.emplace_back();
                      tasks.emplace_back(
                        r_order, two_j, SO3(two_j12, two_j120),
                        SO3(two_j43, two_j430), two_j120_back.back());
                    }
                }
            }
        }
    }
  std::mutex task_mtx;
  size_t current_task_index = 0;
  std::vector<std::thread> threads;

  for(size_t i = 0; i < num_threads; ++i)
    {
      threads.emplace_back([&]() {
        while(true)
          {
            std::tuple<int64_t, int64_t, SO3, SO3, Bigfloat &> *task_ptr
              = nullptr;

            // Synchronized block to fetch the next task
            {
              std::lock_guard<std::mutex> lock(
                task_mtx); // Automatically releases the lock when out of scope
              if(current_task_index < tasks.size())
                {
                  task_ptr = &tasks[current_task_index++];
                }
              else
                {
                  return; // No more tasks to process
                }
            }
            auto [r_order, two_j, left, right, target] = *task_ptr;
            target
              = full_derivative(r_order, two_j, left, right, i_4pt, n,
                                prefactor, f_so3_basis_derivative, timers);
          }
      });
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
  full_derivative_timer.stop();
  timers.exit();
  return result;
}
