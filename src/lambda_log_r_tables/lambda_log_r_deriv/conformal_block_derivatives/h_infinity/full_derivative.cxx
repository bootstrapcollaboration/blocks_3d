#include "../../../F_SO3_Basis_Derivative.hxx"
#include "../../../../timers/Timers.hxx"

Bigfloat
full_derivative(const int64_t &r_order,
                const int64_t &two_j, const SO3 &left_SO3,
                const SO3 &right_SO3, const int64_t &i_4pt, const int64_t &n,
                const std::vector<std::vector<Bigfloat>> &prefactor,
                const F_SO3_Basis_Derivative &f_so3_basis_derivatives,
                Timers &timers)
{
  // timers.enter("full_derivative");
  thread_local auto &f_der_timer = timers.add_and_start("full_derivative.eval");
  f_der_timer.start();
  const std::vector<Bigfloat> &f_derivs
    = f_so3_basis_derivatives.eval(two_j, left_SO3, right_SO3, i_4pt);
  f_der_timer.stop();

  thread_local auto &combine_timer = timers.add_and_start("full_derivative.combine");
  combine_timer.start();
  Bigfloat result(0);
  for(int64_t k(0); k <= n; ++k)
    {
      result += binomial_coefficient(n, k) * f_derivs[k]
                * prefactor[n - k][r_order];
    }
  combine_timer.stop();
  // timers.exit();
  return result;
}
