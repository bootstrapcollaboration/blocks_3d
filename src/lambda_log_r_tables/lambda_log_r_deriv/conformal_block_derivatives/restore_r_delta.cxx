#include "../Delta_Fraction.hxx"
#include "../../../timers/Timers.hxx"

// To get g from h, we must multiply by r^Delta.  This function performs that
// transformation on the PoleSeries of derivatives, resulting in a list of
// PartialFractions. (Derivatives of r^Delta result in polynomials in Delta,
// which is why we have Delta_Fraction.)

std::vector<Delta_Fraction> restore_r_delta(
  const std::vector<Bigfloat> &constants,
  const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>> &poles,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta, Timers &timers)
{
  thread_local Timer &full_timer = timers.add_and_start("restore_r_delta");
  full_timer.start();

  std::vector<std::vector<std::pair<boost::math::tools::polynomial<Bigfloat>,
                                    boost::math::tools::polynomial<Bigfloat>>>>
    pole_products;

  pole_products.reserve(poles.size());
  for(auto &pole : poles)
    {
      pole_products.emplace_back();
      auto &pole_products_back(pole_products.back());
      pole_products_back.reserve(r_delta.size());
      for(auto &r_delta_vec : r_delta)
        {
          boost::math::tools::polynomial<Bigfloat> r_dot_pole;
          for(size_t deriv_of_derivs(0); deriv_of_derivs < r_delta_vec.size();
              ++deriv_of_derivs)
            {
              r_dot_pole += r_delta_vec[deriv_of_derivs]
                            * pole.second.at(deriv_of_derivs);
            }
          boost::math::tools::polynomial<Bigfloat> divisor({-pole.first, 1});
          pole_products_back.emplace_back(
            boost::math::tools::quotient_remainder(r_dot_pole, divisor));
        }
    }

  std::vector<Delta_Fraction> result;
  for(size_t deriv_element(0); deriv_element != r_delta.size();
      ++deriv_element)
    {
      result.emplace_back();
      auto &polynomial(result.back().polynomial);
      for(size_t deriv_of_derivs(0);
          deriv_of_derivs != r_delta[deriv_element].size(); ++deriv_of_derivs)
        {
          polynomial += r_delta[deriv_element][deriv_of_derivs]
                        * constants[deriv_of_derivs];
        }
      for(auto &pole_product : pole_products)
        {
          polynomial += pole_product[deriv_element].first;
        }

      auto &residues(result.back().residues);
      for(size_t pole(0); pole < pole_products.size(); ++pole)
        {
          // If the remainder is zero, the remainder is empty.  So
          // need to make sure not to access an empty element.
          residues.emplace_back(
            poles[pole].first,
            pole_products[pole][deriv_element].second.size() == 0
              ? Bigfloat(0)
              : pole_products[pole][deriv_element].second[0]);
        }
    }
  
  full_timer.stop();
  return result;
}
