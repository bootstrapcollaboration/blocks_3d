#include "../Delta_Fraction.hxx"
#include "../../Cache.hxx"
#include "../../../Q4pm.hxx"
#include "../../../so3_tensor_parity.hxx"
#include "../../../compute_pseudo_parity_types.hxx"

#include "../../../timers/Timers.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <numeric>
#include <set>
#include <thread>
#include <mutex>

void fill_binomials(const Bigfloat &delta_12, const Bigfloat &delta_43,
                    const std::array<int64_t, 4> &q4, const int64_t &order,
                    std::vector<Bigfloat> &binomial_a,
                    std::vector<Bigfloat> &binomial_ab,
                    std::vector<Bigfloat> &binomial_b,
                    std::vector<Bigfloat> &binomial_bb);

std::vector<std::vector<Bigfloat>>
h_infinity_double_series(const int64_t &order,
                         const std::vector<Bigfloat> &binomial_a,
                         const std::vector<Bigfloat> &binomial_ab,
                         const std::vector<Bigfloat> &binomial_b,
                         const std::vector<Bigfloat> &binomial_bb);

std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
h_infinity(const std::array<int64_t, 4> &two_js,
           const std::vector<std::vector<Bigfloat>> &series,
           const int64_t &two_j12, const int64_t &two_j43,
           const std::array<int64_t, 4> &q4, const int64_t &i,
           const int64_t &two_j_max, const int64_t &order, const int64_t &n,
           const F_SO3_Basis_Derivative &f_so3_basis_derivative,
           const size_t &num_threads, Timers &timers);

std::vector<
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
radial_recursion(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const M_Matrix &m_matrix, const Pole_Types &pole_types,
  const size_t &num_threads);

size_t
residues_size(
  const std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
  &residues);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
r_delta_matrices(const int64_t &r_derivs);

std::vector<Delta_Fraction> restore_r_delta(
  const std::vector<Bigfloat> &constants,
  const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>> &poles,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  Timers &timers);

template <typename T> T negate(const T &vec)
{
  T result(vec);
  for(auto &v : result)
    {
      v = -v;
    }
  return result;
}

std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
conformal_block_derivatives(
  const std::array<int64_t, 4> &two_js, const Bigfloat &delta_12,
  const int64_t &two_j12, const Bigfloat &delta_43, const int64_t &two_j43,
  const Q4pm &q4pm, const std::set<int64_t> &two_js_intermediate,
  const int64_t &order, const int64_t &n, const int64_t &r_derivs,
  const M_Matrix &m_matrix, const Pole_Types &pole_types,
  const F_SO3_Basis_Derivative &f_so3_basis_derivative,
  const size_t &num_threads, Timers &timers)
{
  timers.enter("conformal_block_derivatives");

  Timer &init_timer = timers.add_and_start("init");

  // find the two combinations of pseudoparities that need to be computed
  const std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4pm.vec,
                                {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));

  // precompute some binomial coefficients and power series for h_infinity
  std::vector<Bigfloat> binomial_a, binomial_ab, binomial_b, binomial_bb;
  fill_binomials(delta_12, delta_43, q4pm.vec, order, binomial_a, binomial_ab,
                 binomial_b, binomial_bb);

  const std::vector<std::vector<Bigfloat>> series(h_infinity_double_series(
    order, binomial_a, binomial_ab, binomial_b, binomial_bb)),
    series_b(h_infinity_double_series(order, binomial_ab, binomial_a,
                                      binomial_bb, binomial_b));

  const int64_t two_j_max(*two_js_intermediate.rbegin());

  init_timer.stop();

  // compute h_infinity for q4pm and -q4pm
  auto &h_inf_timer = timers.add_and_start("h_infinity");
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    h_inf(h_infinity(two_js, series, two_j12, two_j43, q4pm.vec, 0, two_j_max,
                     order, n, f_so3_basis_derivative, num_threads, timers));
  const std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    h_temp(h_infinity(two_js, series_b, two_j12, two_j43, negate(q4pm.vec), 1,
                      two_j_max, order, n, f_so3_basis_derivative, num_threads, timers));
  h_inf_timer.stop();

  Timer &combine_timer = timers.add_and_start("combine_h_infinities");

  const int64_t factor(
    q4pm.i
    * (std::accumulate(two_js.begin(), two_js.end(), 0) % 4 == 0 ? 1 : -1));

  // combine into h_infinity of definite y-parity
  for(size_t pp_index(0); pp_index != h_inf.size(); ++pp_index)
    for(size_t r_order(0); r_order != h_inf[pp_index].size(); ++r_order)
      for(size_t two_j(0); two_j != h_inf[pp_index][r_order].size(); ++two_j)
        for(size_t two_j120(0);
            two_j120 != h_inf[pp_index][r_order][two_j].size(); ++two_j120)
          for(size_t two_j430(0);
              two_j430 != h_inf[pp_index][r_order][two_j][two_j120].size();
              ++two_j430)
            {
              // The factor of 1/2 was introduced relative to original
              // Mathematica prototype in order to match scalar_blocks
              // (Mathematica code now also has this factor)
              h_inf[pp_index][r_order][two_j][two_j120][two_j430]
                += factor
                   * h_temp[pp_index][r_order][two_j][two_j120][two_j430];
              h_inf[pp_index][r_order][two_j][two_j120][two_j430] *= 0.5;
            }

  combine_timer.stop();

  // using h_infinity as the initial condition, perform radial recursion to
  // obtain power series in r; most of the magic is here
  auto &recursion_timer = timers.add_and_start("recursion");
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    residues(radial_recursion(h_inf, two_j_max, two_j12, two_j43,
                              pseudo_parity_types, order, m_matrix, pole_types,
                              num_threads));
  recursion_timer.stop();

  // TODO: This does nontrivial computation. Only do at high
  // verbosity.
  // std::cout << "lambda_order = " << n
  //           << ", residues_size(residues) = " << residues_size(residues)
  //           << std::endl;

  // The remaining part of this function converts the power series in r to
  // log(r)-derivatives at r_crossing.

  auto &derivatives_timer = timers.add_and_start("residues_to_derivatives");
  timers.enter("residues_to_derivatives");

  const Bigfloat two_point_A0(1), r_crossing(3 - 2 * sqrt(Bigfloat(2))),
    term(two_point_A0 * r_crossing);

  std::vector<std::vector<Bigfloat>> coeffs_to_derivs(
    r_derivs + 1, std::vector<Bigfloat>(order + 1));
  for(int64_t r_order(0); r_order != order + 1; ++r_order)
    {
      const Bigfloat pow_term(pow(term, r_order));
      for(size_t k(0); k != coeffs_to_derivs.size(); ++k)
        {
          // TODO: To speed this up, we can compute this iteratively
          // without using pow().
          coeffs_to_derivs[k][r_order]
            = (r_order == 0 && k == 0 ? Bigfloat(1)
                                      : pow(Bigfloat(r_order), k))
              * pow_term;
        }
    }

  // polynomial-valued matrix which coverts log(r) derivatives of g
  // to r^{-Delta}(log(r) derivatives of r^Delta g)
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    r_delta(r_delta_matrices(r_derivs));

  auto &main_derivatives_loop = timers.add_and_start("main_loop");

  // two_j, pp_index, left_so3_index, right_so3_index
  std::vector<
    std::tuple<int64_t, size_t, size_t, size_t, std::vector<Delta_Fraction> &>>
    tasks;
  tasks.reserve(two_js_intermediate.size() * (two_j12 + 1) * (two_j43 + 1)
                / 2);
  std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
    result;
  for(const int64_t &two_j : two_js_intermediate)
    {
      // two_js_intermediate may skip values of two_j, but h_inf and
      // residues both cover the entire range.  So we compute an index
      // for two_j based on its value.
      result.emplace_back();
      auto &two_j_back(result.back());
      for(size_t pp_index(0); pp_index != 2; ++pp_index)
        {
          two_j_back.emplace_back();
          auto &pp_back(two_j_back.back());
          // TODO: We create these parity vectors just to get the
          // size.  Is this identical to the sizes in h_infinity or
          // residues?
          const size_t left_so3_size(
            so3_tensor_pseudo_parity(pseudo_parity_types[pp_index][0], two_j,
                                     two_j12)
              .size()),
            right_so3_size(so3_tensor_pseudo_parity(
                             pseudo_parity_types[pp_index][1], two_j, two_j43)
                             .size());
          for(size_t left_so3_index(0); left_so3_index != left_so3_size;
              ++left_so3_index)
            {
              pp_back.emplace_back();
              auto &left_back(pp_back.back());
              left_back.resize(right_so3_size);
              for(size_t right_so3_index(0); right_so3_index != right_so3_size;
                  ++right_so3_index)
                {
                  tasks.emplace_back(two_j, pp_index, left_so3_index,
                                     right_so3_index,
                                     left_back.at(right_so3_index));
                }
            }
        }
    }

  std::mutex task_mtx;
  size_t current_task_index = 0;
  std::vector<std::thread> threads;

  for(size_t i = 0; i < num_threads; ++i)
    {
      threads.emplace_back([&]() {
        while(true)
          {
            std::tuple<int64_t, size_t, size_t, size_t,
                       std::vector<Delta_Fraction> &> *task_ptr
              = nullptr;

            // Synchronized block to fetch the next task
            {
              std::lock_guard<std::mutex> lock(
                task_mtx); // Automatically releases the lock when out of scope
              if(current_task_index < tasks.size())
                {
                  task_ptr = &tasks[current_task_index++];
                }
              else
                {
                  return; // No more tasks to process
                }
            }

            // two_js_intermediate may skip values of two_j, but h_inf and
            // residues both cover the entire range.  So we compute an index
            // for two_j based on its value.
            auto [two_j, pp_index, left_so3_index, right_so3_index, target]
              = *task_ptr;
            const size_t two_j_index(two_j / 2);
            std::vector<Bigfloat> coeffs_h_inf;
            coeffs_h_inf.reserve(coeffs_to_derivs.size());
            for(auto &coeff : coeffs_to_derivs)
              {
                coeffs_h_inf.emplace_back(0);
                auto &h_inf_sum(coeffs_h_inf.back());
                for(size_t ii(0); ii != coeff.size(); ++ii)
                  {
                    h_inf_sum += coeff[ii]
                                 * h_inf.at(pp_index)
                                     .at(ii)
                                     .at(two_j_index)
                                     .at(left_so3_index)
                                     .at(right_so3_index);
                  }
              }
            const std::vector<Pole> &poles(
              pole_types.eval(two_j, two_j12, two_j43, order));

            std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
              coeffs_residues(poles.size());

            for(size_t pole_index(0); pole_index != poles.size(); ++pole_index)
              {
                coeffs_residues[pole_index].first
                  = poles[pole_index].delta(two_j);
                coeffs_residues[pole_index].second.resize(
                  coeffs_to_derivs.size());
                int64_t r_shift = poles[pole_index].shift();
                auto &coeffs_residue(coeffs_residues.at(pole_index).second);
                for(size_t coeff_index = 0;
                    coeff_index != coeffs_to_derivs.size(); ++coeff_index)
                  {
                    auto &residue_sum(coeffs_residue.at(coeff_index));
                    auto &coeff(coeffs_to_derivs.at(coeff_index));

                    residue_sum = 0;
                    for(size_t ii(0); ii + r_shift < coeff.size(); ++ii)
                      {
                        residue_sum += coeff[ii + r_shift]
                                       * residues.at(pp_index)
                                           .at(ii)
                                           .at(two_j_index)
                                           .at(Pole_Types::position(
                                             two_j, two_j12, two_j43,
                                             order - ii, poles[pole_index]))
                                           .at(left_so3_index)
                                           .at(right_so3_index);
                      }
                  }
              }
            target = restore_r_delta(coeffs_h_inf, coeffs_residues, r_delta,
                                     timers);
          }
      });
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
  timers.exit();
  main_derivatives_loop.stop();
  derivatives_timer.stop();
  timers.exit();
  return result;
}
