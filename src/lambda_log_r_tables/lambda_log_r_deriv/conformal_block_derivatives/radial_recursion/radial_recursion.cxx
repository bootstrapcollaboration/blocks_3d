#include "../../../Cache.hxx"

#include <vector>
#include <array>

void zero_init_residues(
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const Pole_Types &pole_types,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues);

void fill_residues(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const M_Matrix &m_matrix, const Pole_Types &pole_types,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues,
  const size_t &num_threads);

// This function performs radial recursion given the vectorized
// hInfinity and other standard parameters.

// pseudoparities = {{pL1, pR1},{pL2,pR2}}

// This tells us which pseudoparities to use. When converted to
// parities (depends on external spins which are not an input),
// this becomes {{0,0},{1,1}} or {{0,1},{1,0}} depending on the parity
// of the 4-pt structure.

// Valid input has

// twojMax,twoj12,twoj43,order >= 0
// twojMax-twoj12 and twojMax-twoj43 are even

// pseudo_parities = {{pL1, pR1},{pL2,pR2}} with all entries being 0
// or 1. Furthermore,
// pseudo_parities[0]+1 % 2 == pseudo_parities[1]

std::vector<
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
radial_recursion(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const M_Matrix &m_matrix, const Pole_Types &pole_types,
  const size_t &num_threads)
{
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    result(2); // 2 parities

  zero_init_residues(two_j_max, two_j12, two_j43, pseudo_parities, order,
                     pole_types, result);

  fill_residues(h_infinity, two_j_max, two_j12, two_j43, pseudo_parities,
                order, m_matrix, pole_types, result, num_threads);
  return result;
}

// Measure the size in bytes of the structure returned by
// radial_recursion.
size_t
residues_size(
  const std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
  &residues)
{
  // prec_to_bytes_num/prec_to_bytes_den is a rational approximation
  // for (log 10 / log 2) / 8
  const size_t prec_to_bytes_num = 267;
  const size_t prec_to_bytes_den = 643;

  size_t total_size = 0;
  total_size += sizeof(residues);
  for (const auto &i1 : residues)
    {
      total_size += sizeof(i1);
      for (const auto &i2 : i1)
        {
          total_size += sizeof(i2);
          for (const auto &i3 : i2)
            {
              total_size += sizeof(i3);
              for (const auto &i4 : i3)
                {
                  total_size += sizeof(i4);
                  for (const auto &i5 : i4)
                    {
                      std::cout << "sizeof(i5) = " << sizeof(i5) << std::endl;
                      total_size += sizeof(i5);
                      for (const auto &i6 : i5)
                        {
                          total_size += i6.precision() * prec_to_bytes_num / prec_to_bytes_den;
                        }
                    }
                }
            }
        }
    }
  return total_size;
}
