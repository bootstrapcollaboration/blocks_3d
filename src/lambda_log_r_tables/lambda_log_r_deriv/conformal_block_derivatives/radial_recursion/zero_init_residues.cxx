#include "../two_j_values.hxx"
#include "../../../Pole_Types.hxx"
#include "../../../../so3_tensor_parity.hxx"

void zero_init_residues(
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const Pole_Types &pole_types,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues)
{
  for(size_t parity_type(0); parity_type != residues.size(); ++parity_type)
    {
      auto &r_orders(residues[parity_type]);
      r_orders.resize(order);
      for(size_t r_order_m(0); r_order_m != r_orders.size();
          ++r_order_m)
        {
          auto &two_js(r_orders[r_order_m]);
          std::vector<int64_t> two_j_values_vec(
            two_j_values(r_order_m, order, two_j12, two_j_max));
          two_js.resize(two_j_values_vec.size());
          for(size_t two_j_index(0); two_j_index != two_j_values_vec.size();
              ++two_j_index)
            {
              auto &poles(two_js[two_j_index]);
              const int64_t two_j(two_j_values_vec[two_j_index]);
              // Note that the lists pole_types() and so3_tensor() are
              // not actually needed here -- only their lengths

              const int64_t j_excess= two_j > two_j_max ? (two_j-two_j_max)/2 : 0;
              poles.resize(
                pole_types.eval(two_j, two_j12, two_j43, order - r_order_m - j_excess).size());

              for(size_t pole(0); pole != poles.size(); ++pole)
                {
                  auto &two_j120s(poles[pole]);
                  two_j120s.resize(
                    so3_tensor_pseudo_parity(pseudo_parities[parity_type][0], two_j,
                                      two_j12)
                      .size());
                  for(size_t two_j120(0); two_j120 != two_j120s.size();
                      ++two_j120)
                    {
                      auto &two_j430s(two_j120s[two_j120]);

                      two_j430s.resize(
                        so3_tensor_pseudo_parity(pseudo_parities[parity_type][1],
                                          two_j, two_j43)
                          .size(),
                        0);
                    }
                }
            }
        }
    }
}
