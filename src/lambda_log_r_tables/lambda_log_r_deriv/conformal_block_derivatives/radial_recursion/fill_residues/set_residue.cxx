#include "../../../../../Bigfloat.hxx"

#include <vector>
#include <functional>
#include <cstdint>

void set_residue(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &r_order_m, const size_t &two_j_index,
  const size_t &pole_index, const std::vector<int64_t> &pole_shifts, const std::vector<int64_t> &pole_indices,
  const size_t &two_j_prime_index,
  const std::vector<std::reference_wrapper<Bigfloat>> &delta_vector,
  const size_t &parity_index, const size_t &source_parity_index,
  const std::vector<std::vector<Bigfloat>> &LM,
  const std::vector<std::vector<Bigfloat>> &RM, const Bigfloat &Q_val,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues)
{
  if((!LM.empty() && !(LM.front().empty()))
     && (!RM.empty() && !(RM.front().empty())))
    {
      std::vector<std::vector<Bigfloat>> h_delta_prime_term(LM[0].size());
      // Use explicit temporaries to avoid memory allocation
      Bigfloat temp_float;

      // compute the required term in h_{Delta',j'}
      for(size_t LM_index_1(0); LM_index_1 != h_delta_prime_term.size();
          ++LM_index_1)
        {
          h_delta_prime_term[LM_index_1].resize(RM[0].size());
          for(size_t RM_index_1(0);
              RM_index_1 != h_delta_prime_term[LM_index_1].size();
              ++RM_index_1)
            {
              // add the h_infinity contribution
              h_delta_prime_term[LM_index_1][RM_index_1]
                = h_infinity.at(source_parity_index)
                    .at(r_order_m)
                    .at(two_j_prime_index)
                    .at(LM_index_1)
                    .at(RM_index_1);

              // add the contribution from poles
              for(size_t i(0); i != delta_vector.size(); ++i)
                {
                  temp_float = residues.at(source_parity_index)
                                 .at(r_order_m - pole_shifts[i])
                                 .at(two_j_prime_index)
                                 .at(pole_indices[i])
                                 .at(LM_index_1)
                                 .at(RM_index_1);
                  temp_float *= delta_vector[i].get();
                  h_delta_prime_term[LM_index_1][RM_index_1] += temp_float;
                }
            }
        }

      // The rest is multiplication by L,M, and Q

      std::vector<std::vector<Bigfloat>> intermediate(LM[0].size());

      for(size_t LM_index_1(0); LM_index_1 != intermediate.size();
          ++LM_index_1)
        {
          intermediate[LM_index_1].resize(RM.size());
          for(size_t RM_index_0(0);
              RM_index_0 != intermediate[LM_index_1].size(); ++RM_index_0)
            {
              for(size_t RM_index_1(0); RM_index_1 != RM[0].size();
                  ++RM_index_1)
                {
                  temp_float = h_delta_prime_term[LM_index_1][RM_index_1];
                  temp_float *= RM.at(RM_index_0).at(RM_index_1);
                  intermediate[LM_index_1][RM_index_0] += temp_float;
                }
            }
        }

      for(size_t LM_index_0(0); LM_index_0 != LM.size(); ++LM_index_0)
        {
          for(size_t RM_index_0(0); RM_index_0 != RM.size(); ++RM_index_0)
            {
              auto &residue(residues.at(parity_index)
                              .at(r_order_m)
                              .at(two_j_index)
                              .at(pole_index)
                              .at(LM_index_0)
                              .at(RM_index_0));
              for(size_t LM_index_1(0); LM_index_1 != LM[0].size();
                  ++LM_index_1)
                {
                  temp_float = LM[LM_index_0][LM_index_1];
                  temp_float *= intermediate[LM_index_1][RM_index_0];
                  residue += temp_float;
                }
              residue *= Q_val;
            }
        }
    }
}
