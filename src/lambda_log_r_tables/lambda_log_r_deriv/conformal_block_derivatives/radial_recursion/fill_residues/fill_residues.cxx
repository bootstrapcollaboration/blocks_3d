#include "Q.hxx"
#include "../../two_j_values.hxx"
#include "../../../../Cache.hxx"
#include "../../../../../timers/Timer.hxx"

#include <functional>
#include <thread>
#include <mutex>

void set_residue(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &r_order_index, const size_t &two_j_index,
  const size_t &pole_index, const std::vector<int64_t> &pole_shifts,
  const std::vector<int64_t> &pole_indices, const size_t &two_j_prime_index,
  const std::vector<std::reference_wrapper<Bigfloat>> &delta_vector,
  const size_t &parity_index, const size_t &source_parity_index,
  const std::vector<std::vector<Bigfloat>> &LM,
  const std::vector<std::vector<Bigfloat>> &RM, const Bigfloat &Q_val,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues);

namespace
{
  inline const std::vector<std::vector<Bigfloat>> &
  L_Matrix(const Pole &pole, const int64_t &two_j12,
           const int64_t &prime_pseudo_parity, const int64_t &two_j,
           const M_Matrix &m_matrix)
  {
    return m_matrix.eval(pole, M_Matrix::Delta::delta_12, two_j12,
                         prime_pseudo_parity, two_j);
  }

  inline std::vector<std::vector<Bigfloat>>
  R_Matrix(const Pole &pole, const int64_t &two_j43,
           const int64_t &prime_pseudo_parity, const int64_t &two_j,
           const M_Matrix &m_matrix)
  {
    std::vector<std::vector<Bigfloat>> result(m_matrix.eval(
      pole, M_Matrix::Delta::delta_43, two_j43, prime_pseudo_parity, two_j));
    if((pole.two_j(two_j) - two_j) % 4 != 0)
      {
        for(auto &vec : result)
          for(auto &element : vec)
            {
              element = -element;
            }
      }
    return result;
  }
}

void fill_residues(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, const M_Matrix &m_matrix,
  const Pole_Types &pole_types,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
  &residues,
  const size_t &num_threads)
{
  // Some complications because we precompute the possible elements of
  // delta_vector and just use the correct reference.
  std::vector<Bigfloat> inverse_positive, inverse_negative;
  Q Q_vals;

  inverse_positive.reserve(7 + 2 * two_j_max + 6 * order);
  inverse_negative.reserve(7 + 2 * two_j_max + 6 * order);
  inverse_positive.emplace_back(0);
  inverse_negative.emplace_back(0);
  for(int64_t diff = 1; diff != 8 + 2 * two_j_max + 6 * order; ++diff)
    {
      inverse_positive.emplace_back(2);
      inverse_positive.back() /= diff;
      inverse_negative.emplace_back(-2);
      inverse_negative.back() /= diff;
    }

  std::cout << "radial recursion: order = " << order << std::endl;
  
  // we are updating c_{i,m} with m = r_order_m; the residue is
  // r^{m+n_i}c_{i,m}; c_{i,0} is generically non-zero
  for(int64_t r_order_m(0); r_order_m != order; ++r_order_m)
    {
      // not all spins need to be updated at a given order
      std::vector<int64_t> two_js(
        two_j_values(r_order_m, order, two_j12, two_j_max));

      // overestimating the size
      std::vector<std::tuple<size_t, size_t, Pole>> pole_pool;
      pole_pool.reserve(two_js.size() * 4 * (order + 1));

      // populate the list of tasks with poles which need updating
      for(size_t two_j_index(0); two_j_index != two_js.size(); ++two_j_index)
        {
          const int64_t two_j(two_js[two_j_index]);

          // compute the poles present at this spin value
          const int64_t j_excess
            = two_j > two_j_max ? (two_j - two_j_max) / 2 : 0;
          const std::vector<Pole> poles(pole_types.eval(
            two_j, two_j12, two_j43, order - r_order_m - j_excess));

          // visit all poles
          for(size_t pole_index(0); pole_index != poles.size(); ++pole_index)
            {
              pole_pool.emplace_back(
                std::tuple(two_j_index, pole_index, poles[pole_index]));
            }
        }

      std::mutex times_mtx;
      std::vector<int64_t> times;
      times.reserve(pole_pool.size());
      // here we can have threads pick up tasks as they free up
      std::cout << "iteration = " << r_order_m
                << " pole_pool.size() = " << pole_pool.size() << std::endl;

      std::mutex task_mtx;
      size_t current_task_index = 0;
      std::vector<std::thread> threads;

      // Pre-populate Q_vals. If we don't do this, we get problems
      // with thread-safety.
      for (auto task : pole_pool)
        {
          const auto &[two_j_index, pole_index, pole] = task;
          const int64_t two_j(two_js[two_j_index]);
          Q_vals.calculate_and_store(pole, two_j);
        }
      
      for (size_t i = 0; i < num_threads; ++i) {
        threads.emplace_back([&]() {
          while (true) {
            std::tuple<size_t, size_t, Pole>* task_ptr = nullptr;
          
            // Synchronized block to fetch the next task
            {
              std::lock_guard<std::mutex> lock(task_mtx); // Automatically releases the lock when out of scope
              if (current_task_index < pole_pool.size()) {
                task_ptr = &pole_pool[current_task_index++];
              } else {
                return; // No more tasks to process
              }
            }

            auto& task = *task_ptr;

            Timer task_timer; // Timings; remove later
            task_timer.start();
            const auto &[two_j_index, pole_index, pole] = task;
            const int64_t two_j(two_js[two_j_index]);
            const int64_t r_shift(pole.shift());
            // only consider the poles which are non-zero at the r-order that
            // we are updating

            // (twice the) spin that appears in the residue
            const int64_t two_j_prime(pole.two_j(two_j));
            std::vector<std::reference_wrapper<Bigfloat>> delta_vector;
            std::vector<int64_t> pole_shifts;
            std::vector<int64_t> pole_indices;

            // twice the dimension appearing in the residue
            const int64_t delta_2_prime(pole.delta_2(two_j) + 2 * r_shift);
            // poles of the block in the residue
            const std::vector<Pole> prime_poles(
              pole_types.eval(two_j_prime, two_j12, two_j43, r_order_m));

            delta_vector.reserve(prime_poles.size());
            pole_shifts.resize(prime_poles.size());
            pole_indices.resize(prime_poles.size());
            // visit all poles in the residue block
            // and fill delta_vector with 1/(Delta'-Delta_i) where i
            // runs over residue block poles
            for(size_t prime_pole_index(0);
                prime_pole_index != prime_poles.size(); ++prime_pole_index)
              {
                pole_shifts[prime_pole_index]
                  = prime_poles[prime_pole_index].shift();

                const int64_t j_prime_excess
                  = two_j_prime > two_j_max ? (two_j_prime - two_j_max) / 2 : 0;
                pole_indices[prime_pole_index] = Pole_Types::position(
                  two_j_prime, two_j12, two_j43,
                  order - r_order_m + pole_shifts[prime_pole_index]
                    - j_prime_excess,
                  prime_poles[prime_pole_index]);

                // 2 * scaling dimension of the residue block pole
                const int64_t prime_pole_delta_2(
                  prime_poles[prime_pole_index].delta_2(two_j_prime));

                // inverse_positive[diff] and inverse_negative[diff]
                // hold +2/diff and -2/diff respectively
                const size_t diff(std::abs(delta_2_prime - prime_pole_delta_2));
                if(delta_2_prime >= prime_pole_delta_2)
                  {
                    delta_vector.emplace_back(inverse_positive[diff]);
                  }
                else
                  {
                    delta_vector.emplace_back(inverse_negative[diff]);
                  }
              }

            const size_t two_j_prime_index(two_j_prime / 2);
            for(size_t parity_index(0); parity_index != 2; ++parity_index)
              {
                const size_t source_parity_index((parity_index + pole.parity())
                                                 % 2);

                const std::vector<std::vector<Bigfloat>> LM(L_Matrix(
                  pole, two_j12, pseudo_parities[source_parity_index][0], two_j,
                  m_matrix)),
                  RM(R_Matrix(pole, two_j43,
                              pseudo_parities[source_parity_index][1], two_j,
                              m_matrix));
                const Bigfloat &Q_val(Q_vals.eval(pole, two_j));

                set_residue(h_infinity, r_order_m, two_j_index, pole_index,
                            pole_shifts, pole_indices, two_j_prime_index,
                            delta_vector, parity_index, source_parity_index, LM,
                            RM, Q_val, residues);
              }
            task_timer.stop();

            // Synchronized block to modify times
            {
              std::lock_guard<std::mutex> lock(times_mtx); // Automatically releases the lock when out of scope
              times.push_back(task_timer.elapsed_nanoseconds());
            }
          }
        });
      }

      for (auto& thread : threads) {
        thread.join();
      }
      
      // Timings; remove later
      int64_t total_ns(0), min(-1);
      for(int64_t time : times)
        {
          total_ns += time;
          if(min < 0 || min > time)
            min = time;
        }
      double mean = double(total_ns) / times.size();
      double std_dev(0);
      for(int64_t time : times)
        {
          std_dev += (time - mean) * (time - mean);
        }
      std_dev = sqrt(std_dev / times.size());

      std::cout << "-- mean = " << mean / 1000
                << " us | std = " << std_dev / 1000
                << " us | min = " << double(min) / 1000 << " us " << std::endl;
    }
}
