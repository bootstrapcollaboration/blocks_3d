#pragma once

#include <vector>
#include <cstdint>

std::vector<int64_t>
two_j_values(const int64_t &r_order, const int64_t &order,
             const int64_t &two_j12, const int64_t &two_j_max);
