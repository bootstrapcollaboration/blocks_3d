#include "../../../Bigfloat.hxx"

#include <array>
#include <vector>


// Precomputes binomials used by `h_infinity_double_series`
void fill_binomials(const Bigfloat &delta_12, const Bigfloat &delta_43,
                    const std::array<int64_t, 4> &q4, const int64_t &order,
                    std::vector<Bigfloat> &binomial_a,
                    std::vector<Bigfloat> &binomial_ab,
                    std::vector<Bigfloat> &binomial_b,
                    std::vector<Bigfloat> &binomial_bb)
{
  const Bigfloat alpha((-2 * delta_12 - q4[1] + q4[0]) / 4),
    alphab((-2 * delta_12 + q4[1] - q4[0]) / 4),
    beta((-2 * delta_43 - q4[2] + q4[3]) / 4),
    betab((-2 * delta_43 + q4[2] - q4[3]) / 4);

  const Bigfloat a((2 * (alpha + beta) - 1 - q4[0] - q4[1]) / 2),
    b(-alpha - beta - 0.5), ab((2 * (alphab + betab) - 1 + q4[0] + q4[1]) / 2),
    bb(-alphab - betab - 0.5), c((q4[0] + q4[1]) / 4.0);

  binomial_a.reserve(order + 1);
  binomial_ab.reserve(order + 1);
  binomial_b.reserve(order + 1);
  binomial_bb.reserve(order + 1);
  for(int64_t n = 0; n <= order; ++n)
    {
      binomial_a.emplace_back(binomial_coefficient(a, n));
      binomial_ab.emplace_back(binomial_coefficient(ab, n));
      binomial_b.emplace_back(binomial_coefficient(b, n));
      binomial_bb.emplace_back(binomial_coefficient(bb, n));
    }
}
