#pragma once

#include "../Bigfloat.hxx"

#include <cstdint>
#include <iostream>

struct Pole
{
  enum class Type
  {
    I,
    II,
    III,
    IV
  };

  Type type;
  int64_t value;

  Pole(const Type &t, const int64_t &v) : type(t), value(v) {}

  int64_t shift() const
  {
    switch(type)
      {
      case Type::I:
      case Type::II: return value;
      case Type::III: return 2 * value;
      case Type::IV: return 2 * value - 1;
      }
    throw std::runtime_error("Missing case in Pole::shift(): "
                             + std::to_string(static_cast<int>(type)));
  }

  int64_t two_j(const int64_t &two_j_input) const
  {
    switch(type)
      {
      case Type::I: return two_j_input + 2 * value;
      case Type::II: return two_j_input - 2 * value;
      case Type::III:
      case Type::IV: return two_j_input;
      }
    throw std::runtime_error("Missing case in Pole::two_j(): "
                             + std::to_string(static_cast<int>(type)));
  }

  Bigfloat delta(const int64_t &two_j) const
  {
    switch(type)
      {
      case Type::I: return Bigfloat(1 - 0.5 * two_j - value);
      case Type::II: return Bigfloat(2 + 0.5 * two_j - value);
      case Type::III: return Bigfloat(1.5 - value);
      case Type::IV: return Bigfloat(2 - value);
      }
    throw std::runtime_error("Missing case in Pole::delta(): "
                             + std::to_string(static_cast<int>(type)));
  }

  int64_t delta_2(const int64_t &two_j) const
  {
    switch(type)
      {
      case Type::I: return (2 - two_j - 2 * value);
      case Type::II: return (4 + two_j - 2 * value);
      case Type::III: return (3 - 2 * value);
      case Type::IV: return (4 - 2 * value);
      }
    throw std::runtime_error("Missing case in Pole::delta(): "
                             + std::to_string(static_cast<int>(type)));
  }

  int64_t parity() const
  {
    switch(type)
      {
      case Type::I:
      case Type::II:
      case Type::III: return 0;
      case Type::IV: return 1;
      }
    throw std::runtime_error("Missing case in Pole::parity(): "
                             + std::to_string(static_cast<int>(type)));
  }

  bool operator<(const Pole &p) const
  {
    return type < p.type ? true : (type > p.type ? false : value < p.value);
  }
};

inline std::ostream &operator<<(std::ostream &os, const Pole &pole)
{
  switch(pole.type)
    {
    case Pole::Type::I:
      {
        os << "I";
        break;
      }
    case Pole::Type::II:
      {
        os << "II";
        break;
      }
    case Pole::Type::III:
      {
        os << "III";
        break;
      }
    case Pole::Type::IV:
      {
        os << "IV";
        break;
      }
    }
  os << ":" << pole.value;
  return os;
}
