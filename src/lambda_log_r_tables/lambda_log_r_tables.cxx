#include "Cache.hxx"
#include "../Log_r_Derivatives.hxx"
#include "../Q4pm.hxx"
#include "../compute_pseudo_parity_types.hxx"

#include "../timers/Timers.hxx"

#include <boost/filesystem.hpp>

#include <set>
#include <thread>

std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>
lambda_log_r_deriv(const std::array<int64_t, 4> &two_js,
                   const Bigfloat &delta_12, const Bigfloat &delta_43,
                   const int64_t &two_j12, const int64_t &two_j43,
                   const Q4pm &q4pm,
                   const std::set<int64_t> &two_js_intermediate,
                   const int64_t &r_derivs, const int64_t &n,
                   const int64_t &kept_pole_order, const int64_t &order,
                   const M_Matrix &m_matrix,
                   const Pole_Types &pole_types,
                   const F_SO3_Basis_Derivative &f_so3_basis_derivative,
                   const Pole_Shifting &pole_shifting,
                   const size_t &num_threads,
                   Timers& timers);

// Measure the size in bytes of the structure returned by
// lambda_log_r_tables.
size_t
lambda_log_r_size(
  const std::vector<std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
  &lambda_log_r)
{
  size_t total_size = 0;
  for (const auto& i1 : lambda_log_r)
    {
      for (const auto& i2 : i1)
        {
          for (const auto& i3 : i2)
            {
              for (const auto& i4 : i3)
                {
                  for (const auto& i5 : i4)
                    {
                      total_size += i5.size();
                    }
                }
            }
        }
    }
  return total_size;
}

// return index structure is
// [lambda_order, spin_index, parity_index, left_3pt_index, right_3pt_index]
// each element is Log_r_Derivatives -- a list of d/d(log r) derivatives,
// approximated by polynomials; poles are returned separately
std::vector<
  std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
lambda_log_r_tables(const std::array<int64_t, 4> &two_js,
                    const Bigfloat &delta_12, const Bigfloat &delta_43,
                    const int64_t &two_j12, const int64_t &two_j43,
                    const Q4pm &q4pm,
                    const std::set<int64_t> &two_js_intermediate,
                    const int64_t &lambda, const bool &is_radial,
                    const int64_t &kept_pole_order, const int64_t &order,
                    const size_t &num_threads, const bool &debug,
                    const Pole_Types &pole_types, Timers &timers)
{
  timers.enter("lambda_log_r_tables");

  // vector of the lambda derivative orders to compute
  std::vector<int64_t> lambda_orders;
  if(!is_radial)
    {
      for(int64_t lambda_order = (1 - q4pm.i) / 2; lambda_order <= lambda;
          lambda_order += 2)
        {
          lambda_orders.push_back(lambda_order);
        }
    }
  else
    {
      lambda_orders.push_back((1 - q4pm.i) / 2);
    }
  std::vector<
    std::vector<std::vector<std::vector<std::vector<Log_r_Derivatives>>>>>
    result;

  result.reserve(lambda_orders.size());

  Timer &cache_init = timers.add_and_start("cache_init");
  timers.enter("cache_init");

  // Define caches and initialize them
  Timer &construtors = timers.add_and_start("constructors");
  Clebsch_Gordan clebsch_gordan_cache;
  Factorial factorial;
  M_Matrix m_matrix(delta_12, delta_43);
  Wigner_Cache wigner_cache;
  F_Q_Basis_Derivative f_q_basis_derivative;
  F_W_Conversion_Matrix f_w_conversion_matrix;
  F_SO3_Basis_Derivative f_SO3_basis_derivative;
  Pole_Shifting pole_shifting;
  construtors.stop();

  int64_t two_j_max = *two_js_intermediate.rbegin();

  Timer &factorial_timer = timers.add_and_start("factorial");
  factorial.initialize(two_j_max + 2 * order + 2);
  factorial_timer.stop();
  std::cout << "Done with factorial" << std::endl;

  Timer &wigner_timer = timers.add_and_start("wigner_cache");
  wigner_cache.initialize(two_j_max + 2 * order, two_js, q4pm.vec,
                          lambda, // <----
                          factorial);
  wigner_timer.stop();
  std::cout << "Done with wigner_cache" << std::endl;

  Timer &cg_timer = timers.add_and_start("clebsch_gordan_cache");
  clebsch_gordan_cache.initialize(two_j12, two_j_max + 2 * order, factorial);
  clebsch_gordan_cache.initialize(two_j43, two_j_max + 2 * order, factorial);
  cg_timer.stop();
  std::cout << "Done with clebsch_gordan_cache" << std::endl;

  Timer &m_matrix_timer = timers.add_and_start("m_matrix");
  m_matrix.initialize(two_j12, two_j43, two_j_max,
                      four_point_parity(two_js, q4pm.vec), order, pole_types,
                      clebsch_gordan_cache, factorial);
  m_matrix_timer.stop();
  std::cout << "Done with m_matrix" << std::endl;
  // TODO: This does nontrivial computation -- only do at high
  // verbosity
  // if (debug)
  //   {
  //     std::cout << "m_matrix.size() = " << m_matrix.size() << std::endl;
  //   }

  Timer &f_q_timer = timers.add_and_start("f_q_basis_derivative");
  f_q_basis_derivative.initialize(two_js, two_j_max + 2 * order,
                                  lambda, // <---
                                  wigner_cache, timers);
  f_q_timer.stop();
  std::cout << "Done with f_q_basis_derivative" << std::endl;
  // TODO: This does nontrivial computation -- only do at high
  // verbosity
  // if (debug)
  //   {
  //     std::cout << "f_q_basis_derivative.size() = " << f_q_basis_derivative.size() << std::endl;
  //   }

  Timer &f_so3_timer = timers.add_and_start("f_SO3_basis_derivative");
  f_SO3_basis_derivative.initialize(
    two_js, two_j_max + 2 * order, two_j12, two_j43, q4pm.vec, lambda, // <---
    clebsch_gordan_cache, factorial, f_q_basis_derivative,
    f_w_conversion_matrix);
  f_so3_timer.stop();
  std::cout << "Done with f_SO3_basis_derivative" << std::endl;

  Timer &pole_shifting_timer = timers.add_and_start("pole_shifting");
  const bool should_do_pole_shifting = kept_pole_order < order;
  pole_shifting.initialize(two_js_intermediate, two_j12, two_j43, order, kept_pole_order, pole_types, should_do_pole_shifting);
  pole_shifting_timer.stop();

  timers.exit();
  cache_init.stop();

  std::list<Timers> lambda_timers;
  for(size_t i = 0; i < lambda_orders.size(); ++i)
    lambda_timers.emplace_back(false, timers.prefixes);

  auto lambda_timer_it = lambda_timers.begin();
  for(auto lambda_order : lambda_orders)
    {
      if(debug)
        {
          std::stringstream ss;
          ss << "lambda_order = " << lambda_order << "\n";
          std::cout << ss.str() << std::flush;
        }

      result.emplace_back(lambda_log_r_deriv(
        two_js, delta_12, delta_43, two_j12, two_j43, q4pm,
        two_js_intermediate, lambda - lambda_order, lambda_order,
        kept_pole_order, order, m_matrix, pole_types,
        f_SO3_basis_derivative, pole_shifting,
        num_threads, *lambda_timer_it++));
    }

  for(auto &timers_to_add : lambda_timers)
    {
      timers.splice(timers.begin(), timers_to_add);
    }

  timers.exit();
  return result;
}
