#include "Pole_Types.hxx"
#include "bosonic.hxx"

#include <vector>
#include <array>
#include <algorithm>

namespace
{
  int64_t pole_range_k(const Pole::Type &type, const int64_t &two_j,
                       const int64_t &two_j12, const int64_t &two_j43,
                       const int64_t &order)
  {
    switch(type)
      {
      case Pole::Type::I: return order;
      case Pole::Type::II: return std::min(two_j / 2, order);
      case Pole::Type::III:
        if(bosonic(two_j))
          {
            return order / 2;
          }
        else
          {
            return std::min(
              two_j / 2,
              std::min(two_j12 / 2, std::min(two_j43 / 2, order / 2)));
          }
      case Pole::Type::IV:
        if(bosonic(two_j))
          {
            return std::min(
              two_j / 2,
              std::min(two_j12 / 2, std::min(two_j43 / 2, (order + 1) / 2)));
          }
        else
          {
            return (order + 1) / 2;
          }
      }
    throw std::runtime_error(
      "INTERNAL_ERROR: Reached the end of pole_range_k");
  }
}

// The types of poles that a block with 2*j=two_j has
// TODO: double check the upper bounds on these families
const std::vector<Pole> &
Pole_Types::calculate(const int64_t &two_j, const int64_t &two_j12,
                      const int64_t &two_j43, const int64_t &order)
{
  std::tuple<int64_t, int64_t, int64_t, int64_t> key(two_j, two_j12, two_j43,
                                                     order);
  auto iterator((cache.emplace(key, std::vector<Pole>())).first);
  std::vector<Pole> &result(iterator->second);
  for(auto &type : std::array<Pole::Type, 4>(
        {Pole::Type::I, Pole::Type::II, Pole::Type::III, Pole::Type::IV}))
    {
      for(int64_t k(1);
          k <= pole_range_k(type, two_j, two_j12, two_j43, order); ++k)
        {
          result.emplace_back(type, k);
        }
    }

  // // Added sorting for ease of addressing; using stable sort to make sure
  // the families are ordered std::stable_sort(result.begin(), result.end(),
  // [](const Pole &a, const Pole &b) {
  //   return a.shift() < b.shift();
  // });
  return iterator->second;
}

size_t Pole_Types::position(const int64_t &two_j, const int64_t &two_j12,
                            const int64_t &two_j43, const int64_t &order,
                            const Pole &pole)
{
  switch(pole.type)
    {
    case Pole::Type::I: return pole.value - 1;
    case Pole::Type::II:
      return pole.value - 1
             + pole_range_k(Pole::Type::I, two_j, two_j12, two_j43, order);
    case Pole::Type::III:
      return pole.value - 1
             + pole_range_k(Pole::Type::I, two_j, two_j12, two_j43, order)
             + pole_range_k(Pole::Type::II, two_j, two_j12, two_j43, order);
    case Pole::Type::IV:
      return pole.value - 1
             + pole_range_k(Pole::Type::I, two_j, two_j12, two_j43, order)
             + pole_range_k(Pole::Type::II, two_j, two_j12, two_j43, order)
             + pole_range_k(Pole::Type::III, two_j, two_j12, two_j43, order);
    }
  throw std::runtime_error(
    "INTERNAL_ERROR: Reached the end of Pole_Types::position");
}

void Pole_Types::initialize(const int64_t &two_j_max, const int64_t &two_j12,
                            const int64_t &two_j43, const int64_t &order)
{
  for(int64_t two_j = two_j_max % 2; two_j <= two_j_max + 2*order; two_j += 2)
  {
    int64_t j_excess = two_j > two_j_max ? (two_j-two_j_max)/2 : 0;
    for(int64_t order_prime = 0; order_prime <= order-j_excess; ++order_prime)
    {
      calculate(two_j, two_j12, two_j43, order_prime);
    }
  }
}
