// tildeWignerCoefficient[2j,2m,2mp,n] computes the n-th derivative of \tilde
// d^j_{m,mp}(-\theta) in \lambda=i \theta at \lambda=0.

// \tilde d^j_{m,mp}(-\theta) is defined as

// I^(mp-m)WignerD[{j,m,mp},-theta]

#include "../../Bigfloat.hxx"
#include "../Factorial.hxx"
#include "../Wigner_Cache.hxx"

Bigfloat
wjm_matrix(const int64_t &two_j, const int64_t &two_m1, const int64_t &two_m2);

Bigfloat coefficient(const int64_t &two_j, const int64_t &two_m_out,
                     const int64_t &two_mp_out, const int64_t &Nn,
                     const Factorial &factorial);

// Valid input always has
// two_j, Nn >= 0
// two_j-two_mout, two_j-two_mpout are even
// abs(two_mout), abs(two_mpout) <= two_j

Bigfloat
tilde_wigner_derivative(const int64_t &two_j, const int64_t &two_m,
                        const int64_t &two_mp, const int64_t &Nn,
                        const Factorial &factorial, Wigner_Cache &wigner_cache)
{
  if((Nn < std::abs(two_m - two_mp) / 2)
     || (Nn - std::abs(two_m - two_mp) / 2) % 2 == 1)
    {
      return 0;
    }

  Bigfloat result(0);

  for(int64_t n = 0; n <= (Nn - std::abs(two_m - two_mp) / 2) / 2; ++n)
    {
      result += wigner_cache.eval_sinh_power_derivative(
                  2 * n + std::abs(two_m - two_mp) / 2,
                  (Nn - 2 * n - std::abs(two_m - two_mp) / 2) / 2)
                * coefficient(two_j, two_m, two_mp, n, factorial);
    }
  return result;
}

void Wigner_Cache::initialize(const int64_t &two_j_max,
                              const std::array<int64_t, 4> &two_js,
                              const std::array<int64_t, 4> &q4,
                              const int64_t &Nn_max,
                              const Factorial &factorial)
{
  constexpr int64_t two_point_b(1);

  // Initialize the Wigner derivative cache
  int64_t outer_max_two_m
    = std::max(two_js[0] + two_js[1], two_js[2] + two_js[3]);
  tilde_wigner_derivative_modified_cache.resize(two_j_max / 2 + 1);
  two_m_shifts.resize(two_j_max / 2 + 1);
  for(size_t two_j_index = 0;
      two_j_index != tilde_wigner_derivative_modified_cache.size();
      ++two_j_index)
    {
      const int64_t two_j = two_j_index * 2 + two_j_max % 2;
      const int64_t max_two_m = std::min(outer_max_two_m, two_j);
      auto &spin_table = tilde_wigner_derivative_modified_cache[two_j_index];

      two_m_shifts[two_j_index] = max_two_m;

      spin_table.resize(max_two_m + 1);
      for(int64_t two_m = -max_two_m; two_m <= max_two_m; two_m += 2)
        {
          const int64_t m_index = (two_m + max_two_m) / 2;
          spin_table[m_index].resize(max_two_m + 1);
          for(int64_t two_mp = -max_two_m; two_mp <= max_two_m; two_mp += 2)
            {
              const int64_t mp_index = (two_mp + max_two_m) / 2;
              auto &deriv_array = spin_table[m_index][mp_index];
              deriv_array.resize(Nn_max + 1);
              for(int64_t Nn = 0; Nn <= Nn_max; ++Nn)
                deriv_array[Nn]
                  = tilde_wigner_derivative(two_j, two_m, two_mp, Nn,
                                            factorial, *this)
                    / (two_point_b
                       * sqrt(
                         binomial_coefficient(two_j, (two_j + two_m) / 2)
                         * binomial_coefficient(two_j, (two_j + two_mp) / 2)));
            }
        }
    }

  // Initialize the Wigner matrix product cache
  q_shifts = two_js;

  wigner_product_cache.resize(two_js[0] + 1);
  for(int64_t two_p1 = -two_js[0]; two_p1 <= two_js[0]; two_p1 += 2)
    {
      auto &p1 = wigner_product_cache[(two_js[0] + two_p1) / 2];
      p1.resize(two_js[1] + 1);
      for(int64_t two_p2 = -two_js[1]; two_p2 <= two_js[1]; two_p2 += 2)
        {
          auto &p2 = p1[(two_js[1] + two_p2) / 2];
          p2.resize(two_js[2] + 1);
          for(int64_t two_p3 = -two_js[2]; two_p3 <= two_js[2]; two_p3 += 2)
            {
              auto &p3 = p2[(two_js[2] + two_p3) / 2];
              p3.resize(two_js[3] + 1);
              for(int64_t two_p4 = -two_js[3]; two_p4 <= two_js[3];
                  two_p4 += 2)
                {
                  p3[(two_js[3] + two_p4) / 2]
                    = {wjm_matrix(two_js[0], two_p1, q4[0])
                         * wjm_matrix(two_js[1], two_p2, q4[1])
                         * wjm_matrix(two_js[2], -two_p3, q4[2])
                         * wjm_matrix(two_js[3], -two_p4, q4[3]),
                       wjm_matrix(two_js[0], two_p1, -q4[0])
                         * wjm_matrix(two_js[1], two_p2, -q4[1])
                         * wjm_matrix(two_js[2], -two_p3, -q4[2])
                         * wjm_matrix(two_js[3], -two_p4, -q4[3])};
                }
            }
        }
    }
}
