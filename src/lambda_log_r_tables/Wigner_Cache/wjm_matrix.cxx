#include "../../Bigfloat.hxx"

// Valid input always has
// two_j>=0
// two_j-two_m1 is even
// two_j-two_m2 is even
// abs(two_m1), abs(two_m2) <= two_j

Bigfloat
wjm_matrix(const int64_t &two_j, const int64_t &two_m1, const int64_t &two_m2)
{
  const int64_t two_q(two_m1), p((two_m2 + two_j) / 2);
  Bigfloat result(0);
  for(int64_t l = 0; l <= p; ++l)
    {
      result += (l % 2 == 0 ? 1 : -1)
                * binomial_coefficient((two_j + two_q) / 2, p - l)
                * binomial_coefficient((two_j - two_q) / 2, l);
    }
  // Simplification of Sqrt[Binomial[twoj, (twoj + twoq)/2]/Binomial[twoj, p]] ?
  // We do this in two steps, because pow() with non-integral powers
  // is inaccurate past 80 digits
  result *= sqrt(pow(Bigfloat(2), -two_j));
  return result;
}
