#include "../../Bigfloat.hxx"
#include "../Factorial.hxx"

// tildeWignerCoefficient[j,m,mp,n] computes the coefficient
// of y^{2n+Abs[m-mp]} in the series expansion of \tilde d^j_{m,mp}(-\theta)
// in y = i sin (\theta/2).

// \tilde d^j_{m,mp}(-\theta) is defined as

// I^(mp-m)WignerD[{j,m,mp},-theta]

// Valid input always has
// twoj,Nn>=0
// twoj-twomout is even
// twoj-twompout is even
// Abs[twomout]<=twoj
// Abs[twompout]<=twoj

Bigfloat coefficient(const int64_t &two_j, const int64_t &two_m_out,
                     const int64_t &two_mp_out, const int64_t &Nn,
                     const Factorial &factorial)
{
  const int64_t sign(two_m_out + two_mp_out >= 0 ? 1 : -1);
  const int64_t two_m(sign * two_m_out), two_mp(sign * two_mp_out);
  const int64_t two_m0(std::max(two_m, two_mp)),
    k_max((two_m + two_mp) % 4 == 0 ? (two_m + two_mp) / 4 : Nn);

  Bigfloat result(0), temp_0(Bigfloat(two_m + two_mp) / 4), temp_1, temp_2;
  for(int64_t n = std::max(int64_t(0), Nn - k_max); n <= (two_j - two_m0) / 2;
      ++n)
    {
      // Do it the hard way to avoid temporaries
      temp_1=factorial.eval((two_j + two_m0 + 2 * n) / 2);
      temp_1*=binomial_coefficient(temp_0, Nn - n);
      temp_2=factorial.eval((two_j - two_m0 - 2 * n) / 2);
      temp_2*=factorial.eval(std::abs(two_mp - two_m) / 2 + n);
      temp_2*=factorial.eval(n);
      temp_1/=temp_2;
      result+=temp_1;
    }

  temp_0=factorial.eval((two_j - two_mp) / 2);
  temp_0*=factorial.eval((two_j - two_m) / 2);
  temp_0*=factorial.inverse_eval((two_j + two_mp) / 2);
  temp_0*=factorial.inverse_eval((two_j + two_m) / 2);
  temp_1=sqrt(temp_0);
  result*=temp_1;
  
  return result;
}
