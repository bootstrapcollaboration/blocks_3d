#pragma once

#include "../Bigfloat.hxx"
#include "Factorial.hxx"

#include <map>
#include <tuple>

struct Wigner_Cache
{
  std::map<std::tuple<int64_t, int64_t>, Bigfloat> sinh_power_derivative_cache;

  // index structure [spin_index, m_index, mp_index, deriv_index]
  std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>
    tilde_wigner_derivative_modified_cache;
  std::vector<int64_t> two_m_shifts;

  // index structure [p_1,p_2,p_3,p_4]
  std::vector<std::vector<std::vector<std::vector<std::array<Bigfloat, 2>>>>>
    wigner_product_cache;
  std::array<int64_t, 4> q_shifts;

  void
  initialize(const int64_t &two_j_max, const std::array<int64_t, 4> &two_js,
             const std::array<int64_t, 4> &q4, const int64_t &Nn_max,
             const Factorial &factorial);

  // This includes the binomial denominator
  const Bigfloat &
  eval_tilde_wigner_derivative_modified(const int64_t &two_j,
                                        const int64_t &two_m,
                                        const int64_t &two_mp,
                                        const int64_t &Nn) const
  {
    const int64_t &shift = two_m_shifts[two_j / 2];
    return tilde_wigner_derivative_modified_cache[two_j / 2]
                                                 [(two_m + shift) / 2]
                                                 [(two_mp + shift) / 2][Nn];
  }

  const Bigfloat &
  eval_wigner_product(const int64_t &two_p1, const int64_t &two_p2,
                      const int64_t &two_p3, const int64_t &two_p4,
                      const int64_t &i) const
  {
    return wigner_product_cache[(two_p1 + q_shifts[0]) / 2]
                               [(two_p2 + q_shifts[1]) / 2]
                               [(two_p3 + q_shifts[2]) / 2]
                               [(two_p4 + q_shifts[3]) / 2][i];
  }

  const Bigfloat &
  eval_sinh_power_derivative(const int64_t &n, const int64_t &k)
  {
    std::tuple<int64_t, int64_t> key(n, k);
    auto element(sinh_power_derivative_cache.find(key));
    if(element != sinh_power_derivative_cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate_sinh_power_derivative(n, k);
      }
  }
  const Bigfloat &
  calculate_sinh_power_derivative(const int64_t &n, const int64_t &k)
  {
    std::tuple<int64_t, int64_t> key(n, k);
    auto iterator(
      (sinh_power_derivative_cache.emplace(key, Bigfloat(0))).first);
    Bigfloat &result(iterator->second);

    if(n == 0)
      {
        if(k == 0)
          {
            result = 1;
          }
      }
    else
      {
        for(int64_t m = 0; m <= k; ++m)
          {
            // TODO: Is the iterator invalidated if something else
            // gets inserted?
            result += pow(Bigfloat(0.5), 2 * m + 1)
                      * eval_sinh_power_derivative(n - 1, k - m)
                      * binomial_coefficient(2 * k + n, 2 * m + 1);
          }
      }
    return iterator->second;
  }
};
