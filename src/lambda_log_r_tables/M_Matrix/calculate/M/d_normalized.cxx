#include "../../../bosonic.hxx"
#include "../../../Pole.hxx"
#include "../../../Factorial.hxx"

Bigfloat d_normalized(const Pole &pole, const int64_t &two_j,
                      const int64_t &two_m,  const Factorial &factorial)
{
  switch(pole.type)
    {
    case Pole::Type::I:
      {
        if(1 <= pole.value)
          {
            return sqrt((Pochhammer(1 + (two_j - two_m) * 0.5, pole.value)
                         * Pochhammer(1 + (two_j + two_m) * 0.5, pole.value))
                        / (Pochhammer(1 + two_j, 2 * pole.value)
                           * factorial.eval(pole.value)
                           * factorial.eval(pole.value - 1)));
          }
        break;
      }
    case Pole::Type::II:
      {
        if((std::abs(two_m) <= two_j - 2 * pole.value)
           && (1 <= pole.value && pole.value <= two_j * 0.5))
          {
            return sqrt(
              (Pochhammer(1 + (two_j - two_m) * 0.5 - pole.value, pole.value)
               * Pochhammer(1 + (two_j + two_m) * 0.5 - pole.value,
                            pole.value))
              / (Pochhammer(2 + two_j - 2 * pole.value, 2 * pole.value)
                 * factorial.eval(pole.value)
                 * factorial.eval(pole.value - 1)));
          }
        break;
      }
    case Pole::Type::III:
      {
        if((bosonic(two_j) && (1 <= pole.value))
           || (fermionic(two_j) && (std::abs(two_m) >= 2 * pole.value + 1)
               && (2 <= 2 * pole.value && 2 * pole.value <= two_j)))
          {
            return Pochhammer(-pole.value + two_m * 0.5 + 0.5, 2 * pole.value)
                   / (factorial.eval(2 * pole.value)
                      * Pochhammer(-pole.value + two_j * 0.5 + 0.5,
                                   2 * pole.value));
          }
        break;
      }
    case Pole::Type::IV:
      {
        if((bosonic(two_j) && (std::abs(two_m) >= 2 * pole.value)
            && (2 <= 2 * pole.value && 2 * pole.value <= two_j))
           || (fermionic(two_j) && (1 <= pole.value)))
          {
            return Pochhammer(1 - pole.value + two_m * 0.5, 2 * pole.value - 1)
                   / (factorial.eval(2 * pole.value - 1)
                      * Pochhammer(1 - pole.value + two_j * 0.5,
                                   2 * pole.value - 1));
          }
        break;
      }
    }
  return 0;
}
