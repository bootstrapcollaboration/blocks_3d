#include "../../../bosonic.hxx"
#include "../../../Pole.hxx"
#include "../../../Clebsch_Gordan.hxx"

namespace
{
  // Verifies that SO3struct given by {two_jNN,two_jNN0} is allowed for
  // {*,*,two_j} (assuming two_jNN is allowed for the first two operators)
  bool structure_allowed(const int64_t &two_j, const int64_t &two_jNN,
                         const int64_t &two_jNN0)
  {
    return (two_jNN0 - two_jNN - two_j) % 2 == 0
           && std::abs(two_jNN - two_j) <= two_jNN0
           && two_jNN0 <= two_jNN + two_j;
  }

  int64_t two_m_min(const Pole &pole, const int64_t &two_j)
  {
    switch(pole.type)
      {
      case Pole::Type::I:
      case Pole::Type::II: return bosonic(two_j) ? 0 : 1;
      case Pole::Type::III: return bosonic(two_j) ? 0 : 2 * pole.value + 1;
      case Pole::Type::IV: return bosonic(two_j) ? 2 * pole.value : 1;
      }
    throw std::runtime_error("Missing case in two_m_min(): "
                             + std::to_string(static_cast<int>(pole.type)));
  }

  Bigfloat f_ratio(const Bigfloat &delta, const int64_t &shift,
                   const int64_t &two_j, const int64_t &two_jp)
  {
    // The second argument of Pochhammer is always an integer and
    // non-negative.

    // Parity of a structure is determined by parity of j_120-j. For parity
    // even operator j shifts by the same parity as shift varible above, and so
    // does j_120. For parity-odd similar argument works.
    return pow(Bigfloat(2), shift)
           * Pochhammer(0.5 * (-1 - two_j * 0.5 + delta),
                        (shift - (two_jp - two_j) / 2) / 2)
           * Pochhammer(0.5 * (two_j * 0.5 + delta),
                        (shift + (two_jp - two_j) / 2) / 2);
  }
}

Bigfloat d_normalized(const Pole &pole, const int64_t &two_j,
                      const int64_t &two_m, const Factorial &factorial);

Bigfloat
M(const Pole &pole, const Bigfloat &delta_NN, const int64_t &two_jNN,
  const int64_t &two_jNN0, const int64_t &two_jNN0p, const int64_t &two_j,
  const Clebsch_Gordan &clebsch_gordan, const Factorial &factorial)
{
  const int64_t two_jp(pole.two_j(two_j)), shift(pole.shift());
  const Bigfloat delta_pole(pole.delta(two_j));

  Bigfloat result(0);
  if(structure_allowed(two_j, two_jNN, two_jNN0)
     && structure_allowed(two_jp, two_jNN, two_jNN0p)
     && ((two_jNN0 - two_j - (two_jNN0p - two_jp)) + 2 * pole.parity()) % 4
          == 0 // checks parity match
     && !((shift - (two_jNN0p - two_jNN0) / 2) / 2 < 0
          || (shift + (two_jNN0p - two_jNN0) / 2) / 2 < 0))
    // The result must be polynomial in delta_NN.  This last part
    // removes terms that are identically zero due to cancellation.
    // It also ensures that the arguments to Pochhammer are non-negative.
    {
      Bigfloat ratio(
        f_ratio(delta_NN + delta_pole, shift, two_jNN0, two_jNN0p));
      for(int64_t two_m(two_m_min(pole, two_j));
          two_m <= std::min(two_jNN, std::min(two_jp, two_j)); two_m += 2)
        {
          result += (two_m == 0 ? 1 : 2) * ratio
                    * clebsch_gordan.eval(two_jNN, two_jNN0p, two_jp, two_m)
                    * d_normalized(pole, two_j, two_m, factorial)
                    * clebsch_gordan.eval(two_jNN, two_jNN0, two_j, two_m);
        }
    }
  return result;
}
