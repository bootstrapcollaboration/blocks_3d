#pragma once

#include "Pole.hxx"
#include "Clebsch_Gordan.hxx"
#include "Pole_Types.hxx"

#include <map>
#include <tuple>

struct M_Matrix
{
  // We use an enum here because comparing two Bigfloat's is slow
  enum class Delta : int8_t
  {
    delta_12,
    delta_43
  };
  Bigfloat delta_12, delta_43;

  std::map<std::tuple<Pole, Delta, int64_t, int64_t, int64_t>,
           std::vector<std::vector<Bigfloat>>>
    cache;

  M_Matrix(const Bigfloat &Delta_12, const Bigfloat &Delta_43)
      : delta_12(Delta_12), delta_43(Delta_43)
  {}
  const std::vector<std::vector<Bigfloat>> &
  eval(const Pole &pole, const Delta &delta_NN, const int64_t &two_jNN,
       const int64_t &prime_pseudo_parity, const int64_t &two_j) const
  {
    std::tuple<Pole, Delta, int64_t, int64_t, int64_t> key(
      pole, delta_NN, two_jNN, prime_pseudo_parity, two_j);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        // if(delta_NN == Delta::delta_12)
        //   {
        //     return calculate(pole, delta_NN, delta_12, two_jNN,
        //                      prime_pseudo_parity, two_j, clebsch_gordan,
        //                      factorial);
        //   }
        // return calculate(pole, delta_NN, delta_43, two_jNN,
        //                  prime_pseudo_parity, two_j, clebsch_gordan,
        //                  factorial);
        throw std::runtime_error("M_Matrix is not precomputed");
      }
  }
  std::vector<std::vector<Bigfloat>> &
  calculate(const Pole &pole, const Delta &delta_NN, const Bigfloat &delta,
            const int64_t &two_jNN, const int64_t &prime_pseudo_parity,
            const int64_t &two_j, const Clebsch_Gordan &clebsch_gordan,
            const Factorial &factorial);

  void
  initialize(const int64_t &two_j12, const int64_t &two_j43,
             const int64_t &two_j_max, const int64_t &four_pt_parity,
             const int64_t &order, const Pole_Types &pole_types,
             const Clebsch_Gordan &clebsch_gordan, const Factorial &factorial);

  size_t size() const {
    // prec_to_bytes_num/prec_to_bytes_den is a rational approximation
    // for (log 10 / log 2) / 8
    const size_t prec_to_bytes_num = 267;
    const size_t prec_to_bytes_den = 643;
    
    size_t total_size = sizeof(*this);
    
    for (const auto& pair : cache)
      {
         // TODO: does this correctly measure the size of Delta and
         // Pole?
        total_size += sizeof(pair.first);

        total_size += sizeof(pair.second);
        for (const auto& vec : pair.second)
          {
            total_size += sizeof(vec);

            for (const auto& bigfloat : vec)
              {
                total_size += bigfloat.precision() * prec_to_bytes_num / prec_to_bytes_den;
              }
          }
      }
    
    return total_size;
  }

};
