#include "../Pole_Shifting.hxx"

#include "../lambda_log_r_deriv/Delta_Fraction.hxx"
#include "../../Log_r_Derivatives.hxx"

Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset);

Eigen::PartialPivLU<Eigen_Matrix> pole_lu(const std::vector<Bigfloat> &keep);

std::vector<boost::math::tools::polynomial<Bigfloat>>
pole_complement_product(const std::vector<Bigfloat> &all_kept);

boost::math::tools::polynomial<Bigfloat>
pole_product(const std::vector<Bigfloat> &all_kept);

// A pole p should be "protected" if it is zero. Here, we check that
// |p|<100*epsilon.
bool is_protected(const Bigfloat &p)
{
  // 100 seems big enough to account for any rounding errors
  return boost::multiprecision::abs(p)
         <= std::numeric_limits<Bigfloat>::epsilon() * 100;
}

// Locations of poles in x for the given order, where x = Delta -
// unitarity_bound
std::vector<Bigfloat>
pole_locations_x(const int64_t &two_j, const int64_t &two_j12,
                 const int64_t &two_j43, const int64_t &order,
                 const Pole_Types &pole_types)
{
  const Bigfloat unitarity_bound
    = two_j <= 1 ? two_j * 0.5 + 0.5 : two_j * 0.5 + 1;

  const std::vector<Pole> &poles(
    pole_types.eval(two_j, two_j12, two_j43, order));

  std::vector<Bigfloat> result;
  for(auto &pole : poles)
    {
      result.push_back(pole.delta(two_j) - unitarity_bound);
    }
  return result;
}

void Pole_Shifting::initialize(const std::set<int64_t> &two_js,
                               const int64_t &two_j12, const int64_t &two_j43,
                               const int64_t &order,
                               const int64_t &kept_pole_order,
                               const Pole_Types &pole_types,
                               bool should_do_pole_shifting)
{
  pole_complement_products.reserve(two_js.size());
  pole_products.reserve(two_js.size());
  pole_LUs.reserve(two_js.size());
  new_unprotected_pole_lists.reserve(two_js.size());

  for(int64_t two_j : two_js)
    {
      const Bigfloat unitarity_bound
        = two_j <= 1 ? two_j * 0.5 + 0.5 : two_j * 0.5 + 1;

      std::vector<Bigfloat> new_pole_locations = pole_locations_x(
        two_j, two_j12, two_j43, kept_pole_order, pole_types);
      std::vector<Bigfloat> old_poles
        = pole_locations_x(two_j, two_j12, two_j43, order, pole_types);
      std::vector<Bigfloat> result_poles;

      new_unprotected_pole_lists.emplace_back();
      std::vector<Bigfloat> &new_unprotected_pole_locations
        = new_unprotected_pole_lists.back();
      new_unprotected_pole_locations.reserve(new_pole_locations.size());
      for(auto &loc : new_pole_locations)
        {
          if(!is_protected(loc))
            {
              new_unprotected_pole_locations.push_back(loc);
            }
        }

      if(should_do_pole_shifting)
        {
          std::vector<Bigfloat> unprotected_poles;
          for(auto &pole : old_poles)
            {
              if(is_protected(pole))
                {
                  result_poles.push_back(pole);
                }
              else
                {
                  unprotected_poles.push_back(pole);
                }
            }
          for(auto &pole : new_unprotected_pole_locations)
            {
              result_poles.push_back(pole);
            }
          pole_LUs.emplace_back(pole_lu(new_unprotected_pole_locations));
          result_pole_list_sizes.emplace_back(result_poles.size());
        }
      else
        {
          result_poles = old_poles;
        }
      pole_complement_products.emplace_back(
        pole_complement_product(result_poles));
      pole_products.emplace_back(pole_product(result_poles));
    }
}
