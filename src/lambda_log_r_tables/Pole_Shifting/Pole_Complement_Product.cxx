#include "../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>

std::vector<boost::math::tools::polynomial<Bigfloat>>
pole_complement_product(const std::vector<Bigfloat> &all_kept)
{
  std::vector<boost::math::tools::polynomial<Bigfloat>> result;
  result.reserve(all_kept.size());

  for(size_t index(0); index < all_kept.size(); ++index)
    {
      result.emplace_back(boost::math::tools::polynomial<Bigfloat>({1}));
      auto &element(result.back());
      for(size_t complement_index(0); complement_index < all_kept.size();
          ++complement_index)
        {
          if(complement_index != index)
            {
              element *= boost::math::tools::polynomial<Bigfloat>(
                {-all_kept[complement_index], 1});
            }
        }
    }
  return result;
}
