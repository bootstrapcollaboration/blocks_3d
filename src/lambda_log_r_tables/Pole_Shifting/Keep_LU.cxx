#include "../../Bigfloat.hxx"
#include "../../Eigen.hxx"

Eigen::PartialPivLU<Eigen_Matrix> pole_lu(const std::vector<Bigfloat> &keep)
{
  const int64_t size(keep.size());
  Eigen_Matrix A(size, size);

  Bigfloat temp;
  for(int64_t column = 0; column < size; ++column)
    {
      temp = pow(keep[column], -(size / 2));
      for(int64_t row = 0; row < size; ++row)
        {
          A(row, column) = temp; // keep[column] ^ (row - (size / 2))
          if(row != size - 1)
            {
              temp *= keep[column];
            }
        }
    }

  return A.lu();
}
