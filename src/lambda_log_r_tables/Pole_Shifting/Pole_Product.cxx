#include "../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>

boost::math::tools::polynomial<Bigfloat>
pole_product(const std::vector<Bigfloat> &all_kept)
{
  boost::math::tools::polynomial<Bigfloat> pole_product({1});
  for(auto &pole : all_kept)
    {
      pole_product *= boost::math::tools::polynomial<Bigfloat>({-pole, 1});
    }
  return pole_product;
}
