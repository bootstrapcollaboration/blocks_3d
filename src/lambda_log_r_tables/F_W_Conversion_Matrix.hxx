#pragma once

#include "Factorial.hxx"
#include "Clebsch_Gordan.hxx"
#include "lambda_log_r_deriv/conformal_block_derivatives/h_infinity/SO3.hxx"

#include <map>
#include <tuple>
#include <array>


Bigfloat f_W_conversion_matrix_left(const std::array<int64_t, 4> &two_js,
                                    const int64_t &two_j, const SO3 &left_SO3,
                                    const std::array<int64_t, 3> &left_q3,
                                    const Clebsch_Gordan &cg, const Factorial &factorial);

Bigfloat
f_W_conversion_matrix_right(const std::array<int64_t, 4> &two_js,
                            const int64_t &two_j, const SO3 &right_SO3,
                            const std::array<int64_t, 3> &right_q3,
                            const Clebsch_Gordan &cg, const Factorial &factorial);

struct F_W_Conversion_Matrix
{
  std::map<
    std::tuple<std::array<int64_t, 4>, int64_t, SO3, std::array<int64_t, 3>>,
    Bigfloat>
    cache_left, cache_right;
    
  const Bigfloat &
  eval_left(const std::array<int64_t, 4> &two_js, const int64_t &two_j,
            const SO3 &left_SO3, const std::array<int64_t, 3> &left_q3,
            const Clebsch_Gordan &cg, const Factorial &factorial)
  {
    std::tuple<std::array<int64_t, 4>, int64_t, SO3, std::array<int64_t, 3>>
      key(two_js, two_j, left_SO3, left_q3);
    auto element(cache_left.find(key));
    if(element != cache_left.end())
      {
        return element->second;
      }
    else
      {
        auto iterator(cache_left.emplace(std::pair(key, Bigfloat())).first);
        Bigfloat &result(iterator->second);
        result = f_W_conversion_matrix_left(two_js, two_j, left_SO3, left_q3,
                                            cg, factorial);
        return result;
      }
  }

  const Bigfloat &
  eval_right(const std::array<int64_t, 4> &two_js, const int64_t &two_j,
            const SO3 &right_SO3, const std::array<int64_t, 3> &right_q3,
            const Clebsch_Gordan &cg, const Factorial &factorial)
  {
    std::tuple<std::array<int64_t, 4>, int64_t, SO3, std::array<int64_t, 3>>
      key(two_js, two_j, right_SO3, right_q3);
    auto element(cache_right.find(key));
    if(element != cache_right.end())
      {
        return element->second;
      }
    else
      {
        auto iterator(cache_right.emplace(std::pair(key, Bigfloat())).first);
        Bigfloat &result(iterator->second);
        result = f_W_conversion_matrix_right(two_js, two_j, right_SO3, right_q3,
                                            cg, factorial);
        return result;
      }
  }
};
