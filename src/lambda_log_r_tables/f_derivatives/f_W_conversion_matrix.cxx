#include "../lambda_log_r_deriv/conformal_block_derivatives/h_infinity/SO3.hxx"
#include "../lambda_log_r_deriv/conformal_block_derivatives/clebsch_gordan.hxx"
#include "../Cache.hxx"
#include "../../timers/Timers.hxx"

#include <vector>

Bigfloat f_W_conversion_matrix(const std::array<int64_t, 4> &two_js,
                               const int64_t &two_j, const SO3 &left_SO3,
                               const SO3 &right_SO3,
                               const std::array<int64_t, 3> &left_q3,
                               const std::array<int64_t, 3> &right_q3,
                               const Clebsch_Gordan &clebsch_gordan_cache,
                               const Factorial &factorial,
                               F_W_Conversion_Matrix &f_w_conversion_matrix,
                               Timers &timers)
{
  thread_local auto &full = timers.add_and_start(
    "calculate.f_SO3_basis_derivative.f_W_conversion_matrix");
  full.start();

  // const int64_t sign(
  //   ((two_js[0] + two_js[3] - 2 * two_j + left_q3[1] + right_q3[1]) / 2) % 2
  //       == 0
  //     ? 1
  //     : -1);

  Bigfloat result
    = f_w_conversion_matrix.eval_left(two_js, two_j, left_SO3, left_q3,
                                      clebsch_gordan_cache, factorial)
      * f_w_conversion_matrix.eval_right(two_js, two_j, right_SO3, right_q3,
                                         clebsch_gordan_cache, factorial);

  full.stop();
  return result;
}

Bigfloat f_W_conversion_matrix_left(const std::array<int64_t, 4> &two_js,
                                    const int64_t &two_j, const SO3 &left_SO3,
                                    const std::array<int64_t, 3> &left_q3,
                                    const Clebsch_Gordan &clebsch_gordan_cache,
                                    const Factorial &factorial)
{
  const int64_t two_j12(left_SO3[0]), two_j120(left_SO3[1]), two_q(left_q3[2]);
  const std::array<int64_t, 2> two_qs({left_q3[0], left_q3[1]});

  Bigfloat product(1);
  for(int64_t ii = 0; ii < 2; ++ii)
    {
      product
        *= binomial_coefficient(two_js[ii], (two_js[ii] + two_qs[ii]) / 2);
    }

  Bigfloat result(0);
  if(std::abs(two_q) <= two_j12 && std::abs(two_q) <= two_j)
    {
      result
        = clebsch_gordan(two_js[0], two_qs[0], two_js[1], two_qs[1], two_j12,
                         -two_q, factorial)
          * clebsch_gordan_cache.eval(two_j12, two_j120, two_j, two_q)
          * sqrt(binomial_coefficient(two_j, (two_j + two_q) / 2) * product);
    }

  const int64_t sign_left(
    ((two_js[0] - two_j + left_q3[1]) / 2) % 2 == 0 ? 1 : -1);

  return result * sign_left;
}

Bigfloat
f_W_conversion_matrix_right(const std::array<int64_t, 4> &two_js,
                            const int64_t &two_j, const SO3 &right_SO3,
                            const std::array<int64_t, 3> &right_q3,
                            const Clebsch_Gordan &clebsch_gordan_cache,
                            const Factorial &factorial)
{
  const int64_t two_j43(right_SO3[0]), two_j430(right_SO3[1]),
    two_qp(right_q3[2]);
  const std::array<int64_t, 4> two_qs({0, 0, right_q3[1], right_q3[0]});

  Bigfloat product(1);
  for(int64_t ii = 2; ii < 4; ++ii)
    {
      product
        *= binomial_coefficient(two_js[ii], (two_js[ii] + two_qs[ii]) / 2);
    }

  Bigfloat result(0);
  if(std::abs(two_qp) <= two_j43 && std::abs(two_qp) <= two_j)
    {
      result
        = clebsch_gordan(two_js[3], two_qs[3], two_js[2], two_qs[2], two_j43,
                         -two_qp, factorial)
          * clebsch_gordan_cache.eval(two_j43, two_j430, two_j, two_qp)
          * sqrt(binomial_coefficient(two_j, (two_j + two_qp) / 2) * product);
    }

  const int64_t sign_right(
    ((two_js[3] - two_j + right_q3[1]) / 2) % 2 == 0 ? 1 : -1);

  return result*sign_right;
}
