#include "../../Bigfloat.hxx"
#include "../Factorial.hxx"
#include "../Wigner_Cache.hxx"
#include "../../timers/Timers.hxx"

Bigfloat
wjm_matrix(const int64_t &two_j, const int64_t &two_m1, const int64_t &two_m2);

Bigfloat tilde_wigner_derivative(const int64_t &two_j, const int64_t &two_m,
                                 const int64_t &two_mp, const int64_t &Nn,
                                 const Factorial &factorial,
                                 Wigner_Cache &wigner_cache);

Bigfloat f_q_basis_derivative(const std::array<int64_t, 4> &two_js,
                              const int64_t &two_j,
                              const std::array<int64_t, 3> &left_q3,
                              const std::array<int64_t, 3> &right_q3,
                              // const std::array<int64_t, 4> &q4,
                              const int64_t &i,
                              const int64_t &n, // const Factorial &factorial,
                              const Wigner_Cache &wigner_cache,
                              Timers &timers)
{
  thread_local Timer &full
    = timers.add_and_start("f_q_basis_derivative");
  full.start();

  // We use F43 instead of F430 to have a real block.
  const int64_t two_p(left_q3[2]), two_pp(right_q3[2]),
    f43(two_js[3] % 2 + two_js[2] % 2);

  // constexpr int64_t two_point_b(1);

  // The expression
  //
  // f43 - two_js[2] - two_js[3] - f0 = f43 - two_js[2] - two_js[3]
  //
  // is always even.

  const int64_t sign((f43 - two_js[2] - two_js[3]) % 4 == 0 ? 1 : -1);
  Bigfloat result
    = sign 
      * wigner_cache.eval_wigner_product(left_q3[0], left_q3[1], right_q3[1], right_q3[0], i)
      * wigner_cache.eval_tilde_wigner_derivative_modified(two_j, -two_pp, -two_p, n);

  full.stop();
  return result;
}
