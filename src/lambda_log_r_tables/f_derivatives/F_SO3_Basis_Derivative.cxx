#include "../F_SO3_Basis_Derivative.hxx"


Bigfloat f_W_conversion_matrix(const std::array<int64_t, 4> &two_js,
                               const int64_t &two_j, const SO3 &left_SO3,
                               const SO3 &right_SO3,
                               const std::array<int64_t, 3> &left_q3,
                               const std::array<int64_t, 3> &right_q3,
                               const Clebsch_Gordan &clebsch_gordan_cache,
                               const Factorial &factorial,
                               F_W_Conversion_Matrix &f_w_conversion_matrix,
                               Timers &timers);

Bigfloat f_SO3_basis_derivative(const std::array<int64_t, 4> &two_js,
                                const int64_t &two_j, const SO3 &left_SO3,
                                const SO3 &right_SO3, const int64_t &i_4pt,
                                const int64_t &n,
                                const Clebsch_Gordan &clebsch_gordan_cache,
                                const Factorial &factorial,
                                const F_Q_Basis_Derivative &f_q_basis_derivative,
                                F_W_Conversion_Matrix &f_w_conversion_matrix)
{
  const std::vector<std::vector<std::pair<int64_t, std::array<int64_t, 3>>>>
    &left_q3_set = f_q_basis_derivative.left_q3_sets[two_j / 2];
  const std::vector<std::vector<std::pair<int64_t, std::array<int64_t, 3>>>>
    &right_q3_set = f_q_basis_derivative.right_q3_sets[two_j / 2];

  std::vector<std::reference_wrapper<const Bigfloat>> left_W, right_W;
  left_W.reserve(left_q3_set.size());
  right_W.reserve(right_q3_set.size());

  for(auto &left_q3 : left_q3_set)
    left_W.emplace_back(std::reference_wrapper(f_w_conversion_matrix.eval_left(
      two_js, two_j, left_SO3, left_q3[0].second, clebsch_gordan_cache,
      factorial)));

  for(auto &right_q3 : right_q3_set)
    right_W.emplace_back(std::reference_wrapper(
      f_w_conversion_matrix.eval_right(two_js, two_j, right_SO3,
                                       right_q3[0].second,
                                       clebsch_gordan_cache, factorial)));

  int64_t parity_left
    = ((two_js[0] - two_js[1] + two_j - left_SO3[1]) / 2) % 2;
  int64_t parity_right
    = ((two_js[3] - two_js[2] + two_j - right_SO3[1]) / 2) % 2;

  Bigfloat result(0), tmp_float(0), tmp_float_2(0);
  for(size_t left_q3_index = 0; left_q3_index != left_q3_set.size();
      ++left_q3_index)
    {
      tmp_float = 0;
      const auto &left_q3 = left_q3_set[left_q3_index];
      for(size_t right_q3_index = 0; right_q3_index != right_q3_set.size();
          ++right_q3_index)
        {
          const auto &right_q3 = right_q3_set[right_q3_index];
          tmp_float_2 = 0;
          for(size_t i = 0; i != left_q3.size(); ++i)
            {
              for(size_t j = 0; j != right_q3.size(); ++j)
                {
                  // determine the sign of the contribution based on parity,
                  // so we can save on some multiplications.
                  // This loop is very computation-intensive for TTTT blocks,
                  // can get to 10^8 cycles.
                  if((parity_right * j + parity_left * i) % 2 == 0)
                    tmp_float_2 += f_q_basis_derivative.eval_by_3pt_indices(
                      two_j, left_q3.at(i).first, right_q3.at(j).first, i_4pt,
                      n);
                  else
                    tmp_float_2 -= f_q_basis_derivative.eval_by_3pt_indices(
                      two_j, left_q3.at(i).first, right_q3.at(j).first, i_4pt,
                      n);
                }
            }
          tmp_float_2 *= right_W[right_q3_index].get();
          tmp_float += tmp_float_2;
        }
      result += left_W[left_q3_index].get() * tmp_float;
    }
  return result;
}

void F_SO3_Basis_Derivative::initialize(
  const std::array<int64_t, 4> &two_js, const int64_t &two_j_max,
  const int64_t &two_j12, const int64_t &two_j43,
  const std::array<int64_t, 4> &q4, const int64_t &Nmax,
  const Clebsch_Gordan &clebsch_gordan_cache, const Factorial &factorial,
  const F_Q_Basis_Derivative &f_q_basis_derivative,
  F_W_Conversion_Matrix &f_w_conversion_matrix)
{
  std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4, {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));
  for(int64_t pp_index = 0; pp_index != 2; ++pp_index)
    for(int64_t two_j = two_j_max % 2; two_j <= two_j_max; two_j += 2)
      {
        std::vector<int64_t> two_j120_set
          = so3_tensor_pseudo_parity(pseudo_parity_types[pp_index][0], two_j12,
                                     two_j),
          two_j430_set = so3_tensor_pseudo_parity(
            pseudo_parity_types[pp_index][1], two_j43, two_j);
        for(auto two_j120 : two_j120_set)
          for(auto two_j430 : two_j430_set)
            {
              SO3 left_SO3(two_j12, two_j120), right_SO3(two_j43, two_j430);
              for(int64_t i_4pt = 0; i_4pt != 2; ++i_4pt)
                {
                  std::tuple<int64_t, SO3, SO3, int64_t> key(two_j, left_SO3,
                                                             right_SO3, i_4pt);
                  std::vector<Bigfloat> &result(
                    cache.emplace(key, std::vector<Bigfloat>()).first->second);
                  result.reserve(Nmax + 1);
                  for(int64_t n = 0; n <= Nmax; ++n)
                    {
                      result.emplace_back(f_SO3_basis_derivative(
                        two_js, two_j, left_SO3, right_SO3, i_4pt, n,
                        clebsch_gordan_cache, factorial, f_q_basis_derivative,
                        f_w_conversion_matrix));
                    }
                }
            }
      }
}
