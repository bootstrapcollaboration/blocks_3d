#pragma once

#include <boost/math/constants/constants.hpp>
#include <boost/math/special_functions.hpp>
#include <boost/multiprecision/gmp.hpp>

using Bigfloat = boost::multiprecision::mpf_float;

// We implement factorial, binomial, and Pochhammer by hand because
// boost::math's versions want to initialize epsilon in a static
// constructor.  However, Bigfloat has not had its precision set yet,
// so it ends up dividing by zero and crashing.

inline Bigfloat factorial_slow(const int64_t &n)
{
  Bigfloat result(1);
  for(int64_t kk = 2; kk <= n; ++kk)
    {
      result *= kk;
    }
  return result;
}

// Pochhammer(a,n) = Gamma(a+n)/Gamma(a)
// Boost's implementation of rising_factorial is horrifically slow.
inline Bigfloat Pochhammer(const Bigfloat &alpha, const int64_t &n)
{
  if(n < 0)
    {
      throw std::runtime_error("negative n in Pochhammer");
    }
  Bigfloat result(1);
  for(int64_t kk = 0; kk < n; ++kk)
    {
      result *= alpha + kk;
    }
  return result;
}

// factorial_power(x,n) = Gamma(x+1)/Gamma(x+1-n)
inline Bigfloat factorial_power(const Bigfloat &x, const int64_t &n)
{
  if(n<0)
    {
      return 1/Pochhammer(x + 1, -n);
    }
  return Pochhammer(x + 1 - n, n);
}

// Binomial(k,j) = Gamma(k+1)/(Gamma(j+1) * Gamma(k-j+1))
inline Bigfloat binomial_coefficient(const int64_t &k, const int64_t &j)
{
  // TODO: This is annoyingly complicated
  if(k < j)
    {
      if(j < 0) // k<0 so Gamma(k+1) is inifinite
        // j<0 so Gamma(j+1) is inifinite
        // but Gamma(k+1)/Gamma(j+1) is finite
        // k<j so Gamma(k-j+1) is infinite
        // => 0
        {
          return 0;
        }
      else
        {
          // j>=0 so Gamma(j+1) is finite
          // k<j
          // so Gamma(k+1)/Gamma(k-j+1)==Pochhammer(k-j+1,j) is finite
          //   If k<0, infinities cancel out.
          //   If k>=0, k-j<0 so Pochhammer is zero
          // => Pochhammer(k-j+1,j)/factorial(j)
          return Pochhammer(k - j + 1, j) / factorial_slow(j);
        }
    }
  else
    {
      // Gamma(k+1)/Gamma(j+1) == Pochhammer(j+1,k-j)
      // k>=j so k-j>=0 -> Pochhammer(k+j,k-j) is finite
      // k>=j so Gamma(k-j+1) is finite
      // => Pochhammer(k+j,k-j)/factorial(k-j)
      return Pochhammer(j + 1, k - j) / factorial_slow(k - j);
    }
}

// A version for Bigfloat
inline Bigfloat binomial_coefficient(const Bigfloat &k, const int64_t &j)
{
  if(j < 0)
    {
      // If zero is passed in, it should be exact from integer
      // arithmetic.
      if(k >= 0)
        {
          return 0;
        }
      else
        {
          std::stringstream ss;
          ss << "INTERNAL_ERROR: binomial_coefficient(): " << k << " " << j;
          throw std::runtime_error(ss.str());
        }
    }
  return Pochhammer(k - j + 1, j) / factorial_slow(j);
}
