#pragma once

#include "Bigfloat.hxx"
#include "ostream_vector.hxx"

#include <boost/math/tools/polynomial.hpp>

struct Log_r_Derivatives
{ // the vector is indexed by log(r) derivative order
  std::vector<boost::math::tools::polynomial<Bigfloat>> numerator;

  size_t size() const {
    // prec_to_bytes_num/prec_to_bytes_den is a rational approximation
    // for (log 10 / log 2) / 8
    const size_t prec_to_bytes_num = 267;
    const size_t prec_to_bytes_den = 643;
    
    size_t total_size = sizeof(*this);
    for (const auto& poly : numerator) {
      total_size += sizeof(poly);
      if (poly.size() > 0)
        {
          total_size += poly.size() * poly.data()[0].precision() * prec_to_bytes_num / prec_to_bytes_den;
        }
    }
    return total_size;
  }
};

inline std::ostream &operator<<(std::ostream &os, const Log_r_Derivatives &spin)
{
  return os << spin.numerator;
}
