
* [Using Docker](#Use-Docker)
* [Compile from sources](#Compile-from-source)

# Use Docker

The easiest way is to download the latest [Docker image](https://hub.docker.com/r/bootstrapcollaboration/blocks_3d):

    docker pull bootstrapcollaboration/blocks_3d:master

and run it as follows:

    docker run -v $PWD:/usr/local/share/blocks_3d bootstrapcollaboration/blocks_3d:master blocks_3d --j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 -o /usr/local/share/blocks_3d/output/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"

This command will [mount](https://docs.docker.com/storage/bind-mounts/) your current directory to `/usr/local/share/blocks_3d` directory of the image and run `blocks_3d` with the given arguments.

You may also [build](https://docs.docker.com/engine/reference/commandline/build/) Docker image by yourself, using the included [Dockerfile](Dockerfile):

    docker build . --tag blocks_3d:my_tag
    docker run blocks_3d:my_tag blocks_3d --help

# Compile from source

This is a general guide for building `blocks_3d`.
Site-specific notes for different HPC machines cna be found in [doc/site_installs](doc/site_installs) folder.

## Requirements

Blocks_3D requires

- A modern C++ compiler with C++ 17 support.

- [Boost C++ Libraries](http://www.boost.org/) Versions 1.69-1.73 have a performance bug which prevents
  them from performing well with multiple cores.

- [The GNU Multiprecision Library](https://gmplib.org/) This is used
  by the Boost multiprecision library.  Only the C bindings are
  required.

- [Eigen](https://eigen.tuxfamily.org)

- [fmt](https://fmt.dev/latest/index.html)

- [Python](https://python.org)

Blocks_3D has only been tested on Linux (Debian buster and Centos 7
and 8).  In principle, it should be installable on Mac OS X using
libraries from a package manager such as [Homebrew](https://brew.sh).

Some of these dependencies may be available via modules or a package
manager.  On Yale's Grace cluster:

    module load git CMake GCCcore/6.4.0 Boost/1.66.0-foss-2018a GMP/6.1.2-GCCcore-6.4.0 Eigen/3.3.7

On Debian Buster:

    apt-get install libeigen3-dev libboost-dev libfmt-dev libgmp-dev

## Installation

1. Download Blocks_3D

        git clone https://gitlab.com/bootstrapcollaboration/blocks_3d.git
        cd blocks_3d

2. Configure the project using the included version of
   [waf](https://waf.io).  Waf can usually find libraries that are in
   system directories, but it needs direction for everything else is.
   If you are having problems, running `python ./waf --help` will give
   you a list of options.
   
   On Yale's Grace cluster, a working command with a locally built copy of fmt

        ./waf configure --prefix=$HOME/project/install --fmt-dir=$HOME/project/install --fmt-libdir=$HOME/project/install/lib64 --boost-dir=$BOOST_ROOT

    and on Debian buster, it is

        CXX=mpicxx python ./waf configure

3. Type `python ./waf` to build the executable in `build/blocks_3d`.  Running
   
        ./build/blocks_3d --help
   
   should give you a usage message.
