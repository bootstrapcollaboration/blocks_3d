#! /usr/bin/env python
# encoding: utf-8

import os

def configure(conf):
    def get_param(varname,default):
        return getattr(Options.options,varname,'')or default

    if not conf.options.fmt_incdir:
        for d in ['FMT_INCLUDE','FMT_INCLUDE_DIR','FMT_INC_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.fmt_incdir=env_dir
                
    if not conf.options.fmt_libdir:
        for d in ['FMT_LIB','FMT_LIB_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.fmt_incdir=env_dir

    if not conf.options.fmt_dir:
        env_dir=os.getenv('FMT_DIR')
        if env_dir:
            conf.options.fmt_incdir=env_dir
                
    # Find FMT
    if conf.options.fmt_dir:
        if not conf.options.fmt_incdir:
            conf.options.fmt_incdir=conf.options.fmt_dir + "/include"
        if not conf.options.fmt_libdir:
            conf.options.fmt_libdir=' '.join([conf.options.fmt_dir + lib for lib in ["/lib", "/lib64"]])

    if conf.options.fmt_incdir:
        fmt_incdir=conf.options.fmt_incdir.split()
    else:
        fmt_incdir=[]
    if conf.options.fmt_libdir:
        fmt_libdir=conf.options.fmt_libdir.split()
    else:
        fmt_libdir=[]

    if conf.options.fmt_libs:
        fmt_libs=conf.options.fmt_libs.split()
    else:
        fmt_libs=['fmt']

    conf.check_cxx(msg="Checking for FMT",
                   header_name='fmt/format.h',
                   includes=fmt_incdir,
                   uselib_store='fmt',
                   libpath=fmt_libdir,
                   rpath=fmt_libdir,
                   lib=fmt_libs,
                   use=['cxx17'])

def options(opt):
    fmt=opt.add_option_group('FMT Options')
    fmt.add_option('--fmt-dir',
                   help='Base directory where fmt is installed')
    fmt.add_option('--fmt-incdir',
                   help='Directory where fmt include files are installed')
    fmt.add_option('--fmt-libdir',
                   help='Directory where fmt library files are installed')
    fmt.add_option('--fmt-libs',
                   help='Names of the fmt libraries without prefix or suffix\n'
                   '(e.g. "fmt")')
