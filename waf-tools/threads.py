#! /usr/bin/env python
# encoding: utf-8

def configure(conf):
    def get_param(varname,default):
        return getattr(Options.options,varname,'')or default


    threads_fragment='#include<thread>\nvoid f1(int n){}\nint main() {std::thread t(f1,2); t.join(); }'
    threads_libs=['','pthread']
    if conf.options.threads_libs:
        threads_libs=[conf.options.threads_libs]
    found_threads=False
    for threads_lib in threads_libs:
        try:
            conf.check_cxx(msg="Checking Threads Library " + threads_lib,
                           fragment=threads_fragment,
                           lib=threads_lib.split(),
                           uselib_store='threads',
                           use=['cxx17'])
        except conf.errors.ConfigurationError:
            continue
        else:
            found_threads=True
            break
    if not found_threads:
        conf.fatal('Could not find threads library')

def options(opt):
    threads=opt.add_option_group('Threads Options')
    threads.add_option('--threads-libs',
                       help='libraries for compiling with threads (e.g. "pthreads")')
