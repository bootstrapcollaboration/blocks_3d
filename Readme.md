# Blocks_3D

C++ implementation of 3D spinning blocks

For more information, see the article [blocks_3d: Software for general
3d conformal blocks](https://arxiv.org/abs/2011.01959) and a note on
[conventions](doc/conventions.pdf).

Installation instructions are detailed in [Install.md](Install.md).

Running the executable with the `--help` option should print out all
of the options.  A simple example that uses 4 threads and should
finish quickly is
   
    ./build/blocks_3d --j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 -o output/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"

The results will be in a number of JSON files in the directory
`output/`.  They follow the [JSON schema](https://json-schema.org/)
detailed in [blocks_3d_schema.json](doc/blocks_3d_schema.json).

## Attribution

If you use `blocks_3d` in work that results in publication, consider citing

- Erramilli, R. S. et. al., *blocks_3d: Software for general 3d conformal blocks*, [arXiv:2011.01959 \[hep-th\]](https://arxiv.org/abs/2011.01959).

The recommended BibTeX entry is

```
@article{erramilli2020blocks3d,
        title={blocks_3d: Software for general 3d conformal blocks}, 
        author={Rajeev S. Erramilli and Luca V. Iliesiu and Petr Kravchuk and Walter Landry and David Poland and David Simmons-Duffin},
        year={2020},
        eprint={2011.01959},
        archivePrefix={arXiv},
        primaryClass={hep-th},
        doi = {10.1007/JHEP11(2021)006},
        journal = {JHEP},
        volume = {11},
        pages = {006},
        year = {2021}
}
```

